<?php 
class M_login extends CI_Model{	
	function cek_login($table,$where){		
		return $this->db->get_where($table,$where);
	}
	function session_email($table,$where){
		$query = $this->db->select('email')->from($table)->where($where)->get();
		return $query->row()->email;
	}
	function id_user_login($email){
		$query = $this->db->select('id_user')->from('t_data_user')->where('email', $email)->get();
		return $query->row()->id_user;
	}
	function nama_login($email){
		$query = $this->db->select('nama_depan')->from('t_data_user')->where('email', $email)->get();
		return $query->row()->nama_depan;
    	// return $query->result_array();
	}
}