<?php
class M_data extends CI_Model{
	function get_data(){
		$query = "SELECT t_data_user.nama_depan,t_data_utama.id_user,no,t_data_utama.id_user,nama_project,instansi,type,divisi,esti_pendapatan,real_pendapatan,no_spk,tanggal,kode_project,pic_instansi,no_telp FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user ORDER BY tanggal DESC";
		return $this->db->query($query);
	}
	function get_data_staff(){
		$id = $this->session->userdata('id_user');
		$query = "SELECT t_data_user.nama_depan,t_data_utama.id_user,no,nama_project,instansi,type,divisi,esti_pendapatan,real_pendapatan,no_spk,tanggal FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE t_data_utama.id_user='$id' ORDER BY tanggal DESC";
		return $this->db->query($query);
	}
	function get_data_user(){
		$sql = "SELECT * from t_data_user";
		return $this->db->query($sql);
	}
	function nama($nomor){
		$sql  = "SELECT * FROM t_coba WHERE nomor='$nomor'";
		$nam  = $this->db->query($sql);

		foreach ($nam->result_array() as $data ){

			$a.= "<option value='$data[nomor]'>$data[nama]</option>";

		}
		return $a;
	}
	function email_cc($no){
		$query = $this->db->select('email_group')->from('t_data_utama')->where($no)->get();
		return $query->row()->email_group;
	}
	function hasil_email($id_user){
		$sql  = "SELECT * FROM t_data_user WHERE id_user='$id_user'";
		$tim  = $this->db->query($sql);
		foreach ($tim->result_array()  as $data){
			$t.= "<option value='$data[email]' checked>$data[nama_depan]</option>";
		} 
		return $t;
	}
	function hasil_divisi($tahun){
		$sql  = "SELECT divisi FROM t_data_utama WHERE tanggal LIKE '%$tahun%' GROUP BY divisi ";
		$hasil = $this->db->query($sql);
		echo "<option value='' selected></option>";
		foreach ($hasil->result_array()  as $data){
			$t.= "<option value='$data[divisi]'>$data[divisi]</option>";
		} 
		return $t;
	}
	function hasil_pic($divisi){
		$sql  = "SELECT t_data_utama.id_user,t_data_user.nama_depan FROM t_data_user INNER JOIN t_data_utama 
		ON t_data_user.id_user=t_data_utama.id_user WHERE divisi='$divisi' GROUP BY t_data_utama.id_user";
		echo "<option value='' selected></option>";
		$hasil = $this->db->query($sql);
		foreach ($hasil->result_array()  as $data){   
			$a.="<option value='$data[id_user]'>$data[nama_depan]</option>";
		} 
		return $a;
	}
	function hasil_pic_tahun($tahun){
		$sql  = "SELECT t_data_utama.id_user,t_data_user.nama_depan FROM t_data_user INNER JOIN t_data_utama 
		ON t_data_user.id_user=t_data_utama.id_user WHERE tanggal LIKE '%$tahun%' GROUP BY t_data_utama.id_user";
		echo "<option value='' selected></option>";
		$hasil = $this->db->query($sql);
		foreach ($hasil->result_array()  as $data){   
			$a.="<option value='$data[id_user]'>$data[nama_depan]</option>";
		} 
		return $a;
	}
	function hasil_type($id_user){
		$sql  = "SELECT type FROM t_data_utama WHERE id_user='$id_user' GROUP BY type";
		$hasil = $this->db->query($sql);
		echo "<option value='' selected></option>";
		foreach ($hasil->result_array()  as $data){
			$t.= "<option value='$data[type]'>$data[type]</option>";
		} 
		return $t;
	}
	function hasil_type_divisi($divisi){
		$sql  = "SELECT type FROM t_data_utama WHERE divisi='$divisi' GROUP BY type";
		$hasil = $this->db->query($sql);
		echo "<option value='' selected></option>";
		foreach ($hasil->result_array()  as $data){
			$t.= "<option value='$data[type]'>$data[type]</option>";
		} 
		return $t;
	}
	function hasil_type_tahun($tahun){
		$sql  = "SELECT type FROM t_data_utama WHERE tanggal LIKE '%$tahun%' GROUP BY type";
		$hasil = $this->db->query($sql);
		echo "<option value='' selected></option>";
		foreach ($hasil->result_array()  as $data){
			$t.= "<option value='$data[type]'>$data[type]</option>";
		} 
		return $t;
	}
	function get_group($id_user,$nama_project,$instansi){
		$sql = "SELECT email_group FROM t_log WHERE id_user ='$id_user' && nama_project='$nama_project' && instansi='$instansi'";
		return $this->db->query($sql);
	}
	function add_data_email(){
		$email = $this->session->userdata('email');
		$sql = "SELECT id_user,email,nama_depan,level FROM t_data_user WHERE level ='admin' || email = '$email'";
		return $this->db->query($sql);
	}
	function get_user(){
		$email = $this->session->userdata('email');
		$sql = "SELECT id_user,email,nama_depan,level FROM t_data_user WHERE level ='admin' || email = '$email'";
		return $this->db->query($sql);
	}

	// function get_user_staff(){
	// 	$email = $this->session->userdata('email');
	// 	$sql = "SELECT * FROM t_data_user WHERE NOT email='$email' && level='staff' || level='staff_lain'";
	// 	return $this->db->query($sql);
	// }
	
	function get_user_staff(){
		$sql = "SELECT * FROM t_data_user WHERE level='staff' || level='manager'";
		return $this->db->query($sql);
	}
	function get_user_staff_lain(){
		$sql = "SELECT * FROM t_data_user WHERE level='staff_lain' || level='staff'";
		return $this->db->query($sql);
	}
	function exist_email_group($where){
		$query = $this->db->select('email_group')->from('t_data_utama')->where($where)->get();
		return $query->row()->email_group;
	}
	function exist_email_group_lain($where){
		$query = $this->db->select('email')->from('t_data_user')->where($where)->get();
		return $query->row()->email;
	}
	function show_email_utama($email){
		// $query = $this->db->select('email_group')->from('t_data_utama')->like('email_group',$email,'both')->get();
		// return $query->row()->email_group;
		
		$data = array();
		$this->db->select('email_group');
		$this->db->select('id_user');
		$this->db->select('nama_project');
		$this->db->select('instansi');
		$this->db->from('t_data_utama');
		$this->db->like('email_group',$email,'both');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}else{
				$data[] = "1";
		}
		$query->free_result();
		//return the array to the controller
		return $data;
	}
	function get_nama(){
		$sql = "SELECT * from t_coba";
		return $this->db->query($sql);
	}
	function get_nama_pic($id_user){
		$query = $this->db->select('nama_depan')->from('t_data_user')->where('id_user',$id_user)->get();
		return $query->row()->nama_depan;
	}
	function nama_project_group(){
		$email = $this->session->userdata('email');
		$sql   = "SELECT nama_project FROM t_data_utama WHERE email_group LIKE '%$email%'";
		return $this->db->query($sql); 
	}
	function tampil_gambar(){
		$id    = '126';
		$sql   = "SELECT file FROM t_log WHERE id_log ='$id' ";
		return $this->db->query($sql); 
	}
	function instansi_group(){
		$email = $this->session->userdata('email');
		$sql   = "SELECT instansi FROM t_data_utama WHERE email_group LIKE '%$email%'";
		return $this->db->query($sql); 
	}
	function get_data_excel($group_project,$instansi_group){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,rincian_log,progress_log,update_log,komentar 
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where nama_project='$group_project' && instansi='$instansi_group'ORDER BY update_log DESC";
		return $this->db->query($sql);
	}
	function export_all($tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,real_pendapatan
		FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user where tanggal LIKE '%$tahun%'";
		return $this->db->query($sql);
	}
	function export_1($divisi,$id_user,$type,$tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,real_pendapatan
		FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user 
		where tanggal LIKE '%$tahun%' && divisi='$divisi' && t_data_utama.id_user='$id_user'&& type='$type'";
		return $this->db->query($sql);
	}
	function export_2($divisi,$type,$tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,real_pendapatan
		FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user 
		where tanggal LIKE '%$tahun%' && divisi='$divisi' && type='$type'";
		return $this->db->query($sql);
	}
	function export_3($id_user,$type,$tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,real_pendapatan
		FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user 
		where tanggal LIKE '%$tahun%' && t_data_utama.id_user='$id_user' && type='$type'";
		return $this->db->query($sql);
	}
	function export_4($divisi,$tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,real_pendapatan
		FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user 
		where divisi='$divisi' && tanggal LIKE '%$tahun%'";
		return $this->db->query($sql);
	}
	function export_5($id_user,$divisi,$tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,real_pendapatan
		FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user 
		where tanggal LIKE '%$tahun%' && t_data_utama.id_user='$id_user' && divisi='$divisi'";
		return $this->db->query($sql);
	}
	function export_6($type,$tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,real_pendapatan
		FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user 
		where tanggal LIKE '%$tahun%' && type='$type'";
		return $this->db->query($sql);
	}
	function export_7($tahun,$id_user){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,real_pendapatan
		FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user 
		where tanggal LIKE '%$tahun%' && t_data_utama.id_user='$id_user'";
		return $this->db->query($sql);
	}
	function export_1_log($divisi,$id_user,$type,$tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,rincian_log,progress_log,update_log,komentar
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where update_log LIKE '%$tahun%' && divisi='$divisi' && t_log.id_user='$id_user'&& type='$type'ORDER BY nama_depan ASC";
		return $this->db->query($sql);
	}
	function export_2_log($tahun){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,rincian_log,progress_log,update_log,komentar
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where update_log LIKE '%$tahun%' ORDER BY nama_depan ASC";
		return $this->db->query($sql);
	}
	function export_3_log($tahun,$divisi){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,rincian_log,progress_log,update_log,komentar
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where update_log LIKE '%$tahun%' && divisi='$divisi' ORDER BY nama_depan ASC";
		return $this->db->query($sql);
	}
	function export_4_log($tahun,$id_user){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,rincian_log,progress_log,update_log,komentar
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where update_log LIKE '%$tahun%' && t_log.id_user='$id_user' ORDER BY nama_depan ASC";
		return $this->db->query($sql);
	}
	function export_5_log($tahun,$type){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,rincian_log,progress_log,update_log,komentar
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where update_log LIKE '%$tahun%' && type='$type' ORDER BY nama_depan ASC";
		return $this->db->query($sql);
	}
	function export_6_log($tahun,$divisi,$id_user){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,rincian_log,progress_log,update_log,komentar
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where update_log LIKE '%$tahun%' && divisi='$divisi' && t_log.id_user='$id_user' ORDER BY nama_depan ASC";
		return $this->db->query($sql);
	}
	function export_7_log($tahun,$divisi,$type){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,rincian_log,progress_log,update_log,komentar
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where update_log LIKE '%$tahun%' && divisi='$divisi' && type='$type' ORDER BY nama_depan ASC";
		return $this->db->query($sql);
	}
	function export_8_log($tahun,$id_user,$type){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,divisi,type,rincian_log,progress_log,update_log,komentar
		FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user 
		where update_log LIKE '%$tahun%' && t_log.id_user='$id_user' && type='$type' ORDER BY nama_depan ASC";
		return $this->db->query($sql);
	}
	
	function send_nama($where,$table){
		$query = $this->db->select('')->from($table)->where($where)->get();
		return $query->row()->nama;
	}
	function send_password($where,$table){
		$query = $this->db->select('password')->from($table)->where($where)->get();
		return $query->row()->password;
	}
	function send_progres(){
		$email = $this->session->userdata('email');
		$query = $this->db->select('email_group')
		->from('t_data_utama')
		->like('email_group',$email,'both')
		->get();
		return $query->row()->email_group;
	}
	function email_notif($no){
		$query = $this->db->select('email_group')
		->from('t_data_utama')
		->where($no)
		->get();
		return $query->row()->email_group;
	}
	function email_komentar($where,$table){
		$query = $this->db->select('email_group')->from($table)->where($where)->get();
		return $query->row()->email_group;
	}
	function data_user(){
		$nama = $this->session->userdata('nama');
		$query = $this->db->select('email')->from('t_data_user')->where('nama_depan', $nama)->get();
		return $query->row()->email;
	}
	function tampil_detail_log($where,$table){
		return $this->db->get_where($table,$where);
	}
	function kirim_log($nama_project,$instansi){
		$id_user = $this->session->userdata('id_user');
		$email = $this->session->userdata('email');
		$sql = "SELECT * FROM t_log where id_user='$id_user' || email_group LIKE '%$email%' && nama_project='$nama_project' && instansi='$instansi' ORDER BY update_log DESC";
		return $this->db->query($sql);
	}
	function send_komentar($id_log){
		$sql = "SELECT t_data_user.nama_depan,nama_project,instansi,rincian_log,progress_log,komentar from t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user WHERE id_log='$id_log'";
		return $this->db->query($sql);
	}
	function tampil_data_staff(){
		$nama = $this->session->userdata('id_user');
		$sql = "SELECT * FROM t_data_utama where id_user='$id_user'";
		return $this->db->query($sql);
	}
	function tampil_data_report(){
		$id_login = $this->session->userdata('id_user');
		$email = $this->session->userdata('email');
		$sql = "SELECT t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE t_data_utama.status_project='running' && t_data_utama.id_user='$id_login' || t_data_utama.status_project='running' && t_data_utama.email_group LIKE '%$email%' ORDER BY last_update";
		return $this->db->query($sql);
	}
	function cf_work_staff(){
		$id_login = $this->session->userdata('id_user');
		$email = $this->session->userdata('email');
		$sql = "SELECT t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE  t_data_utama.id_user='$id_login' && t_data_utama.status_project='closed' || t_data_utama.status_project='failed'";
		return $this->db->query($sql);
	}
	function tampil_data_report_manager(){
		$id_login = $this->session->userdata('id_user');
		$email = $this->session->userdata('email');
		$sql = "SELECT t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user where t_data_utama.id_user='$id_login' && status_project='running' ORDER BY last_update DESC";
		return $this->db->query($sql);
	}
	function tampil_data_staff_lain(){
		$id_login = $this->session->userdata('id_user');
		$email = $this->session->userdata('email');
		$sql = "SELECT t_data_user.nama_depan,t_log.id_user,nama_project,instansi,email_group,update_log FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user where email_group LIKE '%$email%' GROUP BY t_log.id_user ORDER BY update_log DESC";
		return $this->db->query($sql);
	}
	function tampil_update_log($id_user,$nama_project,$instansi){
		$sql = "SELECT t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,email_group,type,divisi FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user where t_data_utama.id_user='$id_user' && nama_project='$nama_project' && instansi='$instansi'";
		return $this->db->query($sql);
	}
	function show_all_email(){
		$sql = "SELECT email FROM t_data_user";
		return $this->db->query($sql);
	}
	function update_status_aktif(){
		$sql = "UPDATE t_data_user SET status='1'";
		return $this->db->query($sql);
	}
	function update_status_non(){
		$sql = "UPDATE t_data_user SET status='0'";
		return $this->db->query($sql);
	}
	function tampil_daily_manager(){
		$sql = "SELECT t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE status_project='running' ORDER BY last_update DESC";
		return $this->db->query($sql);
	}
	function tampil_daily_direktur(){
		$sql = "SELECT t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE status_project='running' ORDER BY last_update DESC";
		return $this->db->query($sql);
	}
	function tampil_daily_report(){
		$sql = "SELECT no,t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE status_project='running' ORDER BY last_update DESC";
		return $this->db->query($sql);
	}
	function tampil_cf_work(){
		$sql = "SELECT no,t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE status_project='closed' || status_project='failed' ORDER BY last_update DESC";
		return $this->db->query($sql);
	}
	function tampil_cf_work_staf(){
		$id_user = $this->session->userdata('id_user');
		$sql = "SELECT no,t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE id_user='$id_user' && status_project='closed' || status_project='failed' ORDER BY last_update DESC";
		return $this->db->query($sql);
	}
	function tampil_daily_report_admin(){
		$sql = "SELECT no,t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE t_data_utama.status_project='running' ORDER BY last_update DESC ";
		return $this->db->query($sql);
	}
	function tampil_notif(){
		date_default_timezone_set("Asia/Bangkok"); 
		$tahun = date ("Y");
		$sql = "SELECT no,t_data_user.nama_depan,t_data_utama.id_user,nama_project,instansi,email_group,progres,last_update,status_project FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE status_project ='running' && DATEDIFF(CURRENT_DATE(),last_update) > 5 && last_update LIKE '%$tahun%' ORDER BY last_update ASC";
		return $this->db->query($sql);
	}
	function divisi(){
		$sql = "SELECT * FROM t_department";
		return $this->db->query($sql);
	}
	function tampil_nama_staff(){
		$sql = "SELECT * FROM t_data_user WHERE level ='staff' || level='manager' || level='admin'";
		return $this->db->query($sql);
	}
	function nama_staff_khusus(){
		$email = $this->session->userdata('email');
		$sql = "SELECT id_user,nama_depan FROM t_data_user WHERE email='$email'";
		return $this->db->query($sql);
	}
	function tampil_dept(){
		return $this->db->get('t_department');
	}
	function tampil_staff(){
		return $this->db->get('t_data_user');
	}
	function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function delete($id_log){
		$sql = "UPDATE t_log SET file='' WHERE id_log='$id_log'";
		return $this->db->query($sql);
	}
	function input_data($data,$table){
		return $this->db->insert($table,$data);
	}
	function input_data_email($data){
		$this->db->insert('t_data_utama',$data);
	}
	function input_data_komen($where,$table,$data){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function edit_data($where,$table){
		return $this->db->get_where($table,$where);
	}
	function edit_data_utama($no){
		$sql = "SELECT t_data_user.nama_depan,t_data_utama.id_user,no,kode_project,nama_project,instansi,type
		,divisi,esti_pendapatan,real_pendapatan,no_spk,email_group,pic_instansi,no_telp FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE no='$no'";
		return $this->db->query($sql);
	}
	function data_detail($where,$table){
		return $this->db->get_where($table,$where);
	}
	function target(){
		$sql = "SELECT t_data_user.nama_depan,id_target,nilai,tanggal FROM t_data_user INNER JOIN t_target_perusahaan ON t_data_user.id_user=t_target_perusahaan.id_user ORDER BY tanggal DESC";
		return $this->db->query($sql);
	}
	function target_total(){
		$sql = "SELECT id_total,nilai,tanggal FROM t_total_target ORDER BY tanggal DESC";
		return $this->db->query($sql);
	}
	function jumlah_target(){
		date_default_timezone_set("Asia/Bangkok"); 
		$tahun = date ("Y");
		$sql = "SELECT nilai FROM t_total_target WHERE tanggal LIKE '%$tahun%'";
		return $this->db->query($sql);
	}
	function show_target($id_target){
		date_default_timezone_set("Asia/Bangkok"); 
		$tanggal = date ("Y-m-d");
		$query = $this->db->select('nilai')->from('t_target_perusahaan')->where('id_target',$id_target)->like('tanggal',$tanggal,'both')->get();
		return $query->row()->nilai;
	}
	function prog_detail($where,$table){
		return $this->db->get_where($table,$where);
	}
	function prog_detail_manager($where,$table){
		return $this->db->get_where($table,$where);
	}
	function prog_detail_admin($id_user,$instansi,$nama_project){
		$query = "SELECT t_data_user.nama_depan,t_log.id_user,t_log.id_log,nama_project,instansi,rincian_log,update_log,file,progress_log,komentar FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user WHERE t_log.id_user='$id_user' && instansi='$instansi' && nama_project='$nama_project' ORDER BY update_log DESC";
		return $this->db->query($query);
	}
	function log_detail($id_user,$nama_project,$instansi){
		$query = "SELECT t_data_user.nama_depan,t_log.id_user,t_log.id_log,nama_project,instansi,rincian_log,update_log,file,progress_log,komentar FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user WHERE t_log.id_user='$id_user' && instansi='$instansi' && nama_project='$nama_project' ORDER BY update_log DESC";
		return $this->db->query($query);
	}
	function log_detail_lain($id_user,$nama_project,$instansi,$email){
		$query = "SELECT t_data_user.nama_depan,t_log.id_user,t_log.id_log,nama_project,instansi,rincian_log,update_log,file,progress_log,komentar FROM t_data_user INNER JOIN t_log ON t_data_user.id_user=t_log.id_user WHERE email_group LIKE '%$email%' && t_log.id_user='$id_user' && nama_project='$nama_project' && instansi='$instansi' ORDER BY update_log DESC";
		return $this->db->query($query);
	}
	function select_gambar(){
		$query = $this->db->select('nama')->from('foto')->get();
		return $query->row()->nama;
	}
	function show_file($id_log){
		$query = $this->db->select('file')->from('t_log')->where('id_log',$id_log)->get();
		return $query->row()->file;
	}
	function show_comment($id_log){
		$sql = "SELECT * FROM t_log WHERE id_log='$id_log'";
		return $this->db->query($sql);
	}
	function show_detail($id_log){
		// $query = $this->db->select('komentar')->from('t_log')->where('id_log',$id_log)->get();
 		//return $query->row()->komentar;
		$sql = "SELECT * FROM t_log WHERE id_log='$id_log'";
		return $this->db->query($sql);
	}
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function update_email($data,$table){
		$this->db->update($table,$data);
	}
	function cek_level($data){
		$staff_lain = "staff_lain";
		$this->db->select('id_user');
		$this->db->from('t_data_user');
		$this->db->where($data);
		$this->db->where('level',$staff_lain);
		$query = $this->db->get();
		return $query->result();

		//$query = $this->db->select('level')->from('t_data_user')->where($data);
		//return $query;
	}
	function select_level($d_hasil){
		$this->db->select('level');
		$this->db->from('t_data_user');
		$this->db->where($d_hasil);
		$query = $this->db->get();
		return $query->result();
	}
	function select_id_log($id_user_last,$nama_project_last,$instansi_last){
		$query = $this->db->select('id_log')
		->from('t_log')
		->where('id_user',$id_user_last)
		->where('nama_project',$nama_project_last)
		->where('instansi',$instansi_last)
		->get();
		return $query->row()->id_log;
	}
	function update_profil_data_utama($nama_baru,$nama_lama){
		$sql = "UPDATE t_data_utama SET id_user = '$nama_baru' WHERE id_user = '$nama_lama'";
		return $this->db->query($sql);
	}
	function update_profil_log($nama_baru,$nama_lama){
		$sql = "UPDATE t_log SET id_user = '$nama_baru' WHERE id_user = '$nama_lama'";
		return $this->db->query($sql);
	}
	function tambah_log($data,$table){
		return $this->db->insert($table,$data);
	}
	function pilihan_type(){
		$this->db->select('*');
		$this->db->from('t_type');
		$query = $this->db->get();
		return $query->result();
	}
	function jumlah_new($tahun){
		$sql = "SELECT COUNT(type) AS tot_new FROM t_data_utama WHERE type='New' && tanggal LIKE '%$tahun%' ";
		return $this->db->query($sql);
	}
	function jumlah_exist($tahun){
		$sql = "SELECT COUNT(type) AS tot_exist FROM t_data_utama WHERE type='Existing' && tanggal LIKE '%$tahun%'";
		return $this->db->query($sql);
	}
	function jumlah_up($tahun){
		$sql = "SELECT COUNT(type) AS tot_up FROM t_data_utama WHERE type='Upgrade' && tanggal LIKE '%$tahun%'";
		return $this->db->query($sql);
	}
	function jumlah_down($tahun){
		$sql = "SELECT COUNT(type) AS tot_down FROM t_data_utama WHERE type='Downgrade' && tanggal LIKE '%$tahun%'";
		return $this->db->query($sql);
	}
	function jumlah_estimasi($tahun){
		$sql = "SELECT SUM(esti_pendapatan) as tot_esti from t_data_utama WHERE tanggal LIKE '%$tahun%' ";
		return $this->db->query($sql);
	}
	function jumlah_real($tahun){
		$sql = "SELECT SUM(real_pendapatan) as tot_real from t_data_utama WHERE tanggal LIKE '%$tahun%'";
		return $this->db->query($sql);
	}
	function jumlah_estimasi_staff($tahun){
		$id_login = $this->session->userdata('id_user');
		$sql = "SELECT SUM(esti_pendapatan) as tot_esti from t_data_utama WHERE tanggal LIKE '%$tahun%' && id_user='$id_login'";
		return $this->db->query($sql);
	}
	function jumlah_real_staff($tahun){
		$id_login = $this->session->userdata('id_user');
		$sql = "SELECT SUM(real_pendapatan) as tot_real from t_data_utama WHERE tanggal LIKE '%$tahun%' && id_user='$id_login'";
		return $this->db->query($sql);
	}
	function tabel_grafik($tahun){
		$query = "SELECT t_data_user.nama_depan,SUM(t_data_utama.real_pendapatan) AS tot_real FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user WHERE tanggal LIKE '%$tahun%' GROUP BY nama_depan ORDER BY tot_real DESC";
		return $this->db->query($query)->result();
	}
	function grafik($tahun){
		$query = "SELECT t_data_user.nama_depan,SUM(t_data_utama.real_pendapatan) AS tot_real,COUNT(t_data_utama.real_pendapatan) AS baris_real FROM t_data_user INNER JOIN t_data_utama ON t_data_user.id_user = t_data_utama.id_user WHERE tanggal LIKE '%$tahun%' GROUP BY nama_depan ORDER BY tot_real;";
		return $this->db->query($query)->result();
	}
	function prosen_tot_real(){
		$query = "SELECT id_user,SUM(real_pendapatan) AS tot_real,COUNT(real_pendapatan) AS baris_real FROM t_data_utama WHERE tanggal LIKE '%$tahun%' GROUP BY id_user ORDER BY tot_real";
		return $this->db->query($query)->result();
	}
	function grafik_pie($tahun){
		$sql = "SELECT SUM(real_pendapatan) as tot_real FROM t_data_utama WHERE tanggal LIKE '%$tahun%'";

		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row_array();
			$query->free_result();
			return $result;
		}
		return array();
	}
	function total_perusahaan($tahun){
		$sql = "SELECT nilai FROM t_total_target WHERE tanggal LIKE '%$tahun%'";
		
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row_array();
			$query->free_result();
			return $result;
		}
		return array();
	}
	// function grafik_pie($tahun){
	// 	$sql = "SELECT SUM(esti_pendapatan) as tot_esti,SUM(real_pendapatan) as tot_real from t_data_utama WHERE tanggal LIKE '%$tahun%'";
	// 	$query = $this->db->query($sql);
	// 	if ($query->num_rows() > 0) {
	// 		$result = $query->row_array();
	// 		$query->free_result();
	// 		return $result;
	// 	}
	// 	return array();
	// }
	function grafik_donut($tahun){
		$sql = "SELECT divisi,last_update,SUM(real_pendapatan) as tot_real from t_data_utama WHERE tanggal LIKE '%$tahun%' GROUP BY divisi ORDER BY tot_real DESC ";
		return $this->db->query($sql)->result();
	}
	function grafik_pie_staff($tahun){
		$id_login = $this->session->userdata('id_user');
		$sql = "SELECT 
		(
		SELECT SUM(nilai)
		FROM   t_target_perusahaan WHERE id_user='$id_login'
		) AS tot_nilai,
		(
		SELECT SUM(real_pendapatan)
		FROM   t_data_utama WHERE tanggal LIKE '%$tahun%' && id_user='$id_login'
		) AS tot_real";
		
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row_array();
			$query->free_result();
			return $result;
		}
		return array();
	}
	function coba(){
		$sql = "SELECT id_user,SUM(esti_pendapatan) as tot_esti,SUM(real_pendapatan) as tot_real FROM t_data_utama";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row_array();
			$query->free_result();
			return $result;
		}
		return array();
	}
	function seleksi_email($email){
		$query = $this->db->query("SELECT email FROM t_data_user WHERE email='$email'");
		return $query->num_rows();
	}
	function lihat_nama($nama_baru){
		$query = $this->db->select('nama')->from('t_data_user')->where('nama',$nama_baru)->get();
		return $query->row();
	}
	function update_log_utama($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);	
	}
	function show_profil(){
		$id_user = $this->session->userdata('id_user');
		$sql = "SELECT * FROM t_data_user WHERE id_user='$id_user'";
		return $this->db->query($sql);
	}
	function file($id){
		$query = $this->db->select('email')->from('t_data_user')->where('id_user',$id)->get();
		return $query->row()->email;
	}
	function select_email($d_hasil){
		$this->db->select('email');
		$this->db->from('t_data_user');
		$this->db->where($d_hasil);
		$query = $this->db->get();
		return $query->row()->email;
	}
	function detail_spk(){
		$sql = "SELECT t_data_user.nama_depan,t_target_perusahaan.nilai,t_target_perusahaan.tanggal,SUM(real_pendapatan) AS tot_spk FROM t_data_user INNER JOIN t_target_perusahaan ON t_data_user.id_user=t_target_perusahaan.id_user INNER JOIN t_data_utama ON t_data_user.id_user=t_data_utama.id_user GROUP BY t_data_utama.id_user";
		return $this->db->query($sql);
	}
	function detail_spk_user(){
		$id_user = $this->session->userdata('id_user');
		$this->db->select('t_target_perusahaan.nilai');
		$this->db->select('SUM(real_pendapatan) AS tot_spk');
		$this->db->from('t_data_user');
		$this->db->join('t_target_perusahaan', 't_data_user.id_user= t_target_perusahaan.id_user');
		$this->db->join('t_data_utama', 't_data_user.id_user= t_data_utama.id_user');
		$this->db->where('t_target_perusahaan.id_user',$id_user);
		$query = $this->db->get();
		return $query->result_array();
	}
	function plan_pribadi($tahun){
		$id_user = $this->session->userdata('id_user');
		$sql = "SELECT nilai FROM t_target_perusahaan WHERE id_user='$id_user' && tanggal LIKE '%$tahun%'";
		return $this->db->query($sql);
	}
}
