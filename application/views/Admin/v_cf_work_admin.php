<html lang="en">
<head>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">

</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="admin">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
       <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama'); ?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Admin/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
     <?php
     foreach ($profil as $v) {
      if($v->picture == ""){
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }else{
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }
    }
    ?>
    <div>
      <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
      <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
    </div>
  </div>

      <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="admin">
          <i class="app-menu__icon fas fa-chart-line"></i>
          <span class="app-menu__label">Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-list-ul"></i>
          <span class="app-menu__label">Data</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/daily_report_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Daily Report
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/data_tabel_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Tambah Project
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/cf_work_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              CF Work
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-cog"></i>
          <span class="app-menu__label">Settings</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/daftar_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Data Staff
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/data_department_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Department
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-swatchbook"></i>
          <span class="app-menu__label">Laporan</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/laporan_log')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Export Log
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/laporan_project')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Export Project
            </a>
          </li>
        </ul>
      </li>
    </ul>
</aside>

<main class="app-content">
  <div class="app-title">
    <div>
      <h5>Daily Report</h5>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-list-ul fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">Daily Report</a></li>
    </ul>
  </div>

  <div class="card-body" style="background-color: #f5f6fa">
    <div class="table-responsive">
      <?php
      $message = $this->session->flashdata('message');
      $message_error = $this->session->flashdata('message_error');
      if (isset($message)) {
        echo '<div class="alert alert-success alert-dismissible fade show" id="success-alert">
        '.$message.'</div>';
        $this->session->unset_userdata('message');
      }
      if (isset($message_error)) {
        echo '<div class="alert alert-danger alert-dismissible fade show" id="success-alert">
        '.$message.'</div>';
        $this->session->unset_userdata('message_error');
      }
      ?>
      <table id="example" class="table" style="width:100%;">
        <thead style="background-color: #f6e58d;">
          <tr>
            <th>No</th>
            <th>Nama PIC</th>
            <th>Nama Project</th>
            <th>Instansi</th>
            <th>Status</th>
            <th>Progress</th>
            <th>Tanggal Update</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          <?php
          $no_urut = 1;
          foreach ($t_data_report as $hasil) {
            $tgl_akhir  = new DateTime(date(".".$hasil->last_update.""));
            $tgl_hitung  = new DateTime(date("Y-m-d H:i:s"));
            $difference = $tgl_hitung->diff($tgl_akhir);
            ?>
            <tr>
              <td><?php echo $no_urut++ ?></td>
              <td><?php echo $hasil->nama_depan ?></td>
              <td><?php echo $hasil->nama_project ?></td>
              <td><?php echo $hasil->instansi ?></td>
              <?php
              if($hasil->status_project == "running" && $difference->days > 5){
                ?>
                <td><center><span class="badge" style="background-color: #e58e26;color: white;">running</span></center></td>
                <?php
              }else if($hasil->status_project == "running"){
                ?>
                <td><center><span class="badge badge-success">running</span></center></td>
                <?php
              }
              else if($hasil->status_project == "closed"){
                ?>
                <td><center><span class="badge badge-info">closed</span></center></td>
                <?php
              }else{
                ?>
                <td><center><span class="badge badge-danger">failed</span></center></td>
                <?php
              }
              ?>
              <td><?php echo $hasil->progres."%" ?></td>
              <?php
              if($hasil->status_project == "closed"){
                ?>
                <td>
                  <center><span class="badge badge-info"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }else if($hasil->status_project == "failed"){
                ?>
                <td>
                  <center><span class="badge badge-danger"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }else if($difference->days > 5){
                ?>
                <td>
                  <center><span class="badge" style="background-color: #e58e26;color: white;"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }
              else{
                ?>
                <td>
                  <center><span class="badge badge-success"><?php echo $hasil->last_update ?></span></center>
                  <?php
                }
                ?>
                <td>
                  <center>
                    <div class="btn-group">
                      <form action="detail_project_admin" method="post" >
                        <input type="hidden" name="instan" value="<?php echo $hasil->instansi;?>">
                        <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                        <input type="hidden" name="project" value="<?php echo $hasil->nama_project;?>">
                        <button type="submit" class="btn btn-default btn-sm" style="background-color: #f6e58d;width:100%;">Detail</button>
                      </form> 
                      <form action="update_prog" method="post">
                        <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                        <input type="hidden" name="nama_project" value="<?php echo $hasil->nama_project?>">
                        <input type="hidden" name="instansi" value="<?php echo $hasil->instansi?>">
                        <button type="submit" class="btn btn-info btn-sm"><i class="far fa-edit"></i>Update</button>
                      </form>      
                    </div>                 
                  </center>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
      </div>
      <fieldset style="border: 1px solid #ddd !important;
      margin: 0;
      xmin-width: 0;
      padding: 10px;       
      position: relative;
      border-radius:4px;
      background-color:white;
      padding-left:10px!important;">
      <legend style="font-size:18px;
      font-weight:bold;
      margin-bottom: 0px; 
      width: 35%; 
      border: 1px solid #ddd;
      border-radius: 4px; 
      padding: 5px 5px 5px 10px; 
      background-color: #ffffff;">Keterangan Progress</legend>
      <table>
        <tr>
          <th scope="row" style="background-color: #C2185B;color: white;">Fase 1</th>
          <td>&nbsp; - &nbsp;Sudah ketempat klien tapi belum bertemu pemilik kerjaan</td>
        </tr>
        <tr>
          <th scope="row" style="background-color:#D81B60;color: white;">0 s/d 20 %</th>
          <td>&nbsp; - &nbsp;Sudah bertemu pemilik kerjaan dan sudah membahas kebutuhan IT</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah follow up yang pertama</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah follow up yang kedua</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #303F9F;color: white"><strong>Fase 2</strong></th>
          <td>&nbsp; - &nbsp;Sudah memberikan penawaran, RAB, KAK, pertama</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #3949AB;color: white">20 s/d 40 %</th>
          <td>&nbsp; - &nbsp;Sudah memberikan revisi RAB, KAK</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah memberikan revisi RAB, KAK kedua</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah survey pekerjaan dan tempat</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #0097A7;color: white">Fase 3</th>
          <td>&nbsp; - &nbsp;Sudah membicarakan untuk proses dan strategi lelang/PL</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #00ACC1;color: white">40 s/d 60 %</th>
          <td>&nbsp; - &nbsp;Sedang proses pembuatan administrati lelang/PL</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sedang proses Lelang/PL</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #AFB42B;color: white">Fase 4</th>
          <td>&nbsp; - &nbsp;Sudah proses kontrak</td>

        </tr>
        <tr>
          <th scope="row" style="background-color: #C0CA33;color: white">60 s/d 80 %</th>
          <td>&nbsp; - &nbsp;Sedang proses pekerjaan pendahuluan</td>

        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah proses penagihan pertama</td>

        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sedang proses perkembangan pekerjaan dan proses penagihan terakhir</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #F57C00;color: white">Fase 5</th>
          <td>&nbsp; - &nbsp;Pekerjan sudah selesai</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #FB8C00;color: white">80 s/d 100 %</th>
        </tr>
      </table>
    </fieldset>
  </div>

  <!-- The Modal -->
  <div class="modal hide fade" id="update_stf" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update Progress Project</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <p id="update_staff"></p>
        </div>
      </div>
    </div>
  </div>

</main>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      "autoWidth": false,
      "pageLength": 10,
      "bLengthChange": false,
      "bInfo" : false,
      "dom": '<"toolbar">frtip',
      "scrollY":        "50%",
      "scrollCollapse": true,
      "scrollX": true
    });
    $(".update").submit(function(event){
              event.preventDefault(); //prevent default action 
              $(".update").attr("disabled", true);
              var post_url = $(this).attr("action"); //get form action url
              var request_method = $(this).attr("method"); //get form GET/POST method
              var form_data = $(this).serialize(); //Encode form elements for submission
              console.log(form_data);
              $.ajax({
                url : post_url,
                type: request_method,
                data : form_data
              }).done(function(response){
                $("#update_staff").html(response);
                $(".update").attr("disabled", false);
              });
            });
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
      $("#success-alert").slideUp(500);
    });
  });
</script>
<style type="text/css">
.bubblingG {
  text-align: center;
  width:78px;
  height:49px;
  margin: auto;
}

.bubblingG span {
  display: inline-block;
  vertical-align: middle;
  width: 10px;
  height: 10px;
  margin: 24px auto;
  background: rgb(41,194,143);
  border-radius: 49px;
  -o-border-radius: 49px;
  -ms-border-radius: 49px;
  -webkit-border-radius: 49px;
  -moz-border-radius: 49px;
  animation: bubblingG 0.905s infinite alternate;
  -o-animation: bubblingG 0.905s infinite alternate;
  -ms-animation: bubblingG 0.905s infinite alternate;
  -webkit-animation: bubblingG 0.905s infinite alternate;
  -moz-animation: bubblingG 0.905s infinite alternate;
}

#bubblingG_1 {
  animation-delay: 0s;
  -o-animation-delay: 0s;
  -ms-animation-delay: 0s;
  -webkit-animation-delay: 0s;
  -moz-animation-delay: 0s;
}

#bubblingG_2 {
  animation-delay: 0.2695s;
  -o-animation-delay: 0.2695s;
  -ms-animation-delay: 0.2695s;
  -webkit-animation-delay: 0.2695s;
  -moz-animation-delay: 0.2695s;
}

#bubblingG_3 {
  animation-delay: 0.549s;
  -o-animation-delay: 0.549s;
  -ms-animation-delay: 0.549s;
  -webkit-animation-delay: 0.549s;
  -moz-animation-delay: 0.549s;
}



@keyframes bubblingG {
  0% {
    width: 10px;
    height: 10px;
    background-color:rgba(41,158,152,0.98);
    transform: translateY(0);
  }

  100% {
    width: 23px;
    height: 23px;
    background-color:rgb(255,255,255);
    transform: translateY(-20px);
  }
}

@-o-keyframes bubblingG {
  0% {
    width: 10px;
    height: 10px;
    background-color:rgba(41,158,152,0.98);
    -o-transform: translateY(0);
  }

  100% {
    width: 23px;
    height: 23px;
    background-color:rgb(255,255,255);
    -o-transform: translateY(-20px);
  }
}

@-ms-keyframes bubblingG {
  0% {
    width: 10px;
    height: 10px;
    background-color:rgba(41,158,152,0.98);
    -ms-transform: translateY(0);
  }

  100% {
    width: 23px;
    height: 23px;
    background-color:rgb(255,255,255);
    -ms-transform: translateY(-20px);
  }
}

@-webkit-keyframes bubblingG {
  0% {
    width: 10px;
    height: 10px;
    background-color:rgba(41,158,152,0.98);
    -webkit-transform: translateY(0);
  }

  100% {
    width: 23px;
    height: 23px;
    background-color:rgb(255,255,255);
    -webkit-transform: translateY(-20px);
  }
}

@-moz-keyframes bubblingG {
  0% {
    width: 10px;
    height: 10px;
    background-color:rgba(41,158,152,0.98);
    -moz-transform: translateY(0);
  }

  100% {
    width: 23px;
    height: 23px;
    background-color:rgb(255,255,255);
    -moz-transform: translateY(-20px);
  }
}

.circle {
  border-radius: 100px !important;
  overflow: hidden;
  width: 65px;
  height: 65px;
  left: 10%;
  border: 0px solid rgba(255, 255, 255, 0.7);
}


</style>
</body>
</html>
