<!DOCTYPE html>
<html>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

form {
  border: 3px solid #f1f1f1;
  font-family: Arial;
}

.container {
  padding: 20px;
  background-color: #f1f1f1;
}

input[type=text], input[type=submit] {
  width: 100%;
  padding: 12px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

input[type=checkbox] {
  margin-top: 16px;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  border: none;
}

input[type=submit]:hover {
  opacity: 0.8;
}
</style>
<body>
  <table border="0">
    <tr>
      <td align="right">
        <img src="https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/14691064_1126674984081042_7683922444718356585_n.png?_nc_cat=0&oh=8da3a15e5c5227e8b686de6905046eec&oe=5BD20F44" class="img-responsive" width="20%">
        <td align="left">
          <h3>PT.TIME EXCELINDO</h3>
        </td>
      </td>
    </tr>
  </table>
  <hr>
  <div id="container" style="margin-top: 2%;">
    <div class="row">
      <table>
        <tr>
          <label>Hello <b><?php if(isset($nama)) { echo $nama; }?></b>,This is your account</label>
        </tr>
        <tr>
          <td><h4>Your Email Login</h4></td>
          <td><?php if(isset($email)) { echo $email; }?></td>
        </tr>
        <tr>
          <td><h4>Your Password</h4></td>
          <td><?php if(isset($password)) { echo $password; }?></td>
        </tr>
      </table>
    </div>
  </div>
</body>
</html>