<!DOCTYPE html>
<html>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

form {
  border: 3px solid #f1f1f1;
  font-family: Arial;
}

.container {
  padding: 20px;
  background-color: #f1f1f1;
}

input[type=text], input[type=submit] {
  width: 100%;
  padding: 12px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

input[type=checkbox] {
  margin-top: 16px;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  border: none;
}

input[type=submit]:hover {
  opacity: 0.8;
}
</style>
<body>
  <table width="100%" border="0">
    <tr>
      <td width="50%"align="right">
        <img src="https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/14691064_1126674984081042_7683922444718356585_n.png?_nc_cat=0&oh=8da3a15e5c5227e8b686de6905046eec&oe=5BD20F44" class="img-responsive" width="20%">
      </td>
      <td width="50%"align="left">
        <h3>PT.TIME EXCELINDO</h3>
      </td>
    </tr>
  </table>
  <hr>
  <table class="nothing" align="center">
   <tr>
     <th align="left">Send By</th>
     <td>Computer</td>
   </tr>
   <tr>
     <th align="left">Information</th>
     <td>Segera update pekerjaan!</td>
   </tr>
   <tr>
     <th align="left">Terakhir Update Project Pekerjaan</th>
     <td><?php if(isset($tanggal)) { echo $tanggal; }?></td>
   </tr>
   <tr>
     <th align="left">Nama PIC</th>
     <td><?php if(isset($nama_pic)) { echo $nama_pic; }?></td>
   </tr>
   <tr>
     <th align="left">Nama Project</th>
     <td><?php if(isset($nama_project)) { echo $nama_project; }?></td>
   </tr>  
   <tr>
     <th align="left">Instansi</th>
     <td><?php if(isset($instansi)) { echo $instansi; }?></td>
   </tr> 
 </table>
</body>
</html>