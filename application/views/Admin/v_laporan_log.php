<html lang="en">
<head>
  <?php
  date_default_timezone_set("Asia/Jakarta");
  function rupiah($angka){
    $hasil_rupiah = "" . number_format($angka,0,',','.');
    return $hasil_rupiah;
  }
  ?>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="admin">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
        <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama'); ?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Admin/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
     <?php
     foreach ($profil as $v) {
      if($v->picture == ""){
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }else{
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }
    }
    ?>
    <div>
      <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
      <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
    </div>
  </div>

  <ul class="app-menu">
    <li>
      <a class="app-menu__item active" href="admin">
        <i class="app-menu__icon fas fa-chart-line"></i>
        <span class="app-menu__label">Dashboard</span>
      </a>
    </li>
    <li class="treeview">
      <a class="app-menu__item" href="" data-toggle="treeview">
        <i class="app-menu__icon fas fa-list-ul"></i>
        <span class="app-menu__label">Data</span>
        <i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li>
          <a class="treeview-item" href="<?php echo base_url('Admin/daily_report_admin')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Daily Report
          </a>
        </li>
        <li>
          <a class="treeview-item" href="<?php echo base_url('Admin/data_tabel_admin')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Tambah Project
          </a>
        </li>
        <li>
          <a class="treeview-item" href="<?php echo base_url('Admin/cf_work_admin')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            CF Work
          </a>
        </li>
      </ul>
    </li>

    <li class="treeview">
      <a class="app-menu__item" href="" data-toggle="treeview">
        <i class="app-menu__icon fas fa-cog"></i>
        <span class="app-menu__label">Settings</span>
        <i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li>
          <a class="treeview-item" href="<?php echo base_url('Admin/daftar_staff')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Data Staff
          </a>
        </li>
        <li>
          <a class="treeview-item" href="<?php echo base_url('Admin/data_department_admin')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Department
          </a>
        </li>
      </ul>
    </li>

    <li class="treeview">
      <a class="app-menu__item" href="" data-toggle="treeview">
        <i class="app-menu__icon fas fa-swatchbook"></i>
        <span class="app-menu__label">Laporan</span>
        <i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li>
          <a class="treeview-item" href="<?php echo base_url('Admin/laporan_log')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Export Log
          </a>
        </li>
        <li>
          <a class="treeview-item" href="<?php echo base_url('Admin/laporan_project')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Export Project
          </a>
        </li>
      </ul>
    </li>
  </ul>

</aside>

<main class="app-content">
  <div class="app-title">
    <div>
      <h5>Export log</h5>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-list-ul fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">Export Log</a></li>
    </ul>
  </div>

  <div class="card-body" style="background-color: #f5f6fa">
    <form action="export_excel_log" method="post">
      <table width="100%">
        <tr>
          <td align="right">
            <label for="usr">Tahun</label>
          </td>
          <td colspan="2" align="center">
            <div class="col-sm-10">
              <select class="th_log js-example-basic-single" name="tahun_log" style="width:100%;">
                <option value="" selected="selected">--Pilih--</option>
                <?php  
                $thn_skr = date('Y');
                for ($x = $thn_skr; $x >= 2018; $x--) {
                  ?>
                  <option value="<?php echo $x ?>"><?php echo $x ?></option>
                  <?php
                }
                ?>
              </select>
            </div>
          </td>
        </tr>

        <tr>  
          <td align="right">
            <label for="usr">Divisi</label>
          </td>
          <td colspan="2"  align="center">
            <div class="col-sm-10">
              <select id="divisi_log" class="divisi_log js-example-basic-single" name="divisi_log" style="width:100%;">
              </select>
            </div>
          </td>
        </tr>

        <tr>
          <td align="right">
            <label for="usr">Nama Pic</label>
          </td>
          <td colspan="2"  align="center">
            <div class="col-sm-10">
              <select id="pic_log" class="pic_log js-example-basic-single" name="pic_log" style="width:100%;">
              </select>
            </div>
          </td>
        </tr>

        <tr>
          <td width="20%;" align="right">
            <label for="usr">Type</label>
          </td>
          <td colspan="2"  align="center">        
            <div class="col-sm-10">
              <select id="type_log" class="js-example-basic-single" name="type_log" style="width:100%;">
              </select>
            </div>
          </td>
        </tr>
        <tr>        
          <td align="center" colspan="2">
            <div class="col-sm-10">
              <label for="usr"></label><br>
              <button type="submit" class="btn btn-info btn-sm"><i class="fas fa-download"></i> Download</button>
            </div>
          </td>
        </tr>
      </table> 
    </form>
  </div>
</main>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.41.js"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/my.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
   $('#example').DataTable({
    "autoWidth": false,
    "pageLength": 10,
    "bLengthChange": false,
    "bInfo" : false,
    "dom": '<"toolbar">frtip',
    "scrollY":        "50%",
    "scrollCollapse": true,
    "scrollX": true
  });
   $('.js-example-basic-single').select2({ width: 'resolve' });
 });

  $(".testInput").autoNumeric('init', {
    aSep: '.', 
    aDec: ',',
    aForm: true,
    vMax: '999999999999999',
    vMin: '-9'
  });

  $('.update_project').submit(function(event){
          event.preventDefault(); //prevent default action 
          $('.update').attr('disabled', true);
          var post_url = $(this).attr('action'); //get form action url
          var request_method = $(this).attr('method'); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          if($('form')[0].checkValidity()) {
            $.ajax({
              url : post_url,
              type: request_method,
              data : form_data
            }).done(function(response){
              swal('Success!', 'Data berhasil diupdate', 'success').then(function(){
                window.location = 'data_tabel_admin';
              });
              $('.update').attr('disabled', false);
            });
          }
        });

  $('#myModal').on('hidden.bs.modal', function () {
   location.reload();
 })

      // edit data project
      // 
      $(document).on("click", ".edit", function () {
       $(".card-body #no_h").val($(this).data('no_h')).change();
       $(".card-body #id_pic_h").val($(this).data('id_pic_h')).change();
       $(".card-body #id_user_last_h").val($(this).data('id_user_last_h')).change();
       $(".card-body #kode_project_h").val($(this).data('kode_project_h')).change();
       $(".card-body #nama_project_h").val($(this).data('nama_project_h')).change();
       $(".card-body #nama_project_last_h").val($(this).data('nama_project_last_h')).change();
       $(".card-body #esti_pendapatan_h").val($(this).data('esti_pendapatan_h')).change();
       $(".card-body #instansi_h").val($(this).data('instansi_h')).change();
       $(".card-body #instansi_last_h").val($(this).data('instansi_last_h')).change();
       $(".card-body #type_h").val($(this).data('type_h')).change();
       $(".card-body #divisi_h").val($(this).data('divisi_h')).change();
       $(".card-body #real_pendapatan_h").val($(this).data('real_pendapatan_h')).change();
       $(".card-body #pic_instansi_h").val($(this).data('pic_instansi_h')).change();
       $(".card-body #no_telp_h").val($(this).data('no_telp_h')).change();
     });

      $(".tambah_project").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          if($("form")[0].checkValidity()) {
            $.ajax({
              url : post_url,
              type: request_method,
              data : form_data
            }).done(function(response){
              swal("Success!", "Data berhasil diinputkan", "success").then(function(){
                window.location = 'data_tabel_admin';
              });
            });
          }
        });

      // $(".edit_data").submit(function(event){
      //     event.preventDefault(); //prevent default action 
      //     $(".edit_data").attr("disabled", true);
      //     var post_url = $(this).attr("action"); //get form action url
      //     var request_method = $(this).attr("method"); //get form GET/POST method
      //     var form_data = $(this).serialize(); //Encode form elements for submission
      //     console.log(form_data);
      //     $.ajax({
      //       url : post_url,
      //       type: request_method,
      //       data : form_data
      //     }).done(function(response){
      //       $("#edit").html(response);
      //       $(".edit_data").attr("disabled", false);
      //     });
      //   });

      $(".delete_project").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this row file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((response) => {
            if (response) {
              $.ajax({
                url : post_url,
                type: request_method,
                data : form_data
              }).done(function(response){
                swal("Poof! your project has been deleted!",{
                  icon: "success",
                }).then(function(){
                  window.location = 'data_tabel_admin';
                });
              });
            }else {
              swal("Your your project is safe!").then(function() {
                window.location = 'data_tabel_admin';
              });
            }
          });
        });

      $(".delete_target").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this row file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((response) => {
            if (response) {
              $.ajax({
                url : post_url,
                type: request_method,
                data : form_data
              }).done(function(response){
                swal("Poof! your project has been deleted!",{
                  icon: "success",
                }).then(function(){
                  window.location = 'data_tabel_admin';
                });
              });
            }else {
              swal("Your your project is safe!").then(function() {
                window.location = 'data_tabel_admin';
              });
            }
          });
        });

      $("#targetForm").submit(function(e) {
        // prevent default submit behaviour 
        e.preventDefault();
        // serialize total form data
        var postData = $(this).serializeArray();
        // get form action url
        var formActionURL = $(this).attr("action");
        $("#submit").val('please wait...');
        // JQuery ajax method , for post we can directly use $.post({}); this is shortcut method for sending post request 
        $.ajax({
          url: formActionURL,
          type: "POST",
          data: postData,
        }).done(function(respond) {
          $("#show_target").html(respond);
          swal({
            title: "Success!",
            text: "Data berhasil diinputkan",
            icon: "success",
            button: "Ok",
          }).then(function() {
            window.location = "data_tabel_admin";
          });
        }).fail(function() {
          alert("error");
        }).always(function() {
          $("#submit").val('submit');
        });
      });

      $(".form-control").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/ambil_data') ?>",
          data: $('select#id_user').serialize()
        }).done(function(respond){
          $("#team").html(respond);
        });
      });

      $(".th").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_divisi') ?>",
          data: $('select#tahun').serialize()
        }).done(function(respond){
          $("#divisi").html(respond);
        });
      });

      $(".th").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_type_tahun') ?>",
          data: $('select#tahun').serialize()
        }).done(function(respond){
          $("#type").html(respond);
        });
      });

      $(".th").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_pic_tahun') ?>",
          data: $('select#tahun').serialize()
        }).done(function(respond){
          $("#pic").html(respond);
        });
      });

      $(".divisi").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_pic') ?>",
          data: $('select#divisi').serialize()
        }).done(function(respond){
          $("#pic").html(respond);
        });
      });

      $(".divisi").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_type_divisi') ?>",
          data: $('select#divisi').serialize()
        }).done(function(respond){
          $("#type").html(respond);
        });
      });


      $(".pic").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_type') ?>",
          data: $('select#pic').serialize()
        }).done(function(respond){
          $("#type").html(respond);
        });
      });


      $(".th_log").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_divisi_log') ?>",
          data: $('select#tahun_log').serialize()
        }).done(function(respond){
          console.log(respond);
          $("#divisi_log").html(respond);
        });
      });

      $(".th_log").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_type_tahun_log') ?>",
          data: $('select#tahun_log').serialize()
        }).done(function(respond){
          $("#type_log").html(respond);
        });
      });

      $(".th_log").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_pic_tahun_log') ?>",
          data: $('select#tahun_log').serialize()
        }).done(function(respond){
          $("#pic_log").html(respond);
        });
      });

      $(".divisi_log").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_pic_log') ?>",
          data: $('select#divisi_log').serialize()
        }).done(function(respond){
          $("#pic_log").html(respond);
        });
      });

      $(".divisi_log").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_type_divisi_log') ?>",
          data: $('select#divisi_log').serialize()
        }).done(function(respond){
          $("#type_log").html(respond);
        });
      });

      $(".pic_log").change(function(){
        $.ajax({
          type:"POST",
          url: "<?php echo base_url('Admin/show_type_log') ?>",
          data: $('select#pic_log').serialize()
        }).done(function(respond){
          $("#type_log").html(respond);
        });
      });

      function get_now_datetime(){
        var myField = document.getElementById("my_date");
        var now = new Date();
        myField.valueAsNumber = now.getTime() - now.getTimezoneOffset() * 60000;
      }

      function localDate(datetime) {
        var myField = document.getElementById("my_date");
        var date = new Date(datetime),
        newDate = utcDate(date),
        offset = date.getTimezoneOffset() / 60;
        hours = date.getHours();

        newDate.setHours(hours - offset);
        return newDate;
        myField = newDate;

      }
      function readOnlyCheckBox() {
        return false;
      }
    </script>

    <style type="text/css">
    #kiri
    {
      width:50%;
      height:100px;
      float:left;
    }
    #kanan
    {
      width:50%;
      height:100px;
      float:right;
    }
    
    .circle {
      border-radius: 100px !important;
      overflow: hidden;
      width: 65px;
      height: 65px;
      left: 10%;
      border: 0px solid rgba(255, 255, 255, 0.7);
    }
  </style>

</body>
</html>
