<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Export_Log.xlsx");
?>
<table>
	<tr>
		<td align="center" colspan="10">DATA LOG PROJECT</td>
	</tr>
	<tr>
		<td align="center" colspan="6">DATA PROJECT</td>
		<td align="center" colspan="4">LOG PROJECT</td>
	</tr>
	<tr>
		<td>No</td>
		<td>Nama Pic</td>
		<td>Nama Project</td>
		<td>Instansi</td>
		<td>Divisi</td>
		<td>Type</td>
		<td>Rincian</td>
		<td>Progress</td>
		<td>Update</td>
		<td>Komentar</td>
	</tr>
	<?php
	$temp = "";
	$project = "";
	$instan = "";
	$no = 1;
	foreach ($laporan as $hasil){
		?>
		<tr>
			<?php
			if($hasil->nama_depan != $temp){
				?>
				<td><?php echo $no++ ?></td>
				<?php
			}else{
				?>
				<td></td>
				<?php
			}
			?>

			<?php
			if($hasil->nama_depan != $temp){
				?>
				<td><?php echo $hasil->nama_depan ?></td>
				<?php
			}else{
				?>
				<td></td>
				<?php
			}
			
			if ($hasil->nama_project != $project){
				?>
				<td><?php echo $hasil->nama_project ?></td>
				<?php
			}else{
				?>
				<td></td>
				<?php
			}
			if ($hasil->instansi != $instan){
				?>
				<td><?php echo $hasil->instansi ?></td>
				<?php
			}else{
				?>
				<td></td>
				<?php
			}
			?>
			<td><?php echo $hasil->divisi ?></td>
			<td><?php echo $hasil->type ?></td>
			<td><?php echo $hasil->rincian_log ?></td>
			<td><?php echo $hasil->progress_log.'%'?></td>
			<td><?php echo $hasil->update_log ?></td>
			<td><?php echo $hasil->komentar ?></td>
		</tr>
		<?php
		$temp = $hasil->nama_depan;
		$project = $hasil->nama_project;
		$instan = $hasil->instansi;
	}
	?>
</table>