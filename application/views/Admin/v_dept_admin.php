<html lang="en">
<head>
  <?php
  function rupiah($angka){
    $hasil_rupiah = "" . number_format($angka,2,',','.');
    return $hasil_rupiah;
  }
  ?>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">

</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="admin">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama'); ?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Admin/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
      <?php
      foreach ($profil as $v) {
        if($v->picture == ""){
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }else{
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }
      }
      ?>
      <div>
        <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
        <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
      </div>
    </div>

    <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="admin">
          <i class="app-menu__icon fas fa-chart-line"></i>
          <span class="app-menu__label">Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-list-ul"></i>
          <span class="app-menu__label">Data</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/daily_report_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Daily Report
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/data_tabel_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Tambah Project
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/cf_work_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              CF Work
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-cog"></i>
          <span class="app-menu__label">Settings</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/daftar_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Data Staff
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/data_department_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Department
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-swatchbook"></i>
          <span class="app-menu__label">Laporan</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/laporan_log')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Export Log
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/laporan_project')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Export Project
            </a>
          </li>
        </ul>
      </li>
    </ul>

  </aside>

  <main class="app-content">
    <div class="app-title">
      <div>
        <h5>Data Department</h5>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fas fa-list-ul fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="#">Data Department</a></li>
      </ul>
    </div>

    <div>
      <div class="btn-group">
        <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#tambah-dept"><i class="fas fa-plus-circle"></i> Tambah Department
        </button>
      </div>
    </div>

    <div class="card-body" style="background-color: #f5f6fa">
      <div class="table-responsive">
        <table id="example" class="table" style="width:100%;">
          <thead style="background-color: #f6e58d;">
            <tr>
              <th>No</th>
              <th>Nama Department</th>
              <th>Aksi</th>
            </tr>
          </thead>

          <tbody>
            <?php
            $no = 1;
            foreach ($t_department as $hasil) {
              ?>
              <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $hasil->nama_department ?></td>
                <td align="center">
                  <div class="btn-group">
                    <form action="hapus_data_department" method="post" class="delete_dept">
                      <input type="hidden" name="id_department" value="<?php echo $hasil->id_department ?>">
                      <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>

                    <form action="edit_dept" method="post" class="edit_dept">
                      <input type="hidden" name="id_department" value="<?php echo $hasil->id_department ?>">
                      <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#edit-dept">Edit</button>
                    </form>
                  </div>
                </td>
              </tr>
              <?php 
            } 
            ?>
          </tbody>
        </table>
      </div>
    </div>

    <div id="tambah-dept" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- konten modal-->
        <div class="modal-content">
          <!-- heading modal -->
          <div class="modal-header">
            <h4 class="modal-title">Tambah Department</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- body modal -->
          <div class="modal-body">
           <form action="<?php echo base_url().'Admin/tambah_data_dept'; ?>" method="post" class="tambah-dept"> 
            <div class="card-mb">
              <div class="form-group">
                <div class="col-sm-10">
                  <label for="usr">Nama_Department</label>
                  <input type="text" name="nama" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-5">
                  <div class="btn-group">
                    <button class="btn btn-info btn-sm" type="submit" name="upload"><i class="fas fa-check"></i> Simpan
                    </button>
                    <button class="btn btn-danger btn-sm" type="submit" data-dismiss="modal">Cancel
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="edit-dept" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- konten modal-->
      <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
          <h4 class="modal-title">Edit Department</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- body modal -->
        <div class="modal-body">
         <p id="result"></p>
       </div>
     </div>
   </div>
 </div>  
</main>

<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
     "autoWidth": false,
     "pageLength": 10,
     "bLengthChange": false,
     "bInfo" : false,
     "dom": '<"toolbar">frtip',
     "scrollY":        "50%",
     "scrollCollapse": true,
     "scrollX": true
   });
  });

  $(".edit_dept").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data
          }).done(function(response){ //
            $("#result").html(response);
          });
        });


  $(".tambah-dept").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data
          }).done(function(response){
            swal("Success!", "Data berhasil diinputkan", "success").then(function(){
              window.location = 'data_department_admin';
            });
          });
        });

  $(".delete_dept").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this row file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((response) => {
            if (response) {
              $.ajax({
                url : post_url,
                type: request_method,
                data : form_data
              }).done(function(response){
                swal("Poof! row file has been deleted!",{
                  icon: "success",
                }).then(function(){
                  window.location = 'data_department_admin';
                });
              });
            }else {
              swal("Your row file is safe!").then(function() {
                window.location = 'data_department_admin';
              });
            }
          });
        });

  
      </script>
      <style type="text/css">
      #kiri{
        width:50%;
        height:100px;
        float:left;
      }
      #kanan{
        width:50%;
        height:100px;
        float:right;
      }
      .circle {
        border-radius: 100px !important;
        overflow: hidden;
        width: 65px;
        height: 65px;
        left: 10%;
        border: 0px solid rgba(255, 255, 255, 0.7);
      }
    </style>
  </body>
  </html>




