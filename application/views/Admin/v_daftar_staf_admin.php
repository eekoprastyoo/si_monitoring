<html lang="en">
<head>
  <?php
  function rupiah($angka){
    $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
    return $hasil_rupiah;
  }
  ?>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
</head>

<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="admin"></a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
        <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama'); ?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Admin/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
      <?php
      foreach ($profil as $v) {
        if($v->picture == ""){
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }else{
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }
      }
      ?>
      <div>
        <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
        <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
      </div>
    </div>

    <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="admin">
          <i class="app-menu__icon fas fa-chart-line"></i>
          <span class="app-menu__label">Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-list-ul"></i>
          <span class="app-menu__label">Data</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/daily_report_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Daily Report
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/data_tabel_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Tambah Project
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/cf_work_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              CF Work
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-cog"></i>
          <span class="app-menu__label">Settings</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/daftar_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Data Staff
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/data_department_admin')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Department
            </a>
          </li>
        </ul>
      </li>

      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-swatchbook"></i>
          <span class="app-menu__label">Laporan</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/laporan_log')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Export Log
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Admin/laporan_project')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Export Project
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </aside>

  <main class="app-content">
    <div class="app-title">
      <div>
        <h5>Data Staff</h5>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fas fa-list-ul fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="#">Data Staff</a></li>
      </ul>
    </div>

    <div style="width: 100%;" >
      <div class="btn-group">
        <form>
          <button class="btn btn-info btn-sm" type="button" data-toggle="modal" data-target="#tambah-staff"><i class="fas fa-plus-circle"></i> Tambah Staff
          </button>
        </form>
      </div>
    </div>

    <div class="card-body" style="background-color: #f5f6fa">
      <div class="table-responsive">
        <table id="example" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Jabatan</th>
              <th>Aksi</th>
            </tr>
          </thead>

          <tbody>
            <?php
            $no = 1;
            foreach ($t_staff as $hasil) {
              ?>
              <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $hasil->nama_depan ?></td>
                <td><?php echo $hasil->email ?></td>
                <td><?php echo $hasil->level ?></td>
                <td>
                  <center>
                    <div class="btn-group">
                      <form action="hapus_staff" method="post" class="delete">
                        <input type="hidden" name="id_user" value="<?php echo $hasil->id_user ?>">
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                      </form>

                      <form action="edit_staff" method="post" class="edit_staff">
                        <input type="hidden" name="id_user" value="<?php echo $hasil->id_user ?>">
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#edit_staff">Edit</button>
                      </form>
                    </div>
                  </center>
                </td>
              </tr>
              <?php 
            } 
            ?>
          </tbody>
        </table>
      </div>
    </div>

    <div id="tambah-staff" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- konten modal-->
        <div class="modal-content">
          <!-- heading modal -->
          <div class="modal-header">
            <h4 class="modal-title">Tambah Data Staff</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- body modal -->
          <div class="modal-body">
           <div id="LoadingImage" style="display: none;">
             <div class="bubblingG">
              <span id="bubblingG_1">
              </span>
              <span id="bubblingG_2">
              </span>
              <span id="bubblingG_3">
              </span>
            </div>
          </div>
          <form action="<?php echo base_url(). 'Admin/tambah_data_staff'; ?>" method="post" class="tambah_staff">
            <div class="card-mb">
              <div class="form-group">
                <div class="col-sm-10">
                  <label for="usr">Email</label>
                  <input type="email" name="email" class="form-control" required="true">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-10">
                  <label for="usr">Nama Depan</label>
                  <input type="text" name="nama_depan" class="form-control" required="true">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-10">
                  <label for="usr">Nama Belakang</label>
                  <input type="text" name="nama_belakang" class="form-control" required="true">
                </div>
              </div>
              <div class="form-group">
               <div class="container">
                <div class="row">
                  <div class="col-sm-5">
                    <label for="usr">Jabatan</label>
                    <select class="form-control" name="level" required="true">
                      <option></option>
                      <option value="staff">Staff</option>
                      <option value="staff_lain">Staff Lainya</option>
                      <option value="admin">Admin</option>
                      <option value="manager">Manager</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-5">
                <div class="btn-group">
                  <button class="btn btn-info btn-sm" type="submit" name="upload"><i class="fas fa-check"></i> Simpan</button>
                  <button class="btn btn-danger btn-sm" type="submit" data-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="edit_staff">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <p id="result_edit"></p>
      </div>
    </div>
  </div>
</div>

</main>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
  });

  $(".tambah_staff").submit(function(event){
    $("#LoadingImage").show();  
          event.preventDefault(); //prevent default action
          $(".tambah_staff").attr("disabled", true); 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          if($("form")[0].checkValidity()) {
            $.ajax({
              url : post_url,
              type: request_method,
              data : form_data
            }).done(function(response){
             $("#LoadingImage").hide(); 
             swal("Success!", "Berhasil didaftarkan", "success").then(function(){
              window.location = 'daftar_staff';
            });
             $(".tambah_staff").attr("disabled", true); 
           });
          }
        });

  $(".edit_staff").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data
          }).done(function(response){ //
            $("#result_edit").html(response);
          });
        });

  $(".delete").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          swal({
            title:"Are you sure?",
            text: "Once deleted, you will not be able to recover this row file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((response) => {
            if (response) {
              $.ajax({
                url : post_url,
                type: request_method,
                data : form_data
              }).done(function(response){
                swal("Poof! row file has been deleted!",{
                  icon: "success",
                }).then(function(){
                  window.location = 'daftar_staff';
                });
              });
            }else {
              swal("Your row file is safe!").then(function() {
                window.location = 'daftar_staff';
              });
            }
          });
        });
      </script>

      <style type="text/css">
      .circle {
        border-radius: 100px !important;
        overflow: hidden;
        width: 65px;
        height: 65px;
        left: 10%;
        border: 0px solid rgba(255, 255, 255, 0.7);
      }
      .bubblingG {
        text-align: center;
        width:78px;
        height:49px;
        margin: auto;
      }

      .bubblingG span {
        display: inline-block;
        vertical-align: middle;
        width: 10px;
        height: 10px;
        margin: 24px auto;
        background: rgb(41,194,143);
        border-radius: 49px;
        -o-border-radius: 49px;
        -ms-border-radius: 49px;
        -webkit-border-radius: 49px;
        -moz-border-radius: 49px;
        animation: bubblingG 0.905s infinite alternate;
        -o-animation: bubblingG 0.905s infinite alternate;
        -ms-animation: bubblingG 0.905s infinite alternate;
        -webkit-animation: bubblingG 0.905s infinite alternate;
        -moz-animation: bubblingG 0.905s infinite alternate;
      }

      #bubblingG_1 {
        animation-delay: 0s;
        -o-animation-delay: 0s;
        -ms-animation-delay: 0s;
        -webkit-animation-delay: 0s;
        -moz-animation-delay: 0s;
      }

      #bubblingG_2 {
        animation-delay: 0.2695s;
        -o-animation-delay: 0.2695s;
        -ms-animation-delay: 0.2695s;
        -webkit-animation-delay: 0.2695s;
        -moz-animation-delay: 0.2695s;
      }

      #bubblingG_3 {
        animation-delay: 0.549s;
        -o-animation-delay: 0.549s;
        -ms-animation-delay: 0.549s;
        -webkit-animation-delay: 0.549s;
        -moz-animation-delay: 0.549s;
      }



      @keyframes bubblingG {
        0% {
          width: 10px;
          height: 10px;
          background-color:rgba(41,158,152,0.98);
          transform: translateY(0);
        }

        100% {
          width: 23px;
          height: 23px;
          background-color:rgb(255,255,255);
          transform: translateY(-20px);
        }
      }

      @-o-keyframes bubblingG {
        0% {
          width: 10px;
          height: 10px;
          background-color:rgba(41,158,152,0.98);
          -o-transform: translateY(0);
        }

        100% {
          width: 23px;
          height: 23px;
          background-color:rgb(255,255,255);
          -o-transform: translateY(-20px);
        }
      }

      @-ms-keyframes bubblingG {
        0% {
          width: 10px;
          height: 10px;
          background-color:rgba(41,158,152,0.98);
          -ms-transform: translateY(0);
        }

        100% {
          width: 23px;
          height: 23px;
          background-color:rgb(255,255,255);
          -ms-transform: translateY(-20px);
        }
      }

      @-webkit-keyframes bubblingG {
        0% {
          width: 10px;
          height: 10px;
          background-color:rgba(41,158,152,0.98);
          -webkit-transform: translateY(0);
        }

        100% {
          width: 23px;
          height: 23px;
          background-color:rgb(255,255,255);
          -webkit-transform: translateY(-20px);
        }
      }

      @-moz-keyframes bubblingG {
        0% {
          width: 10px;
          height: 10px;
          background-color:rgba(41,158,152,0.98);
          -moz-transform: translateY(0);
        }

        100% {
          width: 23px;
          height: 23px;
          background-color:rgb(255,255,255);
          -moz-transform: translateY(-20px);
        }
      }
    </style>
  </body>
  </html>


