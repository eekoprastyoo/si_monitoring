<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link href="<?php echo base_url('assets/docs/css/main.css')?>" rel="stylesheet" type="text/css">
  <!-- Font-icon css-->
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <title>Sistem informasi monitoring marketing</title>
</head>
<body>
   <?php
   $message = $this->session->flashdata('message');
   if (isset($message)) {
    echo '<div class="alert alert-danger alert-dismissible fade show" id="success-alert">
    '.$message.'</div>';
    $this->session->unset_userdata('message');
  }
  ?>
   <div id="_IE-Alert"></div>
   <div class="_load" id="_load">
    <div class="_load-middle">
      <div class="_load-circle"></div>
    </div>
  </div>
  <section class="material-half-bg">
    <div class="cover"></div>
  </section>
  <section class="login-content">
    <div class="logo">
      <h1></h1>
    </div>
    <div class="login-box" >
      <form class="login-form" action="<?php echo base_url('Login/aksi_login');?>" method="post">
        <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
        <div class="form-group">
          <label class="control-label">EMAIL</label>
          <input class="form-control" type="email" name="email" placeholder="Email" autofocus>
        </div>
        <div class="form-group">
          <label class="control-label">PASSWORD</label>
          <input class="form-password" type="password" name="password" placeholder="Password">
        </div>
        <div class="form-group">
          <div class="utility">
            <div class="animated-checkbox">
              <label>
               <input type="checkbox" class="password" ><span class="label-text">Show password</span> 
             </label>
           </div>
           <p class="semibold-text mb-2"><a href="Login/forgot_password">Forgot Password ?</a></p>
         </div>
       </div>
       <div class="form-group btn-container">
        <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
      </div>
    </form>
  </div>
</section>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>

    <script type="text/javascript">
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){   
        $('.password').click(function(){
          if($(this).is(':checked')){
            $('.form-password').attr('type','text');
          }else{
            $('.form-password').attr('type','password');
          }
        });
      });
      $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
          $("#success-alert").slideUp(500);
      });
    </script>
    <style type="text/css">
    .form-password{
      display: block;
      width: 100%;
      padding: 0.375rem 0.75rem;
      font-size: 0.875rem;
      line-height: 1.5;
      color: #495057;
      background-color: #FFF;
      background-clip: padding-box;
      border: 2px solid #ced4da;
      border-radius: 4px;
      -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
      transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
      -o-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
      transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
      transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    }
  </style>
</body>
</html>