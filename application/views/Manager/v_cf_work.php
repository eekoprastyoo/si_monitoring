<html lang="en">
<head>
  <?php
  function rupiah($angka){
    $hasil_rupiah = "" . number_format($angka,0,',','.');
    return $hasil_rupiah;
  }
  ?>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">

</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="Manager">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('Manager/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
     <?php
     foreach ($profil as $v) {
      if($v->picture == ""){
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }else{
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }
    }
    ?>
    <div>
      <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
      <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
    </div>
  </div>
  <ul class="app-menu">
    <li>
      <a class="app-menu__item active" href="manager">
        <i class="app-menu__icon fas fa-chart-line"></i>
        <span class="app-menu__label">Dashboard</span>
      </a>
    </li>
    <li class="treeview">
      <a class="app-menu__item" href="" data-toggle="treeview">
        <i class="app-menu__icon fas fa-list-ul"></i>
        <span class="app-menu__label">Data</span>
        <i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
       <li>
        <a class="app-menu__item active" href="daily_report_manager">
         <!--  <i class="app-menu__icon fas fa-clock"></i> -->
         <span class="app-menu__label">Laporan Harian</span>
       </a>
     </li>
     <li>
      <a class="app-menu__item active" href="report_manager">
        <!-- <i class="app-menu__icon fas fa-sync-alt"></i> -->
        <span class="app-menu__label">Project Pribadi </span>
      </a>
    </li>
    <li>
      <a class="app-menu__item active" href="tambah_data">
       <!--  <i class="app-menu__icon fas fa-plus-circle"></i> -->
       <span class="app-menu__label">Tambah Project</span>
     </a>
   </li>
   <li>
    <a class="app-menu__item active" href="cf_work">
     <!--  <i class="app-menu__icon fas fa-thumbs-up"></i> -->
     <span class="app-menu__label">Close Failed</span>
   </a>
 </li>
</ul>
</li>
<li class="treeview">
  <a class="app-menu__item active" href="" data-toggle="treeview">
    <i class="app-menu__icon fas fa-cog"></i>
    <span class="app-menu__label">Settings</span>
    <i class="treeview-indicator fa fa-angle-right"></i>
  </a>
  <ul class="treeview-menu">
    <li>
      <a class="app-menu__item active" href="t_staff">
       <!--  <i class="app-menu__icon fas fa-clock"></i> -->
       <span class="app-menu__label">Target Staff</span>
     </a>
   </li>
   <li>
    <a class="app-menu__item active" href="t_perusahaan">
      <!-- <i class="app-menu__icon fas fa-clock"></i> -->
      <span class="app-menu__label">Target Perusahaan</span>
    </a>
  </li>
</ul>
</li>
<li class="treeview">
  <a class="app-menu__item active" href="" data-toggle="treeview">
    <i class="app-menu__icon fas fa-swatchbook"></i>
    <span class="app-menu__label">Laporan</span>
    <i class="treeview-indicator fa fa-angle-right"></i>
  </a>
  <ul class="treeview-menu">
    <li>
      <a class="app-menu__item active" href="laporan_log">
       <!--  <i class="app-menu__icon fas fa-clock"></i> -->
       <span class="app-menu__label">Export Log</span>
     </a>
   </li>
   <li>
    <a class="app-menu__item active" href="laporan_project">
      <!-- <i class="app-menu__icon fas fa-clock"></i> -->
      <span class="app-menu__label">Export Project</span>
    </a>
  </li>
</ul>
</li>
</ul>
</aside>

<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="app-menu__icon fas fa-receipt"></i>CF Work</h1>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-receipt fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">CF Work</a></li>
    </ul>
  </div>

  <div class="modal fade" id="target">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <table>
            <tr>
              <td><h4 class="modal-title">Nilai Target Perusahaan</h4></td>
            </tr>
            <tr>
              <td align="left"><button data-toggle="modal" class="btn btn-info btn-sm" data-target="#input">Input Nilai Target</button></td>
            </tr>
          </table>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <table class="table table-striped center">
            <thead>
              <tr>
               <th>No</th>
               <th>Tahun</th>
               <th>Target Perusahaan</th>
               <th>Action</th>
             </tr>
           </thead>
           <tbody>
            <?php
            $no = 1;
            foreach ($target as $hasil) {
              ?>
              <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $hasil->tanggal?></td>
                <td><?php echo rupiah($hasil->nilai);?></td>
                <td>
                  <div class="btn-group">

                    <form action="delete_target" method="post" class="delete_target">
                      <input type="hidden" name="id_target" value="<?php echo $hasil->id_target?>">
                      <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                  </div>
                </td>
              </tr>

              <?php
            }
            ?>

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="input">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Input Nilai Target Perusahaan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="col-sm-10">
          <form name="target"  id="targetForm" action="target_perusahaan" method="post">
            <table align="right">
             <tr>
              <td>
                <label>Target</label>
              </td>
              <td>
                <input type="text" name="target" class="testInput form-control"><span id="format"></span>
              </td>
            </tr>
            <tr>
              <td>
                <label>Tahun</label>
              </td>
              <td>
                <input type="date" name="tanggal" class="form-control">
              </td>
              <td>
                <button type="submit" name="submit" class="btn btn-info btn-sm">Simpan</button>
              </td>
            </tr>
          </table>
        </form>
      </div>
    </div>

  </div>
</div>
</div>

<div class="modal fade" id="export-log">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Export Log Project</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>    
      <div class="modal-body">
        <div class="card-mb">
          <div class="form-group">
            <form action="export_excel_log" method="post">
              <table width="100%">
                <tr>
                  <td width="20%;" align="right">
                    <label for="usr">Tahun</label>
                  </td>
                  <td colspan="2" align="center">
                    <div class="col-sm-10">
                      <select class="th_log js-example-basic-single" name="tahun_log" style="width:100%;">
                        <option value="" selected="selected">--Pilih--</option>
                        <?php  
                        $thn_skr = date('Y');
                        for ($x = $thn_skr; $x >= 2018; $x--) {
                          ?>
                          <option value="<?php echo $x ?>"><?php echo $x ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </td>
                </tr>

                <tr>  
                  <td width="20%;" align="right">
                    <label for="usr">Divisi</label>
                  </td>
                  <td colspan="2"  align="center">
                    <div class="col-sm-10">
                      <select id="divisi_log" class="divisi_log js-example-basic-single" name="divisi_log" style="width:100%;">
                      </select>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td width="20%;" align="right">
                    <label for="usr">Nama Pic</label>
                  </td>
                  <td colspan="2"  align="center">
                    <div class="col-sm-10">
                      <select id="pic_log" class="pic_log js-example-basic-single" name="pic_log" style="width:100%;">
                      </select>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td width="20%;" align="right">
                    <label for="usr">Type</label>
                  </td>
                  <td colspan="2"  align="center">        
                    <div class="col-sm-10">
                      <select id="type_log" class="js-example-basic-single" name="type_log" style="width:100%;">
                      </select>
                    </div>
                  </td>
                  <tr>

                    <tr>        
                      <td align="left" colspan="2" width="160%;">
                        <div class="col-sm-10">
                          <label for="usr"></label><br>
                          <button type="submit" class="btn btn-info btn-sm"><i class="fas fa-file-export">Export Now</i></button>
                        </div>
                      </td>
                    </tr>
                  </table> 
                </form>
              </div>
            </div>
          </div> 
        </div>
      </div>
    </div>

    <div class="modal fade" id="export-project">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Export Project</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>    
          <div class="modal-body">
            <div class="card-mb">
              <form action="export_excel" method="post">
                <div class="form-group">
                  <table width="100%">
                    <tr>
                      <td width="20%;" align="right">
                        <label for="usr">Tahun</label>
                      </td>
                      <td colspan="2" align="center">
                        <div class="col-sm-10">
                          <select class="th js-example-basic-single" name="tahun" style="width:100%;">
                            <option value="" selected="selected">--Pilih--</option>
                            <?php  
                            $thn_skr = date('Y');
                            for ($x = $thn_skr; $x >= 2018; $x--) {
                              ?>
                              <option value="<?php echo $x ?>"><?php echo $x ?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </td>
                    </tr>

                    <tr>  
                      <td width="20%;" align="right">
                        <label for="usr">Divisi</label>
                      </td>
                      <td colspan="2"  align="center">
                        <div class="col-sm-10">
                          <select id="divisi" class="divisi js-example-basic-single" name="divisi" style="width:100%;">
                          </select>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td width="20%;" align="right">
                        <label for="usr">Nama Pic</label>
                      </td>
                      <td colspan="2"  align="center">
                        <div class="col-sm-10">
                          <select id="pic" class="pic js-example-basic-single" name="pic" style="width:100%;">
                          </select>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td width="20%;" align="right">
                        <label for="usr">Type</label>
                      </td>
                      <td colspan="2"  align="center">        
                        <div class="col-sm-10">
                          <select id="type" class="js-example-basic-single" name="type" style="width:100%;">
                          </select>
                        </div>
                      </td>
                      <tr>

                        <tr>        
                          <td align="left" colspan="2" width="160%;">
                            <div class="col-sm-10">
                              <label for="usr"></label><br>
                              <button type="submit" class="btn btn-default" style="color: white;background-color: #03a9f4"><i class="fas fa-file-export">Export Now</i></button>
                            </div>
                          </td>
                        </tr>
                      </table> 
                    </div>
                  </form>
                </div>
              </div> 
            </div>
          </div>
        </div>


        <div class="card-body" style="background-color: #f5f6fa">
          <div class="table-responsive">
            <table id="example" class="table" style="width:100%;">
              <thead style="background-color: #f6e58d;">
                <tr>
                  <th>No</th>
                  <th>Nama PIC</th>
                  <th>Nama Project</th>
                  <th>Instansi</th>
                  <th>Status</th>
                  <th>Progress</th>
                  <th>Tanggal Update</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                <?php
                $no_urut = 1;
                foreach ($t_data_report as $hasil) {
                  $tgl_akhir  = new DateTime(date(".".$hasil->last_update.""));
                  $tgl_hitung  = new DateTime(date("Y-m-d H:i:s"));
                  $difference = $tgl_hitung->diff($tgl_akhir);
                  ?>

                  <tr>
                    <td><?php echo $no_urut++ ?></td>
                    <td><?php echo $hasil->nama_depan ?></td>
                    <td><?php echo $hasil->nama_project ?></td>
                    <td><?php echo $hasil->instansi ?></td>
                    <?php
                    if($hasil->status_project == "running" && $difference->days > 5){
                      ?>
                      <td><center><span class="badge" style="background-color: #e58e26;color: white;">running</span></center></td>
                      <?php
                    }else if($hasil->status_project == "running"){
                      ?>
                      <td><center><span class="badge badge-success">running</span></center></td>
                      <?php
                    }
                    else if($hasil->status_project == "closed"){
                      ?>
                      <td><center><span class="badge badge-info">closed</span></center></td>
                      <?php
                    }else{
                      ?>
                      <td><center><span class="badge badge-danger">failed</span></center></td>
                      <?php
                    }
                    ?>
                    <td><?php echo $hasil->progres."%" ?></td>

                    <?php
                    if($hasil->status_project == "closed"){
                      ?>
                      <td>
                        <center><span class="badge badge-info"><?php echo $hasil->last_update ?></span></center>
                      </td>
                      <?php
                    }else if($hasil->status_project == "failed"){
                      ?>
                      <td>
                        <center><span class="badge badge-danger"><?php echo $hasil->last_update ?></span></center>
                      </td>
                      <?php
                    }else if($difference->days > 5){
                      ?>
                      <td>
                        <center><span class="badge" style="background-color: #e58e26;color: white;"><?php echo $hasil->last_update ?></span></center>
                      </td>
                      <?php
                    }
                    else{
                      ?>
                      <td>
                        <center><span class="badge badge-success"><?php echo $hasil->last_update ?></span></center>
                      </td>
                      <?php
                    }
                    ?>
                    <td>
                      <center>
                        <form action="detail_project_manager" method="post">
                          <input type="hidden" name="instan" value="<?php echo $hasil->instansi;?>">
                          <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                          <input type="hidden" name="project" value="<?php echo $hasil->nama_project;?>">
                          <button type="submit" class="btn btn-info btn-sm" >Detail</button>
                        </form>
                      </center>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </div>
          </div>
        </main>

        <!-- Essential javascripts for application to work-->
        <script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
        <script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.41.js"></script>
        <script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
        <script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
        <script src="<?php echo base_url('assets/docs/js/my.js')?>"></script>
        <script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script type="text/javascript">
          $(document).ready(function() {
            $('#example').DataTable({
              "bInfo" : false,
              "dom": '<"toolbar">frtip',
              "scrollY":        "50%",
              "scrollCollapse": true,
              "scrollX": true
            });
            $('.js-example-basic-single').select2({ width: 'resolve' });
          });

          $(".testInput").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999999999',
            vMin: '-9'
          });

          $(".delete_target").submit(function(event){
            event.preventDefault(); //prevent default action 
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = $(this).serialize(); //Encode form elements for submission
            console.log(form_data);
            swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this row file!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((response) => {
              if (response) {
                $.ajax({
                  url : post_url,
                  type: request_method,
                  data : form_data
                }).done(function(response){
                  swal("Poof! your project has been deleted!",{
                    icon: "success",
                  }).then(function(){
                    window.location = 'daily_report_manager';
                  });
                });
              }else {
                swal("Your your project is safe!").then(function() {
                  window.location = 'daily_report_manager';
                });
              }
            });
          });

          $(".th").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_divisi') ?>",
              data: $('select#tahun').serialize()
            }).done(function(respond){
              $("#divisi").html(respond);
            });
          });

          $(".th").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_type_tahun') ?>",
              data: $('select#tahun').serialize()
            }).done(function(respond){
              $("#type").html(respond);
            });
          });

          $(".th").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_pic_tahun') ?>",
              data: $('select#tahun').serialize()
            }).done(function(respond){
              $("#pic").html(respond);
            });
          });

          $(".divisi").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_pic') ?>",
              data: $('select#divisi').serialize()
            }).done(function(respond){
              $("#pic").html(respond);
            });
          });

          $(".divisi").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_type_divisi') ?>",
              data: $('select#divisi').serialize()
            }).done(function(respond){
              $("#type").html(respond);
            });
          });

          $(".pic").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_type') ?>",
              data: $('select#pic').serialize()
            }).done(function(respond){
              $("#type").html(respond);
            });
          });

          $(".th_log").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_divisi_log') ?>",
              data: $('select#tahun_log').serialize()
            }).done(function(respond){
              console.log(respond);
              $("#divisi_log").html(respond);
            });
          });

          $(".th_log").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_type_tahun_log') ?>",
              data: $('select#tahun_log').serialize()
            }).done(function(respond){
              $("#type_log").html(respond);
            });
          });

          $(".th_log").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_pic_tahun_log') ?>",
              data: $('select#tahun_log').serialize()
            }).done(function(respond){
              $("#pic_log").html(respond);
            });
          });

          $(".divisi_log").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_pic_log') ?>",
              data: $('select#divisi_log').serialize()
            }).done(function(respond){
              $("#pic_log").html(respond);
            });
          });

          $(".divisi_log").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_type_divisi_log') ?>",
              data: $('select#divisi_log').serialize()
            }).done(function(respond){
              $("#type_log").html(respond);
            });
          });

          $(".pic_log").change(function(){
            $.ajax({
              type:"POST",
              url: "<?php echo base_url('Manager/show_type_log') ?>",
              data: $('select#pic_log').serialize()
            }).done(function(respond){
              $("#type_log").html(respond);
            });
          });

          $("#targetForm").submit(function(e) {
              // prevent default submit behaviour 
              e.preventDefault();
              // serialize total form data
              var postData = $(this).serializeArray();
              // get form action url
              var formActionURL = $(this).attr("action");
              $("#submit").val('please wait...');
              // JQuery ajax method , for post we can directly use $.post({}); this is shortcut method for sending post request 
              $.ajax({
                url: formActionURL,
                type: "POST",
                data: postData,
              }).done(function(respond) {
                swal({
                  title: "Success!",
                  text: "Data berhasil diinputkan",
                  icon: "success",
                  button: "Ok",
                }).then(function() {
                  window.location = "daily_report_manager";
                });
              }).fail(function() {
                alert("error");
              }).always(function() {
                $("#submit").val('submit');
              });
            });
          </script>
          <style type="text/css">
          .circle {
            border-radius: 100px !important;
            overflow: hidden;
            width: 65px;
            height: 65px;
            left: 10%;
            border: 0px solid rgba(255, 255, 255, 0.7);
          }
        </style>
      </body>
      </html>
