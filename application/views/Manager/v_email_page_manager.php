<!DOCTYPE html>
<html>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

form {
  border: 3px solid #f1f1f1;
  font-family: Arial;
}

.container {
  padding: 20px;
  background-color: #f1f1f1;
}

input[type=text], input[type=submit] {
  width: 100%;
  padding: 12px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

input[type=checkbox] {
  margin-top: 16px;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  border: none;
}

input[type=submit]:hover {
  opacity: 0.8;
}
</style>
<body>
  <table width="100%">
    <tr>
      <td width="50%"align="right">
        <img src="https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/14691064_1126674984081042_7683922444718356585_n.png?_nc_cat=0&oh=8da3a15e5c5227e8b686de6905046eec&oe=5BD20F44" class="img-responsive" width="20%">
      </td>
      <td width="50%"align="left">
        <h2 style="color: black;">PT.TIME EXCELINDO</h2>
      </td>
    </tr>
  </table>

  <div class="container">    
    <div class="row">
      <div class="col-sm-12">
       <table>
         <tr>
           <td><h4 style="color: black;"><label>Comment By</label></h4></td>
           <td><h4><?php if(isset($update_by)) { echo $update_by; }?></h4></td>
         </tr>
         <tr>
           <td height="10px"><h4 style="color: black;"><label>Date</label></h4></td>
           <td height="10px"><label style="color: black;"><?php if(isset($tanggal)) { echo $tanggal; }?></label></td>
         </tr>
       </table>
     </div>
   </div>
 </div>

 <div id="container" style="margin-top: 2%;">
  <div class="row">
    <table style="margin-left: 10%;">
      <?php
      foreach ($all_log as $hasil) {
        ?>
        <tr>
          <td style="width: 10%;"><h4>Nama PIC</h4></td>
          <td style="width: 40%;"><?php echo $hasil->nama_depan ?></td>
        </tr>
        <tr>
          <td style="width: 10%;"><h4>Nama Project</h4></td>
          <td style="width: 40%;"><?php echo $hasil->nama_project ?></td>
        </tr>
        <tr>
          <td style="width: 10%;"><h4>Instansi</h4></td>
          <td style="width: 40%;"><?php echo $hasil->instansi ?></td>
        </tr>
        <tr>
          <td style="width: 10%;"><h4>Rincian Log Pekerjaan</h4></td>
          <td style="width: 40%;"><?php echo $hasil->rincian_log ?></td>
        </tr>
        <tr>
          <td style="width: 10%;"><h4>Hasil Progres</h4></td>
          <td style="width: 40%;"><?php echo $hasil->progress_log?> %</td>
        </tr>
        <tr>
          <td style="width: 10%;"><h4>Komentar</h4></td>
          <td style="width: 40%;"><?php echo $hasil->komentar?></td>
        </tr>
        <?php
      }
      ?>
    </table>
  </div>
</div>
</body>
</html>