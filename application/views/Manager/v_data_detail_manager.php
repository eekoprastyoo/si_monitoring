<html lang="en">
<head>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">

</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="manager">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('Manager/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
     <?php
     foreach ($profil as $v) {
      if($v->picture == ""){
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }else{
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }
    }
    ?>
    <div>
      <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
      <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
    </div>
  </div>
  <ul class="app-menu">
    <li>
      <a class="app-menu__item active" href="manager">
        <i class="app-menu__icon fas fa-chart-line"></i>
        <span class="app-menu__label">Dashboard</span>
      </a>
    </li>
    <li class="treeview">
      <a class="app-menu__item" href="" data-toggle="treeview">
        <i class="app-menu__icon fas fa-list-ul"></i>
        <span class="app-menu__label">Data</span>
        <i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
       <li>
        <a class="app-menu__item active" href="daily_report_manager">
         <!--  <i class="app-menu__icon fas fa-clock"></i> -->
          <span class="app-menu__label">Laporan Harian</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item active" href="report_manager">
          <!-- <i class="app-menu__icon fas fa-sync-alt"></i> -->
          <span class="app-menu__label">Project Pribadi </span>
        </a>
      </li>
      <li>
        <a class="app-menu__item active" href="tambah_data">
         <!--  <i class="app-menu__icon fas fa-plus-circle"></i> -->
          <span class="app-menu__label">Tambah Project</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item active" href="cf_work">
         <!--  <i class="app-menu__icon fas fa-thumbs-up"></i> -->
          <span class="app-menu__label">Close Failed</span>
        </a>
      </li>
    </ul>
  </li>
  <li class="treeview">
    <a class="app-menu__item active" href="" data-toggle="treeview">
      <i class="app-menu__icon fas fa-cog"></i>
      <span class="app-menu__label">Settings</span>
      <i class="treeview-indicator fa fa-angle-right"></i>
    </a>
    <ul class="treeview-menu">
      <li>
        <a class="app-menu__item active" href="t_staff">
         <!--  <i class="app-menu__icon fas fa-clock"></i> -->
          <span class="app-menu__label">Target Staff</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item active" href="t_perusahaan">
          <!-- <i class="app-menu__icon fas fa-clock"></i> -->
          <span class="app-menu__label">Target Perusahaan</span>
        </a>
      </li>
    </ul>
  </li>
  <li class="treeview">
    <a class="app-menu__item active" href="" data-toggle="treeview">
      <i class="app-menu__icon fas fa-swatchbook"></i>
      <span class="app-menu__label">Laporan</span>
      <i class="treeview-indicator fa fa-angle-right"></i>
    </a>
    <ul class="treeview-menu">
      <li>
        <a class="app-menu__item active" href="laporan_log">
         <!--  <i class="app-menu__icon fas fa-clock"></i> -->
         <span class="app-menu__label">Export Log</span>
       </a>
     </li>
     <li>
      <a class="app-menu__item active" href="laporan_project">
        <!-- <i class="app-menu__icon fas fa-clock"></i> -->
        <span class="app-menu__label">Export Project</span>
      </a>
    </li>
  </ul>
</li>
</ul>
</aside>

<main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="app-menu__icon fa fa-table"></i>Daily Report</h1>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-table fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="data_tabel">Daily Report</a></li>
    </ul>
  </div>

  <div class="card-body" style="background-color: #f5f6fa">
    <div class="table-responsive">
      <table id="example" class="table table-striped table-bordered" style="width:100%;">
        <thead>
          <tr>
            <th>Nama PIC</th>
            <th>Nama Project</th>
            <th>Instansi</th>
            <th>Rincian Log</th>
            <th>Progress</th>
            <th>Update Log</th>
            <th>File Pendukung</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          <?php
          foreach ($data_log as $hasil) {
            ?>
            <tr>
              <td><?php echo $hasil->nama_depan ?></td>
              <td><?php echo $hasil->nama_project ?></td>
              <td><?php echo $hasil->instansi ?></td>
              <td><?php echo $hasil->rincian_log ?></td>
              <td><?php echo $hasil->progress_log."%" ?></td>
              <td><?php echo $hasil->update_log ?></td>
              <?php
              $file = $hasil->file;
              if($file == NULL){
                ?>
                <td align="center"><label>-</label></td>
                <?php
              }else{
                ?>
                <td align="center">
                  <form action="<?php echo base_url().'Manager/show_file'; ?>" method="post" class="my_file">
                    <input type="hidden" name="id_log" value="<?php echo $hasil->id_log ?>">
                    <button class="btn btn-info btn-sm" name="submit" data-toggle="modal" data-target="#myFile">Show</button>
                  </form>
                </td>
                <?php
              }
              ?>
              <td>
                <center>
                  <form action="<?php echo base_url().'Manager/show_detail'; ?>" method="post" class="show_detail">
                    <input type="hidden" name="id_log" value="<?php echo $hasil->id_log ?>">
                    <input type="submit" class="btn btn-info btn-sm" name="submit" value="Comment" data-toggle="modal" data-target="#mydetail">
                  </form>
                </center>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>

      </table>
    </div>
  </div>
</div>

<div id="mydetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <label><b>Komentar Manager</b></label>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="result">
        </p>   
      </div>
    </div>
  </div>
</div>
</div>

<div id="myFile" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <label>File Pendukung</label>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="hasil_file">
        </p>   
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<div id="mycomment" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <label><b>Komentar Manager</b></label>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p id="result_comment">
        </p>   
      </div>
    </div>
  </div>
</div>
</div>


</main>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $(".show_detail").submit(function(event){
            event.preventDefault(); //prevent default action 
            // prevent default
            var post_url = $(this).attr("action"); //get form action url
            var request_method = $(this).attr("method"); //get form GET/POST method
            var form_data = $(this).serialize(); //Encode form elements for submission
            $.ajax({
              url : post_url,
              type: request_method,
              data : form_data
            }).done(function(response){ //
              $("#result").html(response);
            });
          });

    $('#example').DataTable(
    {
      "pageLength": 20
    });
    
    $("#send_coment").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data
          }).done(function(response){ //
            $("#result_comment").html(response);
          });
        });

    $(".my_file").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data
          }).done(function(response){
            $("#hasil_file").html(response);
          });
        });

  });
</script>
<style type="text/css">
.circle {
  border-radius: 100px !important;
  overflow: hidden;
  width: 65px;
  height: 65px;
  left: 10%;
  border: 0px solid rgba(255, 255, 255, 0.7);
}
</style>
</body>
</html>
