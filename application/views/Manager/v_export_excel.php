<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Export_Project.xlsx");
?>
<table style="width:100%;" border="1">
	<tr>
		<td colspan="5" align="center">DATA PROJECT</td>
	</tr>
	<tr>
		<th>No</th>
		<th>Nama PIC</th>
		<th>Nama Project</th>
		<th>Instansi</th>
		<th>Divisi</th>
		<th>Type</th>
		<th>Nilai SPK</th>
	</tr>
	<?php
	$no = 1;
	foreach ($laporan as $hasil) {
		?>
		<tr>
			<td><?php echo $no++?></td>
			<td><?php echo $hasil->nama_depan ?></td>
			<td><?php echo $hasil->nama_project ?></td>
			<td><?php echo $hasil->instansi ?></td>
			<td><?php echo $hasil->divisi ?></td>
			<td><?php echo $hasil->type ?></td>
			<td><?php echo $hasil->real_pendapatan ?></td>
		</tr>
		<?php
	}
	?>
</table>