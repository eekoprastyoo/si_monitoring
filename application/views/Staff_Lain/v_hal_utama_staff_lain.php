<html lang="en">
<head>
  <?php
  function rupiah($angka){
    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
    return $hasil_rupiah;
  }
  ?>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="index.html"></a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Staff_Lain/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
     <?php
     foreach ($profil as $v) {
      if($v->picture == ""){
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }else{
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }
    }
    ?>
    <div>
      <p class="app-sidebar__user-name"> <?php echo $this->session->userdata('nama');?></p>
      <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
    </div>
  </div>
  <ul class="app-menu">
    <li>
      <a class="app-menu__item active" href="staff_lain">
        <i class="app-menu__icon fas fa-tasks"></i>
        <span class="app-menu__label">Task</span>
      </a>
    </li>
    </ul>
  </aside>

  <main class="app-content">
    <div>
      <?php
      $message = $this->session->flashdata('success');
      if (isset($message)) {
        echo '<div class="alert alert-success alert-dismissible fade show" id="success-alert">
        '.$message.'</div>';
        $this->session->unset_userdata('success');
      }
      ?>
    </div>
    <div class="card-body" style="background-color: #f5f6fa">
      <div class="table-responsive">
        <table id="example" class="table" style="width:100%;">
          <thead style="background-color: #f6e58d;">
            <tr>
              <th>No</th>
              <th>Nama PIC</th>
              <th>Nama Project</th>
              <th>Instansi</th>
              <th>Action</th>
            </tr>
          </thead>

          <tbody>
            <?php
            $no_urut = 1;
            foreach ($t_data_report as $hasil) {
              ?>
              <tr>
                <td><?php echo $no_urut++ ?></td>
                <td><?php echo $hasil->nama_depan ?></td>
                <td><?php echo $hasil->nama_project ?></td>
                <td><?php echo $hasil->instansi ?></td>
                <td>
                  <center>
                    <div class="btn-group">
                      <?php
                      $email = $this->session->userdata('email');
                      if(in_array($email,$group)){
                        ?>
                        <form action="update_progres_staff_lain" method="post">
                          <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                          <input type="hidden" name="nama_project" value="<?php echo $hasil->nama_project?>">
                          <input type="hidden" name="instansi" value="<?php echo $hasil->instansi?>">
                          <button type="submit" class="btn btn-info btn-sm">Update</button>
                        </form>
                        <form action="tampil_log_staff_lain" method="post">
                          <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                          <input type="hidden" name="nama_project" value="<?php echo $hasil->nama_project?>">
                          <input type="hidden" name="instansi" value="<?php echo $hasil->instansi?>">
                          <input type="hidden" name="email" value="<?php echo $this->session->userdata('email');?>">
                          <button type="submit" class="btn btn-success btn-sm" >Detail</button>
                        </form>
                        <?php
                      }else{
                        ?>
                        <form action="tampil_log_staff_lain" method="post">
                          <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                          <input type="hidden" name="nama_project" value="<?php echo $hasil->nama_project?>">
                          <input type="hidden" name="instansi" value="<?php echo $hasil->instansi?>">
                          <input type="hidden" name="email" value="<?php echo $this->session->userdata('email');?>">
                          <button type="submit" class="btn btn-success btn-sm" >Detail</button>
                        </form>

                        <?php
                      }
                      ?>
                    </div>
                  </center>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>

        </table>
      </div>
    </div>

  </main>
  <!-- Essential javascripts for application to work-->
  <script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
  <script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
  <script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
  <script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#example').DataTable();
      $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
      });
    });
  </script>
  <style type="text/css">
  #kiri
  {
    width:45%;
    float:left;
  }
  #kanan
  {
    width:45%;
    float:right;
  }
        .circle {
        border-radius: 100px !important;
        overflow: hidden;
        width: 65px;
        height: 65px;
        left: 10%;
        border: 0px solid rgba(255, 255, 255, 0.7);
      }
</style>
</body>
</html>
