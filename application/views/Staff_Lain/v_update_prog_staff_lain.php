<html lang="en">
<head>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="admin">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('Staff/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
      <?php
      foreach ($profil as $v) {
        if($v->picture == ""){
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }else{
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }
      }
      ?>
      <div>
        <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
        <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
      </div>
    </div>

    <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="staff_lain">
          <i class="app-menu__icon fas fa-tasks"></i>
          <span class="app-menu__label">Task</span>
        </a>
      </li>
    </ul>
  </aside>

  <main class="app-content">
    <div class="app-title">
      <div>
        <h5>Update Progres</h5>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fas fa-list-ul fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="data_tabel">Update Progres</a></li>
      </ul>
    </div>
    <div class="card-mb" style="background-color: #f5f6fa">
      <form enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().'Staff_Lain/tambah_log_staff_lain'; ?>" method="post" >
       <div id="kiri">
         <div class="card-mb">
          <?php foreach($edit_prog as $u){ ?>
            <div class="form-group">
              <div class="col-sm-7">
                <label for="usr">Nama Project</label>
                <input type="text" name="nama_project" value="<?php echo $u->nama_project ?>" class="form-control" readonly>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-7">
                <label for="usr">Nama PIC</label>
                <input type="hidden" name="id_user" value="<?php echo $u->id_user ?>">
                <input type="text" value="<?php echo $u->nama_depan ?>" class="form-control" readonly>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-7">
                <label for="usr">Instansi</label>
                <input type="text" name="instansi" value="<?php echo $u->instansi ?>" class="form-control" readonly>
              </div>
            </div>
            <input type="hidden" name="tgl_update" value="<?php date_default_timezone_set("Asia/Bangkok"); echo date ("Y-m-d H:i:s");?>">
            <input type="hidden" name="type" value="<?php echo $u->type ?>">
            <input type="hidden" name="divisi" value="<?php echo $u->divisi ?>">
          <?php } ?>
          <div class="form-group">
            <div class="col-sm-10">
              <label for="usr">Rincian Pekerjaan</label>
              <script src="<?php echo base_url('assets/docs/tinymce/js/tinymce.min.js')?>"></script>
              <script>tinymce.init({ selector:'textarea' });</script>
              <textarea name="rincian" class="form-control"></textarea>
            </div>
          </div>
        </div> 
      </div>

      <div id="kanan">
        <div class="card-mb">
          <div class="form-group">
            <div class="col-sm-10">
              <label for="usr">Pilih Staff</label><br>
              <?php
              foreach($data_mail as $h){
                ?>
                <input type="hidden" name="email_group[]" value="<?php echo $h->email?>">
                <?php 
              }
              ?>
              <select class="js-example-basic-multiple" name="email_group[]" multiple="multiple" type="hidden">
                <?php
                foreach($mail_data as $pic){
                  if($pic->level == "direktur" || $pic->level == "manager" 
                    || $pic->level == "admin" || $pic->email == $this->session->userdata('email') ){
                      ?>
                      <option hidden></option>
                      <?php
                    }else if(in_array($pic->email,$email_exist)){?>
                      <option value="<?php echo $pic->email?>"selected><?php echo $pic->nama_depan?></option>
                      ?>
                      <?php  
                    }else{
                      ?>
                      <option value="<?php echo $pic->email?>"><?php echo $pic->nama_depan?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-5">
                <label for="usr">Progress (%)</label>
                <input type="text" name="progres" class="form-control" required="true">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-10">
                <label for="usr">Upload File Pendukung</label>                            
                <input type="file" multiple name="userfile[]" size="20" class="form-control">                                 
              </div>
            </div>
            
            <div class="form-group-sm">
              <div class="col-sm-5">
                <label for="usr">Status Project</label>
                <select class="form-control" name="status">
                  <option value="running" selected>running</option>
                  <option value="closed">closed</option>
                  <option value="failed">failed</option>
                </select>
              </div>
              <div class="col-sm-5">
               <br>
               <button type="submit" name="Simpan" class="btn btn-info btn-sm">Update</button>
             </div>
           </div>
         </div>
       </div>
     </form>
   </div>
</main>

<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
    $('.js-example-basic-multiple').select2({width: '100%'},{theme: "classic"});
    $('#log_staff').on('submit',function(e) {  
      $.ajax({
        url:'<?php echo base_url().'Staff/tambah_log_staff';?>',
        data:$(this).serialize(),
        type:'POST',
        success:function(data){
          console.log(data);
          swal("Success!", "Update!", "success");
          window.location = 'daily_report_staff';
        },
        error:function(data){
          swal("Oops...", "Something went wrong :(", "error");
        }
      });
      e.preventDefault(); 
    });
  });
</script>

</body>
</html>
<style type="text/css">
#kiri
{
  width:60%;
  height:75%;
  float:left;
  background-color:white;
}
#kanan
{
  width:40%;
  height:75%;
  float:right;
  background-color:white;
}
      .circle {
        border-radius: 100px !important;
        overflow: hidden;
        width: 65px;
        height: 65px;
        left: 10%;
        border: 0px solid rgba(255, 255, 255, 0.7);
      }
</style>

