<html lang="en">
<head>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="staff">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Staff/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
      <?php
      foreach ($profil as $v) {
        if($v->picture == ""){
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }else{
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }
      }
      ?>
      <div>
        <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
        <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
      </div>
    </div>

    <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="staff">
          <i class="app-menu__icon fas fa-chart-line"></i>
          <span class="app-menu__label">Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-list-ul"></i>
          <span class="app-menu__label">Data</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/daily_report_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Daily Report
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/tambah_data')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Tambah Project
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/cf_work_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              CF Work
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </aside>

  <main class="app-content">
    <div class="app-title">
      <div>
        <h1><i class="app-menu__icon fa fa-table"></i>Update Progres</h1>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-table fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="data_tabel">Update Progres</a></li>
      </ul>
    </div>
    <div class="card-mb" style="background-color: #f5f6fa">
      <form enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url().'Staff/tambah_log_staff'; ?>" method="post" >
        <div class="row" style="background-color: white;">
          <div class="col-sm-12">
           <div id="kiri">
             <div class="card-mb">
              <?php foreach($edit_prog as $u){ ?>
                <div class="form-group">
                  <div class="col-sm-7">
                    <label for="usr">Nama Project</label>
                    <input type="text" name="nama_project" value="<?php echo $u->nama_project ?>" class="form-control" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-7">
                    <label for="usr">Nama PIC</label>
                    <input type="hidden" name="id_user" value="<?php echo $u->id_user ?>">
                    <input type="text" value="<?php echo $u->nama_depan ?>" class="form-control" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-7">
                    <label for="usr">Instansi</label>
                    <input type="text" name="instansi" value="<?php echo $u->instansi ?>" class="form-control" readonly>
                  </div>
                </div>
                <input type="hidden" name="tgl_update" value="<?php date_default_timezone_set("Asia/Bangkok"); echo date ("Y-m-d H:i:s");?>">
                <input type="hidden" name="type" value="<?php echo $u->type ?>">
                <input type="hidden" name="divisi" value="<?php echo $u->divisi ?>">
              <?php } ?>
              <div class="form-group">
                <div class="col-sm-10">
                  <label for="usr">Rincian Pekerjaan</label>
                  <script src="<?php echo base_url('assets/docs/tinymce/js/tinymce.min.js')?>"></script>
                  <script>tinymce.init({ selector:'textarea' });</script>
                  <textarea name="rincian" class="form-control"></textarea>
                </div>
              </div>
            </div> 
          </div>

          <div id="kanan">
            <div class="card-mb">
              <div class="form-group">
                <div class="col-sm-10">
                  <label for="usr">Pilih Staff</label><br>
                  <?php
                  foreach($data_mail as $h){
                    ?>
                    <input type="hidden" name="email_group[]" value="<?php echo $h->email?>">
                    <?php 
                  }
                  ?>
                  <select class="js-example-basic-multiple" name="email_group[]" multiple="multiple">
                    <?php
                    $email = $this->session->userdata('email');
                    foreach($mail_data as $pic){
                     if($pic->used_by == $email ){?>
                      <option value="<?php echo $pic->email?>"selected><?php echo $pic->nama_depan?></option>
                      ?>

                      <?php  
                    }else if($pic->used_by != ""){
                      ?>
                      <option value="<?php echo $pic->email?>" disabled="disabled"><?php echo $pic->nama_depan?></option>
                      <?php
                    }else{
                      ?>
                      <option value="<?php echo $pic->email?>"><?php echo $pic->nama_depan?></option>
                      <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-5">
                <label for="usr">Progress (%)</label>
                <input type="number" name="progres" class="form-control" required="true">
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-10">
                <label for="usr">Upload File Pendukung</label>                            
                <input type="file" multiple name="userfile[]" size="20" class="form-control">                                 
              </div>
            </div>
            
            <div class="form-group-sm">
              <div class="col-sm-5">
                <label for="usr">Status Project</label>
                <select class="form-control" name="status">
                  <option value="running" selected>running</option>
                  <option value="closed">closed</option>
                  <option value="failed">failed</option>
                </select>
              </div>
              <div class="col-sm-5">
               <br>
               <button type="submit" name="Simpan" class="btn btn-info btn-sm">Update</button>
             </div>
           </div>
         </div>
       </div>
     </div>
     <div class="col-sm-12">
       <fieldset style="border: 1px solid #ddd !important;
       margin: 0;
       xmin-width: 0;
       padding: 10px;       
       position: relative;
       border-radius:4px;
       background-color:white;
       padding-left:10px!important;">
       <legend style="font-size:18px;
       font-weight:bold;
       margin-bottom: 0px; 
       width: 35%; 
       border: 1px solid #ddd;
       border-radius: 4px; 
       padding: 5px 5px 5px 10px; 
       background-color: #ffffff;">Keterangan Progress</legend>
       <table>
         <tr>
          <th scope="row" style="background-color: #C2185B;color: white;">Fase 1</th>
          <td>&nbsp; - &nbsp;Sudah ketempat klien tapi belum bertemu pemilik kerjaan</td>
        </tr>
        <tr>
          <th scope="row" style="background-color:#D81B60;color: white;">0 s/d 20 %</th>
          <td>&nbsp; - &nbsp;Sudah bertemu pemilik kerjaan dan sudah membahas kebutuhan IT</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah follow up yang pertama</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah follow up yang kedua</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #303F9F;color: white"><strong>Fase 2</strong></th>
          <td>&nbsp; - &nbsp;Sudah memberikan penawaran, RAB, KAK, pertama</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #3949AB;color: white">20 s/d 40 %</th>
          <td>&nbsp; - &nbsp;Sudah memberikan revisi RAB, KAK</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah memberikan revisi RAB, KAK kedua</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah survey pekerjaan dan tempat</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #0097A7;color: white">Fase 3</th>
          <td>&nbsp; - &nbsp;Sudah membicarakan untuk proses dan strategi lelang/PL</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #00ACC1;color: white">40 s/d 60 %</th>
          <td>&nbsp; - &nbsp;Sedang proses pembuatan administrati lelang/PL</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sedang proses Lelang/PL</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #AFB42B;color: white">Fase 4</th>
          <td>&nbsp; - &nbsp;Sudah proses kontrak</td>

        </tr>
        <tr>
          <th scope="row" style="background-color: #C0CA33;color: white">60 s/d 80 %</th>
          <td>&nbsp; - &nbsp;Sedang proses pekerjaan pendahuluan</td>

        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah proses penagihan pertama</td>

        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sedang proses perkembangan pekerjaan dan proses penagihan terakhir</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #F57C00;color: white">Fase 5</th>
          <td>&nbsp; - &nbsp;Pekerjan sudah selesai</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #FB8C00;color: white">80 s/d 100 %</th>
        </tr>
      </table>
    </fieldset>
  </div>
</form>
</div>
</main>

<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
    $('.js-example-basic-multiple').select2({width: '100%'},{theme: "classic"});
    $('#log_staff').on('submit',function(e) {  
      $.ajax({
        url:'<?php echo base_url().'Staff/tambah_log_staff';?>',
        data:$(this).serialize(),
        type:'POST',
        success:function(data){
          console.log(data);
          swal("Success!", "Update!", "success");
          window.location = 'daily_report_staff';
        },
        error:function(data){
          swal("Oops...", "Something went wrong :(", "error");
        }
      });
      e.preventDefault(); 
    });
  });
</script>

</body>
</html>
<style type="text/css">
#kiri
{
  width:60%;
  float:left;
  background-color:white;
}
#kanan
{
  width:40%;

  float:right;
  background-color:white;

}

.circle {
  border-radius: 100px !important;
  overflow: hidden;
  width: 65px;
  height: 65px;
  left: 10%;
  border: 0px solid rgba(255, 255, 255, 0.7);
}
</style>

