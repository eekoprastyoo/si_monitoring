 <?php
 function rupiah($angka){
  $hasil_rupiah = number_format($angka,0,',','.');
  return $hasil_rupiah;
}
?>
<html lang="en">
<head>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="staff">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Staff/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
     <?php
     foreach ($profil as $v) {
      if($v->picture == ""){
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }else{
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>"  alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }
    }
    ?>
    <div>
      <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
      <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
    </div>
  </div>

  <ul class="app-menu">
    <li>
      <a class="app-menu__item active" href="staff">
        <i class="app-menu__icon fas fa-chart-line"></i>
        <span class="app-menu__label">Dashboard</span>
      </a>
    </li>
    <li class="treeview">
      <a class="app-menu__item" href="" data-toggle="treeview">
        <i class="app-menu__icon fas fa-list-ul"></i>
        <span class="app-menu__label">Data</span>
        <i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li>
          <a class="treeview-item" href="<?php echo base_url('Staff/daily_report_staff')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Daily Report
          </a>
        </li>
        <li>
          <a class="treeview-item" href="<?php echo base_url('Staff/tambah_data')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Tambah Project
          </a>
        </li>
        <li>
          <a class="treeview-item" href="<?php echo base_url('Staff/cf_work_staff')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            CF Work
          </a>
        </li>
      </ul>
    </li>
  </ul>

</aside>

<main class="app-content">
  <div class="app-title">
    <div>
      <h5>Data Project</h5>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-list-ul fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="">Data Project</a></li>
    </ul>
  </div>

  <div class="card-body" style="background-color: #f5f6fa">
    <div class="table-responsive">
      <table id="example" class="table" style="width:100%;">
        <thead style="background-color: #f6e58d;">
          <tr>
            <th>No</th>
            <th>PIC</th>
            <th>Nama Project</th>
            <th>Instansi</th>
            <th>Type</th>
            <th>Divisi</th>
            <th>Planning Pekerjaan</th>
            <th>Nilai(SPK)</th>
            <th>Nomor SPK</th>
            <th>Tanggal</th>
          </tr>
        </thead>

        <tbody>
          <?php
          $no_urut = 1;
          foreach ($t_data_utama as $hasil) {
            ?>
            <tr>
              <td><?php echo $no_urut++ ?></td>
              <td><?php echo $hasil->nama_depan ?></td>
              <td><?php echo $hasil->nama_project ?></td>
              <td><?php echo $hasil->instansi ?></td>
              <td><?php echo $hasil->type ?></td>
              <td><?php echo $hasil->divisi ?></td>
              <td><?php echo rupiah($hasil->esti_pendapatan) ?></td>
              <td><?php echo rupiah($hasil->real_pendapatan) ?></td>
              <td><?php echo $hasil->no_spk ?></td>
              <td><?php echo $hasil->tanggal?></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

</main>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
     "bInfo" : false,
     "dom": '<"toolbar">frtip',
     "scrollY":        "50%",
     "scrollCollapse": true,
     "scrollX": true
   });
  });
</script>
<style type="text/css">
#kiri
{
  width:50%;
  height:100px;
  float:left;
}
#kanan
{
  width:50%;
  height:100px;
  float:right;
}
.circle {
  border-radius: 100px !important;
  overflow: hidden;
  width: 65px;
  height: 65px;
  left: 10%;
  border: 0px solid rgba(255, 255, 255, 0.7);
}
</style>
</body>
</html>
