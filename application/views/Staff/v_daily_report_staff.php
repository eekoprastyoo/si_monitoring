<html lang="en">
<head>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="staff">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Staff/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
      <?php
      foreach ($profil as $v) {
        if($v->picture == ""){
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }else{
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }
      }
      ?>
      <div>
        <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
        <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
      </div>
    </div>

    <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="staff">
          <i class="app-menu__icon fas fa-chart-line"></i>
          <span class="app-menu__label">Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-list-ul"></i>
          <span class="app-menu__label">Data</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/daily_report_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Daily Report
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/tambah_data')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Tambah Project
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/cf_work_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              CF Work
            </a>
          </li>
        </ul>
      </li>
    </ul>

  </aside>

  <main class="app-content">
    <div class="app-title">
      <div>
        <h5>Daily Report</h5>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fas fa-list-ul fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="">Daily Report</a></li>
      </ul>
    </div>
    <div>
    </div>
    <div class="card-body" style="background-color: #f5f6fa">
      <div class="table-responsive">
       <?php
       $message = $this->session->flashdata('message');
       $message_error = $this->session->flashdata('message_error');
       if (isset($message)) {
        echo '<div class="alert alert-success alert-dismissible fade show" id="success-alert">
        '.$message.'</div>';
        $this->session->unset_userdata('message');
      }
      if (isset($message_error)) {
        echo '<div class="alert alert-danger alert-dismissible fade show" id="success-alert">
        '.$message.'</div>';
        $this->session->unset_userdata('message_error');
      }
      ?>
      <table id="example" class="table" style="width:100%;">
        <thead style="background-color: #f6e58d;">
          <tr>
            <th>No</th>
            <th>Nama PIC</th>
            <th>Nama Project</th>
            <th>Instansi</th>
            <th>Status</th>
            <th>Progress</th>
            <th>Tanggal Update</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          <?php
          $no_urut = 1;
          foreach ($t_data_report as $hasil) {
            $tgl_akhir  = new DateTime(date(".".$hasil->last_update.""));
            $tgl_hitung  = new DateTime(date("Y-m-d H:i:s"));
            $difference = $tgl_hitung->diff($tgl_akhir);
            ?>
            <tr>
              <td><?php echo $no_urut++ ?></td>
              <td><?php echo $hasil->nama_depan ?></td>
              <td><?php echo $hasil->nama_project ?></td>
              <td><?php echo $hasil->instansi ?></td>
              <?php
              if($hasil->status_project == "running" && $difference->days > 5){
                ?>
                <td><center><span class="badge" style="background-color: #e58e26;color: white;">running</span></center></td>
                <?php
              }else if($hasil->status_project == "running"){
                ?>
                <td><center><span class="badge badge-success">running</span></center></td>
                <?php
              }
              else if($hasil->status_project == "closed"){
                ?>
                <td><center><span class="badge badge-info">closed</span></center></td>
                <?php
              }else{
                ?>
                <td><center><span class="badge badge-danger">failed</span></center></td>
                <?php
              }
              ?>
              <td><?php echo $hasil->progres."%" ?></td>
              <?php
              if($hasil->status_project == "closed"){
                ?>
                <td>
                  <center><span class="badge badge-info"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }else if($hasil->status_project == "failed"){
                ?>
                <td>
                  <center><span class="badge badge-danger"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }else if($difference->days > 5){
                ?>
                <td>
                  <center><span class="badge" style="background-color: #e58e26;color: white;"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }
              else{
                ?>
                <td>
                  <center><span class="badge badge-success"><?php echo $hasil->last_update ?></span></center>
                  <?php
                }
                ?>


                <td>
                  <div class="btn-group">
                    <form action="update_progres_staff" method="post">
                      <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                      <input type="hidden" name="nama_project" value="<?php echo $hasil->nama_project?>">
                      <input type="hidden" name="instansi" value="<?php echo $hasil->instansi?>">
                      <button type="submit" class="btn btn-default btn-sm" style="background-color: #f6e58d;width:100%;">Update</button>
                    </form>
                    <form action="tampil_log_staff" method="post">
                      <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                      <input type="hidden" name="nama_project" value="<?php echo $hasil->nama_project?>">
                      <input type="hidden" name="instansi" value="<?php echo $hasil->instansi?>">
                      <button type="submit" class="btn btn-info btn-sm" >Detail</button>
                    </form>
                  </div>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <fieldset style="border: 1px solid #ddd !important;
        margin: 0;
        xmin-width: 0;
        padding: 10px;       
        position: relative;
        border-radius:4px;
        background-color:white;
        padding-left:10px!important;">
        <legend style="font-size:18px;
        font-weight:bold;
        margin-bottom: 0px; 
        width: 35%; 
        border: 1px solid #ddd;
        border-radius: 4px; 
        padding: 5px 5px 5px 10px; 
        background-color: #ffffff;">Keterangan Progress</legend>
        <table>
         <tr>
          <th scope="row" style="background-color: #C2185B;color: white;">Fase 1</th>
          <td>&nbsp; - &nbsp;Sudah ketempat klien tapi belum bertemu pemilik kerjaan</td>
        </tr>
        <tr>
          <th scope="row" style="background-color:#D81B60;color: white;">0 s/d 20 %</th>
          <td>&nbsp; - &nbsp;Sudah bertemu pemilik kerjaan dan sudah membahas kebutuhan IT</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah follow up yang pertama</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah follow up yang kedua</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #303F9F;color: white"><strong>Fase 2</strong></th>
          <td>&nbsp; - &nbsp;Sudah memberikan penawaran, RAB, KAK, pertama</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #3949AB;color: white">20 s/d 40 %</th>
          <td>&nbsp; - &nbsp;Sudah memberikan revisi RAB, KAK</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah memberikan revisi RAB, KAK kedua</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah survey pekerjaan dan tempat</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #0097A7;color: white">Fase 3</th>
          <td>&nbsp; - &nbsp;Sudah membicarakan untuk proses dan strategi lelang/PL</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #00ACC1;color: white">40 s/d 60 %</th>
          <td>&nbsp; - &nbsp;Sedang proses pembuatan administrati lelang/PL</td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sedang proses Lelang/PL</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #AFB42B;color: white">Fase 4</th>
          <td>&nbsp; - &nbsp;Sudah proses kontrak</td>

        </tr>
        <tr>
          <th scope="row" style="background-color: #C0CA33;color: white">60 s/d 80 %</th>
          <td>&nbsp; - &nbsp;Sedang proses pekerjaan pendahuluan</td>

        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sudah proses penagihan pertama</td>

        </tr>
        <tr>
          <th scope="row"></th>
          <td>&nbsp; - &nbsp;Sedang proses perkembangan pekerjaan dan proses penagihan terakhir</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #F57C00;color: white">Fase 5</th>
          <td>&nbsp; - &nbsp;Pekerjan sudah selesai</td>
        </tr>
        <tr>
          <th scope="row" style="background-color: #FB8C00;color: white">80 s/d 100 %</th>
        </tr>
      </table>
    </fieldset>
  </div>
</div>
</div>

</main>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
     "bInfo" : false,
     "dom": '<"toolbar">frtip',
     "scrollY":        "50%",
     "scrollCollapse": true,
     "scrollX": true
   });
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
      $("#success-alert").slideUp(500);
    });
  });
</script>
<style type="text/css">
#kiri
{
  width:50%;
  height:100px;
  float:left;
}
#kanan
{
  width:50%;
  height:100px;
  float:right;
}
.circle {
  border-radius: 100px !important;
  overflow: hidden;
  width: 65px;
  height: 65px;
  left: 10%;
  border: 0px solid rgba(255, 255, 255, 0.7);
}
</style>
</body>
</html>
