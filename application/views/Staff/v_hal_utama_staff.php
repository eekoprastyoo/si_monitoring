<html lang="en">
<head>
  <?php
  function rupiah($angka){
    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
    return $hasil_rupiah;
  }
  ?>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="staff"></a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Staff/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
     <?php
     foreach ($profil as $v) {
      if($v->picture == ""){
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }else{
        ?>
        <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
        <?php
      }
    }
    ?>
    <div>
      <p class="app-sidebar__user-name"> <?php echo $this->session->userdata('nama');?></p>
      <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
    </div>
  </div>
  <ul class="app-menu">
    <li>
      <a class="app-menu__item active" href="staff">
        <i class="app-menu__icon fas fa-chart-line"></i>
        <span class="app-menu__label">Dashboard</span>
      </a>
    </li>
    <li class="treeview">
      <a class="app-menu__item" href="" data-toggle="treeview">
        <i class="app-menu__icon fas fa-list-ul"></i>
        <span class="app-menu__label">Data</span>
        <i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li>
          <a class="treeview-item" href="<?php echo base_url('Staff/daily_report_staff')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Daily Report
          </a>
        </li>
        <li>
          <a class="treeview-item" href="<?php echo base_url('Staff/tambah_data')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            Tambah Project
          </a>
        </li>
        <li>
          <a class="treeview-item" href="<?php echo base_url('Staff/cf_work_staff')?>">
            <!-- <i class="icon fab fa-uikit"></i> -->
            CF Work
          </a>
        </li>
      </ul>
    </li>
  </ul>
</aside>

<main class="app-content">
  <div class="app-title">
    <div>
      <h5>Dashboard</h5>
    </div>
    <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fas fa-chart-line fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="staff">Dashboard</a></li>
    </ul>
  </div>

  <div>
    <form method="post" action="<?php echo base_url().'Staff/staff';?>">
     <table>
      <tr>
        <td>
          <select name="tahun" class="form-control">
            <option value="<?php echo $search ?>"selected><?php echo $search ?></option>
            <?php  
            $thn_skr = date('Y');
            for ($x = $thn_skr; $x >= 2018; $x--) {
              ?>
              <option value="<?php echo $x ?>" selected><?php echo $x ?></option>
              <?php
            }
            ?>
          </select>
        </td>
        <td>
         <button class="btn btn-default btn-sm" type="submit" style="background-color: #079992;color: white;">Tahun</button>
       </td>
     </tr>
   </table>
 </form>
</div>

<div class="row">
  <div class="col-md-4 col-lg-4">
    <div class="widget-small primary coloured-icon"><i class="icon fas fa-solar-panel fa-3x"></i>
      <div class="info">
       <h5>Nilai Target Perusahaan</h5>
       <?php
       foreach ($hasil_target as $row) {
        ?>
        <p><b><?php echo rupiah($row->nilai); ?></b></p>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<div class="col-md-4 col-lg-4">
  <div class="widget-small primary coloured-icon"><i class="icon far fa-building fa-3x"></i>
    <div class="info">
      <h5>Nilai Planning Pekerjaan Anda <?php echo $search ?></h5>
      <?php
      foreach ($hasil_estimasi->result() as $row) {
        ?>
        <p><b><?php echo rupiah($row->tot_esti); ?></b></p>
        <?php
      }
      ?>
    </div>
  </div>
</div>

<div class="col-md-4 col-lg-4">
  <div class="widget-small primary coloured-icon"><i class="icon fas fa-user-check fa-3x"></i>
    <div class="info">
      <h5>Nilai Target Boss Anda <?php echo $search ?></h5>
      <?php
      foreach ($plan_pribadi as $row) {
        ?>
        <p><b><?php echo rupiah($row->nilai); ?></b></p>
        <?php
      }
      ?>
    </div>
  </div>
</div>
</div>

<div class="row">
  <div class="col-md-6 col-lg-6">
    <label><b><?php echo $this->session->userdata('nama');?>'s Graph <?php echo $search ?></b></label>
    <div class="widget-small primary coloured-icon">
      <div id="piechart" style="height:255px;width:500px;">
        <?php
        $data_string = '';
        foreach($data_pie as $key=>$data){
          $string = ($key == "tot_real")?"Total Pendapatan SPK":"Total Planning Pekerjaan";
          $warna  = ($key == "tot_real")?"#76A7FA":"#e5e4e2";
          $data_string .= "['$string'".",". $data ."],";
        }
        ?>
        <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Task', 'Hours per Day'],
              <?php
              echo $data_string;
              ?>
              ]);
            var options = {
              colors: ['#EF5350', '#9CCC65'],
              chartArea: {width:450, height:150},
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data,options);
          }
        </script>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-lg-6">
    <label><b>CAPAIAN TARGET <?php echo $this->session->userdata('nama');?>  <?php echo $search ?></b></label>
    <div class="row" style=" margin-left: -0.5%; width: 100.5%; height: 285px;">      
      <div class="widget-small primary coloured-icon">

        <div class="col-md-6 col-sm-6">
          <div class="widget-small primary coloured-icon">
            <table border="0" style="margin-top: 10%;">
              <tr style="height: 50px;">
                <td align="center" style=" background-color: #C0CA33">
                  <h5>Nilai Total Pendapatan(SPK) <?php echo $search ?></h5>
                </td>
              </tr>
              <tr style="height: 145px;">
                <td align="center" width="600px" style="background-color: #DCE775;">
                  <h4>
                    <?php
                    foreach ($hasil_real->result() as $row) {
                      ?>
                      <p><b><?php echo rupiah($row->tot_real); ?></b></p>
                      <?php
                    }
                    ?>
                  </h4>
                </td>
              </tr>
            </table>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
         <div class="widget-small primary coloured-icon">
          <table border="0" style="margin-top: 10%;">
            <tr style="height: 50px;">
              <td align="center" style=" background-color: #C0CA33">
                <h5>Target Proyek <?php echo $this->session->userdata('nama');?> </h5>
              </td>
            </tr>
            <tr style="height: 145px;">
              <td align="center" width="600px"  style="background-color: #E6EE9C;">
                <?php
                foreach ($detail_spk_user as $detail) {
                 $hasil_detail  = $detail['tot_spk'] - $detail['nilai'];
                 if($detail['tot_spk'] < $detail['nilai']){
                  ?>
                  <span class="badge badge-pill badge-warning badge-lg"><h4>Kurang</h4></span><br>
                  <?php
                  echo rupiah($hasil_detail );
                }else{
                  ?>
                  <span class="badge badge-pill badge-success"><h4>Success</h4></span><br>
                  <?php
                  echo "+ ".rupiah($hasil_detail );
                }
              }
              ?>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<div class="row" style="background-color: white; margin: 0.1%;">
  <div class="col-md-2 col-lg-2"></div>
  <div class="col-md-8 col-lg-8">
    <div align="center"><label><b>GRAFIK PENDAPATAN PERSONAL IN CHARGE</b></label></div>
    <div class="widget-small primary coloured-icon">
      <canvas id="myChart" style="background-color: white;">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
        <?php
        foreach($hasil as $data){
          $nama[] = $data->nama_depan;
          $tot_real[] = $data->tot_real;
        }
        ?>
        <script>
          var ctx = document.getElementById("myChart").getContext('2d');
          ctx.width = 200;
          var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: <?php echo json_encode ($nama);?>,
              datasets: [{
                data: <?php echo json_encode ($tot_real);?>,
                backgroundColor: [
                '#FFA726',
                '#D4E157',
                '#26A69A',
                '#26C6DA',
                '#AB47BC',
                '#EC407A',
                '#EF5350',
                '#66BB6A'
                ]
              }]
            },
            options: {
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero:true,
                    userCallback: function(value, index, values) {
                      value = value.toString();
                      value = value.split(/(?=(?:...)*$)/);
                      value = value.join('.');
                      return value;
                    }
                  }
                }]

              },
              title: {
                display: true,
                position: 'left',
                text: 'Nilai SPK'
              },
              legend: {
                labels: {
                  filter: function(legendItem, chartData) {
                  }
                }
              }
            }
          });
        </script>
      </canvas>
    </div>
  </div>
  <div class="col-md-2 col-lg-2"></div>
</div>
</main>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();
  });
</script>
<style type="text/css">
#kiri
{
  width:45%;
  float:left;
}
#kanan
{
  width:45%;
  float:right;
}
.circle {
  border-radius: 100px !important;
  overflow: hidden;
  width: 65px;
  height: 65px;
  left: 10%;
  border: 0px solid rgba(255, 255, 255, 0.7);
}
</style>
</body>
</html>
