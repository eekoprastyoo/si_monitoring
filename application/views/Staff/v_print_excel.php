<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan_Log.xls");
?>
<table style="width:100%;" border="1">
	<tr>
		<td colspan="8" align="center">LAPORAN LOG PROJECT</td>
	</tr>
	<tr>
		<th>No</th>
		<th>Nama PIC</th>
		<th>Nama Project</th>
		<th>Instansi</th>
		<th>Rincian Log</th>
		<th>Progress Log</th>
		<th>Update Log</th>
		<th>Komentar</th>
	</tr>
	<?php
	$no = 1;
	foreach ($laporan as $hasil) {
		?>
		<tr>
			<td><?php echo $no++?></td>
			<td><?php echo $hasil->nama_depan ?></td>
			<td><?php echo $hasil->nama_project ?></td>
			<td><?php echo $hasil->instansi ?></td>
			<td><?php echo $hasil->rincian_log ?></td>
			<td><?php echo $hasil->progress_log?></td>
			<td><?php echo $hasil->update_log ?></td>
			<td><?php echo $hasil->komentar ?></td>
		</tr>
		<?php
	}
	?>
</table>