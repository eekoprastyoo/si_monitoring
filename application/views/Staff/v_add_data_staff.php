<html lang="en">
<head>
  <?php
  function rupiah($angka){
    $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
    return $hasil_rupiah;
  }
  ?>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">

</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="staff"></a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a>
          </li>
          <li><a class="dropdown-item" href="<?php echo base_url('Staff/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
      <?php
      foreach ($profil as $v) {
        if($v->picture == ""){
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }else{
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }
      }
      ?>
      <div>
        <p class="app-sidebar__user-name"> <?php echo $this->session->userdata('nama');?></p>
        <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
      </div>
    </div>
    <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="staff">
          <i class="app-menu__icon fas fa-chart-line"></i>
          <span class="app-menu__label">Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a class="app-menu__item" href="" data-toggle="treeview">
          <i class="app-menu__icon fas fa-list-ul"></i>
          <span class="app-menu__label">Data</span>
          <i class="treeview-indicator fa fa-angle-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/daily_report_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Daily Report
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/tambah_data')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              Tambah Project
            </a>
          </li>
          <li>
            <a class="treeview-item" href="<?php echo base_url('Staff/cf_work_staff')?>">
              <!-- <i class="icon fab fa-uikit"></i> -->
              CF Work
            </a>
          </li>
        </ul>
      </li>
    </ul>
  </aside>

  <main class="app-content">
    <div class="kiri" style="background-color: #f3f6fa">
      <form action="tambah_data_utama" method="post" class="tambah_project">
        <div class="card-mb">
          <div class="form-group">
            <div class="col-sm-3">
              <label for="usr">Nama PIC</label>
              <?php
              foreach($nama_staff as $r){
                ?>
                <input type="hidden" name="id_user" value="<?php echo $r->id_user?>">
                <input class="form-control" type="text" value="<?php echo $r->nama_depan ?>" readonly>
                <?php
              }
              ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-7">
              <label for="usr">Nama Project</label>
              <input type="text" name="nama_project" class="form-control" required="true">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-7">
              <label for="usr">Instansi</label>
              <input type="text" name="instansi" class="form-control" required="true">
            </div>
          </div>
          <div class="form-group">
            <div class="container">
              <div class="row">
                <div class="col-sm-3">
                  <label for="usr">Type</label>
                  <select class="form-control" name="type" required="true">
                    <option ></option>
                    <option value="New">New</option>
                    <option value="Existing">Maintenance</option>
                    <option value="Upgrade">Upgrade</option>
                    <option value="Downgrade">Downgrade</option>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="container">
              <div class="row">
                <div class="col-sm-3">
                  <label for="usr">Divisi</label>
                  <select class="form-control" name="divisi" required="true">
                    <option></option>
                    <?php
                    foreach($nama_dept as $row)
                    {
                      echo '<option value="'.$row->nama_department.'">'.$row->nama_department.'</option>';
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-3">
              <label for="usr">Nilai Planning Pekerjaan</label><br>
              <input type="text" name="est_pendapatan" class="testInput form-control" required="true"><span id="format"></span>
            </div>
          </div>

          <?php foreach ($mail_data as $pic){ ?>
            <select id="team" name="group_mail[]" hidden="true">
              <input type="checkbox" name="group_mail[]" value="<?php echo $pic->email ?>" 
              style="opacity:0; position:absolute; left:9999px;" checked>
            </select>
            <?php 
          } 
          ?>

          <div class="form-group">
            <div class="col-sm-3">
              <label for="usr">Nama PIC Instansi</label>
              <input type="text" name="pic_instansi" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-7">
              <label for="usr">No Telp PIC Instansi</label>
              <input type="text" name="no_telp" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-5">
              <label for="usr">Tanggal</label>
              <?php $date = date('Y-m-d h:i:sa'); ?>
              <input type="datetime-local" value="<?php echo date('Y-m-d\TH:i:s',strtotime($date));?>" name="tanggal" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-3">
              <button type="submit" name="upload" class="btn btn-info btn-sm" >Tambah</button>
            </div>
          </div>
        </div>
      </form>     
    </div>
  </main>
  <!-- Essential javascripts for application to work-->
  <script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
  <script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
  <script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
  <script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
  <script src="http://www.decorplanit.com/plugin/autoNumeric-1.9.41.js"></script>
  <script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#example').DataTable();
      $(".testInput").autoNumeric('init', {
        aSep: '.', 
        aDec: ',',
        aForm: true,
        vMax: '999999999999999',
        vMin: '-9'
      });
      $(".tambah_project").submit(function(event){
          event.preventDefault(); //prevent default action 
          var post_url = $(this).attr("action"); //get form action url
          var request_method = $(this).attr("method"); //get form GET/POST method
          var form_data = $(this).serialize(); //Encode form elements for submission
          console.log(form_data);
          if($("form")[0].checkValidity()) {
            $.ajax({
              url : post_url,
              type: request_method,
              data : form_data
            }).done(function(response){
              swal("Success!", "Data berhasil diinputkan", "success").then(function(){
                window.location = 'data_staff';
              });
            });
          }
        });
    });

  </script>
  <style type="text/css">
  #kiri
  {
    width:43%;
    float:left;
  }
  #kanan
  {
    width:43%;
    float:right;
  }
  .circle {
    border-radius: 100px !important;
    overflow: hidden;
    width: 65px;
    height: 65px;
    left: 10%;
    border: 0px solid rgba(255, 255, 255, 0.7);
  }
</style>

</body>
</html>