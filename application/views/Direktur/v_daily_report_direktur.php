<html lang="en">
<head>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">

</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="direktur">
      <p></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('Direktur/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
      <?php
      foreach ($profil as $v) {
        if($v->picture == ""){
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }else{
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }
      }
      ?>
      <div>
        <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
        <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
      </div>
    </div>

    <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="direktur">
          <i class="app-menu__icon fas fa-chart-line"></i>
          <span class="app-menu__label">Dashboard</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item active" href="daily_report_direktur">
          <i class="app-menu__icon fas fa-list-ul"></i>
          <span class="app-menu__label">Daily Report</span>
        </a>
      </li>
    </ul>
  </aside>

  <main class="app-content">
    <div class="app-title">
      <div>
        <h5>Daily Report</h5>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fas fa-list-ul fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="daily_report_direktur">Daily Report</a></li>
      </ul>
    </div>
    <div class="card-body" style="background-color: #f5f6fa">
      <div class="table-responsive">
       <table id="example" class="table" style="width:100%;">
        <thead style="background-color: #f6e58d;">
          <tr>
            <th>No</th>
            <th>Nama PIC</th>
            <th>Nama Project</th>
            <th>Instansi</th>
            <th>Status</th>
            <th>Progress</th>
            <th>Tanggal Update</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
          <?php
          $no_urut = 1;
          foreach ($t_data_report as $hasil) {
            $tgl_akhir  = new DateTime(date(".".$hasil->last_update.""));
            $tgl_hitung  = new DateTime(date("Y-m-d H:i:s"));
            $difference = $tgl_hitung->diff($tgl_akhir);
            ?>
            <tr>
              <td><?php echo $no_urut++ ?></td>
              <td><?php echo $hasil->nama_depan ?></td>
              <td><?php echo $hasil->nama_project ?></td>
              <td><?php echo $hasil->instansi ?></td>
              <?php
              if($hasil->status_project == "running" && $difference->days > 5){
                ?>
                <td><center><span class="badge" style="background-color: #e58e26;color: white;">running</span></center></td>
                <?php
              }else if($hasil->status_project == "running"){
                ?>
                <td><center><span class="badge badge-success">running</span></center></td>
                <?php
              }
              else if($hasil->status_project == "closed"){
                ?>
                <td><center><span class="badge badge-info">closed</span></center></td>
                <?php
              }else{
                ?>
                <td><center><span class="badge badge-danger">failed</span></center></td>
                <?php
              }
              ?>
              <td><?php echo $hasil->progres."%" ?></td>

              <?php
              if($hasil->status_project == "closed"){
                ?>
                <td>
                  <center><span class="badge badge-info"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }else if($hasil->status_project == "failed"){
                ?>
                <td>
                  <center><span class="badge badge-danger"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }else if($difference->days > 5){
                ?>
                <td>
                  <center><span class="badge" style="background-color: #e58e26;color: white;"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }
              else{
                ?>
                <td>
                  <center><span class="badge badge-success"><?php echo $hasil->last_update ?></span></center>
                </td>
                <?php
              }
              ?>
              <td>
                <center>
                  <form action="detail_project_direktur" method="post">
                    <input type="hidden" name="id_user" value="<?php echo $hasil->id_user;?>">
                    <input type="hidden" name="project" value="<?php echo $hasil->nama_project;?>">
                    <input type="hidden" name="instan" value="<?php echo $hasil->instansi;?>">
                    <button type="submit" class="btn btn-info btn-sm">Detail</button>
                  </form>
                </center>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<!-- Essential javascripts for application to work-->
<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
<script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable(
    {
     "bInfo" : false,
     "dom": '<"toolbar">frtip',
     "scrollY":        "50%",
     "scrollCollapse": true,
     "scrollX": true
   });
  });
</script>
<style type="text/css">


.circle {
  border-radius: 100px !important;
  overflow: hidden;
  width: 65px;
  height: 65px;
  left: 10%;
  border: 0px solid rgba(255, 255, 255, 0.7);
}
</style>
</body>
</html>
