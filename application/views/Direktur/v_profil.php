<!doctype html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html">
	<title>Profil</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
	<script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
	<link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
</head>

<body>
	<div id="topbar">
		<?php
		$message = $this->session->flashdata('success');
		if (isset($message)) {
			echo '<div class="alert alert-success alert-dismissible fade show" id="success-alert">
			'.$message.'</div>';
			$this->session->unset_userdata('success');
		}
		$message = $this->session->flashdata('error');
		if (isset($message)) {
			echo '<div class="alert alert-danger alert-dismissible fade show" id="danger-alert">
			'.$message.'</div>';
			$this->session->unset_userdata('error');
		}
		?>
	</div>
	
	<div class="panel-body" style="margin-left: 15%; margin-right: 15%; padding-left: -15%; padding-right: -15%;">

		<div id="content" class="clearfix">
			<div class="container">
				<div class="row row-centered">		
					<div class="col-md-4 col-sm-4 col-centered">
						<?php
						foreach ($profil as $h) {
							?>
							<div class="userphoto" style="padding: 5%;">
								<?php
								if($h->picture == ""){
									?>
									<img src="<?php echo base_url('profile/auto.png')?>" class="avatar img-circle img-thumbnail circle" alt="avatar" width="150px" height="150px">
									<?php
								}else{
									?>
									<img src="<?php echo base_url("profile/$h->picture")?>" class="avatar img-circle img-thumbnail circle" alt="avatar" width="150px" height="150px">
									<?php
								}
								?>
								<div class="row">
									<form action="upload_profil" method="post" enctype="multipart/form-data">
										<div class="col-centered2 col-md-4">
											<div class="fileUpload btn btn-default btn-sm">
												<input type="file" name="userfile" class="file-upload upload" accept="image/*" /><span>Browse</span>

											</div>
										</div>
										<div class="col-centered2 col-md-4">
											<div class="fileUpload upload btn btn-default btn-sm" >
												<input type="submit" value="upload" class="upload"><span>Upload</span>
											</div>
										</div>
									</form>
									<?php
								}
								?>
							</div>
						</div>
					</div>
					<div class="col-md-8 col-sm-8 col-centered">
						<h1>Profile</h1>
						<div class="row row-centered">
							<div class="col-md-8 col-centered2">
								<nav id="profiletabs">
									<ul>
										<li><a href="#bio" class="sel">Bio</a></li>
										<li><a href="#ganti_pwd">Ganti Password</a></li>
									</ul>
								</nav>
							</div>
						</div>
						<div class="row row-centered">
							<div class="col-md-1 col-centered">
								<section id="bio">
									<?php
									foreach ($profil as $hasil) {
										?>
										<table class="table table-borderless">
											<tr>
												<td>Nama</td>
												<td><?php echo $hasil->nama_depan ?></td>
											</tr>
											<tr>
												<td>Email</td>
												<td><?php echo $hasil->email ?></td>
											</tr>
											<tr>
												<td>Jabatan</td>
												<td><?php echo $hasil->level ?></td>
											</tr>
										</table>
										<?php
									}
									?>
									<br>
									<br>
									<br>
									<br>
									<a href="direktur"><i class="fas fa-check btn">Selesai</i></a>
								</section>
							</div>
						</div>
						<section id="ganti_pwd" class="hidden">
							<form action="ganti_pwd" method="post">
								<fieldset>
									<legend>Email<legend>
										<input type="email" height="30px" name="email" placeholder="Email" required>
									</fieldset>
									<fieldset>
										<legend>Password Baru</legend>
										<input name="password_baru" type="password" placeholder="Password" id="password" required>
									</fieldset>
									<fieldset>
										<legend>Confirm Password</legend>
										<input type="password" placeholder="Confirm Password" id="confirm_password" required>
									</fieldset>
									<input type="submit" value="Confirm">
								</form>
							</section>
						</div>

					</div><!-- @end #content -->
				</div><!-- @end #w -->
			</div>
		</div>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {
				var readURL = function(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader();
						reader.onload = function (e) {
							$('.avatar').attr('src', e.target.result);
						}
						reader.readAsDataURL(input.files[0]);
					}
				}
				$(".file-upload").on('change', function(){
					readURL(this);
				});
				$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
					$("#success-alert").slideUp(500);
				});
				$("#danger-alert").fadeTo(2000, 500).slideUp(500, function(){
					$("#danger-alert").slideUp(500);
				});
			});
			$(function(){
				$('#profiletabs ul li a').on('click', function(e){
					e.preventDefault();
					var newcontent = $(this).attr('href');

					$('#profiletabs ul li a').removeClass('sel');
					$(this).addClass('sel');

					$('#content section').each(function(){
						if(!$(this).hasClass('hidden')) { $(this).addClass('hidden'); }
					});

					$(newcontent).removeClass('hidden');
				});
			});
			var password = document.getElementById("password")
			, confirm_password = document.getElementById("confirm_password");

			function validatePassword(){
				if(password.value != confirm_password.value) {
					confirm_password.setCustomValidity("Passwords Don't Match");
				} else {
					confirm_password.setCustomValidity('');
				}
			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
		</script>

		<style type="text/css">
		html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 100%;
			font: inherit;
			vertical-align: baseline;
			outline: none;
			-webkit-font-smoothing: antialiased;
			-webkit-text-size-adjust: 100%;
			-ms-text-size-adjust: 100%;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}
		html { overflow-y: scroll; }
		body { 
			background: #f0f0f0; 
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			color: #313131;
			font-size: 62.5%; 
			line-height: 1; 
		}

		::selection { background: #a4dcec; }
		::-moz-selection { background: #a4dcec; }
		::-webkit-selection { background: #a4dcec; }

		::-webkit-input-placeholder { /* WebKit browsers */
			color: #ccc;
			font-style: italic;
		}
		:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
			color: #ccc;
			font-style: italic;
		}
		::-moz-placeholder { /* Mozilla Firefox 19+ */
			color: #ccc;
			font-style: italic;
		}
		:-ms-input-placeholder { /* Internet Explorer 10+ */
			color: #ccc !important;
			font-style: italic;  
		}

		br { display: block; line-height: 2.2em; } 

		article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section { display: block; }
		ol, ul { list-style: none; }

		input, textarea { 
			-webkit-font-smoothing: antialiased;
			-webkit-text-size-adjust: 100%;
			-ms-text-size-adjust: 100%;
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
			outline: none; 
		}

		blockquote, q { quotes: none; }
		blockquote:before, blockquote:after, q:before, q:after { content: ''; content: none; }
		strong { font-weight: bold; } 

		table { border-collapse: collapse; border-spacing: 0; }
		img { border: 0; max-width: 100%; }

		#topbar {
			background: #079992;
			padding: 10px 0 10px 0;
			text-align: center;
			height: 36px;
			overflow: hidden;
			-webkit-transition: height 0.5s linear;
			-moz-transition: height 0.5s linear;
			transition: height 0.5s linear;
		}
		#topbar a {
			color: #fff;
			font-size:1.3em;
			line-height: 1.25em;
			text-decoration: none;
			opacity: 0.5;
			font-weight: bold;
		}
		#topbar a:hover {
			opacity: 1;
		}

		/** typography **/
		h1 {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 2.5em;
			line-height: 1em;
			letter-spacing: -0.01em;
			margin-bottom: 15px;
			padding: .1em 0;
			color: #444;
			position: relative;
			overflow: hidden;
			white-space: nowrap;
			text-align: center;
		}
		h1:before,
		h1:after {
			content: "";
			position: relative;
			display: inline-block;
			width: 50%;
			height: 2px;
			vertical-align: middle;
			background: #f0f0f0;
		}
		h1:before {    
			left: -.5em;
			margin: 0 0 0 -50%;
		}
		h1:after {    
			left: .5em;
			margin: 0 -50% 0 0;
		}
		h1 > span {
			display: inline-block;
			vertical-align: middle;
			white-space: normal;
		}


		h2 {
			font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-size: 2.1em;
			line-height: 1.4em;
			letter-spacing: normal;
			margin-bottom: 20px;
			padding: .1em 0;
			color: #444;
			position: relative;
			overflow: hidden;
			white-space: nowrap;
			text-align: center;
		}

		p {
			display: block;
			font-size: 1.4em;
			line-height: 1.55em;
			margin-bottom: 22px;
			color: #555;
		}

		a { color: #5a9352; text-decoration: none; }
		a:hover { text-decoration: underline; }

		.center { display: block; text-align: center; }

		/** page structure **/
		.kepala {
			display: block;
			width: 750px;
			margin: 0 auto;
			padding-top: 30px;
			padding-bottom: 45px;
		}

		.row-centered{
			text-align: center;
		}
		.col-centered{
			display: inline-block;
			float: none;
			text-align: left;
			margin-right: -4px;
			position:relative;
		}
		.col-centered2{
			display: inline-block;
			float: none;
			text-align: center;
			margin-right: -4px;
			position:relative;
		}
		.fileUpload {
			position: relative;
			overflow: hidden;
			margin:5px;
		}
		.fileUpload input.upload {
			position: absolute;
			top: 0;
			right: 0;
			margin: 0;
			padding: 0;
			font-size: 10px;
			font-size: 10;
			cursor: pointer;
			opacity: 0;
			filter: alpha(opacity=0);
		}
		.circle{
			border-radius: 10px !important;
			overflow: hidden;
			width: 128px;
			height: 128px;
			border: 3px solid rgba(255, 255, 255, 0.7);
		}
		.table-borderless td,
		.table-borderless th {
			border: 0;
			font-size: 12px;
			padding-left: 10px;
		}
		#content {
			display: block;
			width: 100%;
			background: #fff;
			padding: 25px 20px;
			padding-bottom: 35px;
			-webkit-box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 2px 0px;
			-moz-box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 2px 0px;
			box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 2px 0px;
		}

		.userphoto {
			display: block;
			float: none;
			margin-left: 20%;
			margin-right: 20%;
			margin-bottom: 8px;
		}
		.userphoto img {
			display: block;
			padding: 2px;
			margin-left: 1%;
			background: #fff;
			-webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.4);
			-moz-box-shadow: 0 1px 3px rgba(0,0,0,0.4);
			box-shadow: 0 1px 3px rgba(0,0,0,0.4);
		}


		/** profile nav links **/
		#profiletabs {
			display: block;
			margin-bottom: 4%;
			height: 20%;
		}

		#profiletabs ul { list-style: none; display: block; width: 70%; height: 50px; }
		#profiletabls ul li { float: left; }
		#profiletabs ul li a { 
			display: block;
			float: left;
			padding: 8px 11px;
			font-size: 1.2em;
			font-weight: bold;
			background: #eae8db;
			color: #666;
			margin-right: 7px;
			border: 1px solid #fff;
			-webkit-border-radius: 5px;
			-webkit-border-bottom-left-radius: 0;
			-moz-border-radius: 5px;
			-moz-border-radius-bottomleft: 0;
			border-radius: 5px;
			border-bottom-left-radius: 0;
		}
		#profiletabs ul li a:hover {
			text-decoration: none;
			background: #dad7c2;
			color: #565656;
		}

		#profiletabs ul li a.sel {
			background: #fff;
			border-color: #d1cdb5;
		}


		/** profile content sections **/
		.hidden {
			display: none;
		}

		.activity {
			border-bottom: 1px solid #d6d1af;
			padding-bottom: 4px;
		}

		.setting {
			display: block;
			font-weight: normal;
			padding: 7px 3px;
			border-top: 1px solid #d6d1af;
			margin-bottom: 5px;
		}
		.setting span {
			float: left; 
			width: 250px;
			font-weight: bold;
		}
		.setting span img { 
			cursor: pointer;
		}


		#friendslist {
			display: block;
			font-size: 1.1em;
			font-weight: bold;
		}
		#friendslist li { line-height: 30px; }
		#friendslist li a {
			float: left;
			height: 30px;
			padding: 3px 6px;
			line-height: 22px;
			margin-right: 15px;
			border: 1px solid #c9d8b8;
		}

		#friendslist li a img { float: left; margin-right: 5px; }

		/** clearfix **/
		.clearfix:after { content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0; }
		.clearfix { display: inline-block; }

		html[xmlns] .clearfix { display: block; }
		* html .clearfix { height: 1%; }
	</style>
</body>
</html>