<html lang="en">
<head>
  <?php
  function rupiah($angka){
    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
    return $hasil_rupiah;
  }
  function uang($angka){
    $hasil_rupiah = number_format($angka,0,',','.');
    return $hasil_rupiah;
  }
  ?>
  <title>Sistem informasi monitoring marketing</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Main CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url ('assets/docs/css/main.css')?>">
  <!-- Font-icon css-->
  <link rel="stylesheet" href="<?php echo base_url ('assets/docs/font-awesome/css/all.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css')?>">
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>
<body class="app sidebar-mini rtl">
  <!-- Navbar-->
  <header class="app-header">
    <a class="app-header__logo" href="direktur">
      <p class="app-sidebar__user-designation"></p>
    </a>
    <a class="app-sidebar__toggle fas fa-bars" href="#" data-toggle="sidebar" aria-label="Hide Sidebar" style="padding-top: 10px;"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown">
        <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
          <?php echo $this->session->userdata('nama');?>
          <i class="fas fa-user fa-lg"></i>
        </a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="<?php echo base_url('Login/logout') ?>"><i class="fas fa-sign-out-alt fa-lg"></i> Logout</a></li>
          <li><a class="dropdown-item" href="<?php echo base_url('Direktur/show_profil')?>"><i class="fas fa-cog"></i> Settings</a>
          </li>
        </ul>
      </li>
    </ul>
  </header>
  <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user">
      <?php
      foreach ($profil as $v) {
        if($v->picture == ""){
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/auto.png");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }else{
          ?>
          <img class="app-sidebar__user-avatar circle" src="<?php echo base_url("profile/$v->picture");?>" alt="User Image" style="overflow: hidden; max-width: 65px; max-height: 100%;">
          <?php
        }
      }
      ?>
      <div>
        <p class="app-sidebar__user-name"><?php echo $this->session->userdata('nama');?></p>
        <p class="app-sidebar__user-designation"><?php echo $this->session->userdata('status');?></p>
      </div>
    </div>
    <ul class="app-menu">
      <li>
        <a class="app-menu__item active" href="direktur">
          <i class="app-menu__icon fas fa-chart-line"></i>
          <span class="app-menu__label">Dashboard</span>
        </a>
      </li>
      <li>
        <a class="app-menu__item active" href="daily_report_direktur">
          <i class="app-menu__icon fas fa-list-ul"></i>
          <span class="app-menu__label">Daily Report</span>
        </a>
      </li>
    </ul>
  </aside>

  <main class="app-content">
    <div class="app-title">
      <div>
        <h5>Dashboard</h5>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fas fa-chart-line"></i></li>
        <li class="breadcrumb-item"><a href="direktur">Dashboard</a></li>
      </ul>
    </div>

    <div>
      <form method="post" action="<?php echo base_url().'Direktur/direktur';?>">
        <table>
          <tr>
            <td>
              <select name="tahun" class="form-control">
                <option value="<?php echo $search ?>"selected><?php echo $search ?></option>
                <?php  
                $thn_skr = date('Y');
                for ($x = $thn_skr; $x >= 2018; $x--) {
                  ?>
                  <option value="<?php echo $x ?>" selected><?php echo $x ?></option>
                  <?php
                }
                ?>
              </select>
            </td>
            <td>
             <button class="btn btn-default btn-sm" type="submit" style="background-color: #079992;color: white;">Tahun</button>
           </td>
         </tr>
       </table>
     </form>
   </div>

   <div class="row">
    <div class="col-md-6 col-lg-4">
      <div class="widget-small primary coloured-icon"><i class="icon fas fa-solar-panel fa-3x"></i>
        <div class="info">
          <h5>Nilai Target Perusahaan</h5>
          <?php
          foreach ($hasil_target->result() as $row) {
            ?>
            <p><b><?php echo rupiah($row->nilai); ?></b></p>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-4">
      <div class="widget-small primary coloured-icon"><i class="icon far fa-building fa-3x"></i>
        <div class="info">
          <h5>Nilai Total Planning Pekerjaan</h5>
          <?php
          foreach ($hasil_estimasi->result() as $row) {
            ?>
            <p><b><?php echo rupiah($row->tot_esti); ?></b></p>
            <?php
          }
          ?>
        </div>
      </div>
    </div>

    <div class="col-md-6 col-lg-4">
      <div class="widget-small primary coloured-icon"><i class="icon fas fa-user-check fa-3x"></i>
        <div class="info">
          <h5>Nilai Total Pendapatan(SPK)</h5>
          <?php
          foreach ($hasil_real->result() as $row) {
            ?>
            <p><b><?php echo rupiah($row->tot_real); ?></b></p>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  
  <div class="row">
   <div class="col-md-6 col-lg-6">
    <label><b>GRAFIK PERBANDINGAN PLANNING DENGAN PENDAPATAN (SPK)</b></label>
    <div class="widget-small primary coloured-icon">
      <div id="piechart" style="height:255px; width: 500px;">
       <?php
       $data_string = '';    
       foreach($data as $key=>$data){
        $data_string .= "['Total SPK'".",". $data ."]";
      }
      ?>

      <?php
      $total_string = '';    
      foreach($data_total as $key=>$data_total){
        $total_string .= "['Total Planning Perusahaan'".",". $data_total ."],";
      }
      ?>
        <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Task', 'Hours per Day'],
              <?php
              echo $total_string;
              echo $data_string;
              ?>
              ]);
            var options = {
              colors: ['#EF5350', '#9CCC65'],
              chartArea: {width:450, height:150},
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data,options);
          }
        </script>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-lg-6">
    <label><b>GRAFIK PENDAPATAN PERSONAL IN CHARGE</b></label>
    <div class="widget-small primary coloured-icon">
      <canvas id="myChart" style="background-color: white;">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
        <?php
        foreach($hasil as $data){
          $nama_pic[] = $data->nama_depan;
          $tot_real[] = $data->tot_real;
        }
        ?>
        <script>
          var ctx = document.getElementById("myChart").getContext('2d');
          ctx.width = 200;
          var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
              labels: <?php echo json_encode ($nama_pic);?>,
              datasets: [{
                data: <?php echo json_encode ($tot_real);?>,
                backgroundColor: [
                '#FFA726',
                '#D4E157',
                '#26A69A',
                '#26C6DA',
                '#AB47BC',
                '#EC407A',
                '#EF5350',
                '#66BB6A'
                ]
              }]
            },
            options: {
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero:true,
                    userCallback: function(value, index, values) {
                      value = value.toString();
                      value = value.split(/(?=(?:...)*$)/);
                      value = value.join('.');
                      return value;
                    }
                  }
                }]

              },
              title: {
                display: true,
                position: 'left',
                text: 'Nilai SPK'
              },
              legend: {
                labels: {
                  filter: function(legendItem, chartData) {
                  }
                }
              }
            }
          });
        </script>
      </canvas>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-lg-6">
    <label><b>GRAFIK PERBANDINGAN PENDAPATAN PER DIVISI</b></label>
    <div class="widget-small primary coloured-icon">
      <div id="asd" style="height: 255px;width: 500px;">
        <?php  
        $pie = "";
        foreach($h_pie as $pie_h){
          $pie .= "['$pie_h->divisi'".",". $pie_h->tot_real."],";
        }
        ?>
        <script type="text/javascript">
          google.charts.load('current', {'packages':['corechart']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Task', 'Hours per Day'],
              <?php echo $pie ?>
              ]);

            var options = {
              pieHole: 0.5,
              chartArea: {width:465, height:150},
            };

            var chart = new google.visualization.PieChart(document.getElementById('asd'));
            chart.draw(data, options);
          }
        </script>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-lg-6">
    <label><b>NILAI PENDAPATAN PERSONAL IN CHARGE</b></label>
    <div class="widget-small primary coloured-icon">
      <div style="width: 100%;background-color: white;">
        <div class="table-responsive">
          <table id="example" class="table table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama PIC</th>
                <th>Jumlah</th>
              </tr>
            </thead>

            <tbody>
              <?php
              $no_urut = 1;
              foreach ($hasil_tabel as $data) {
                ?>
                <tr>
                  <td><?php echo $no_urut++ ?></td>
                  <td><?php echo $data->nama_depan?></td>
                  <td><?php echo uang($data->tot_real)?></td>
                </tr>
                <?php
              }
              ?>

            </tbody>
          </table>
        </div>
      </div>

      <div id="detail-pendapatan" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- konten modal-->
          <div class="modal-content">
            <!-- heading modal -->
            <div class="modal-header">
              <h5 class="modal-title">Tabel Pendapatan</h5>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- body modal -->
            <div class="modal-body">
              <div class="card-mb">  
                <div class="table-responsive">
                  <table id="example" class="table table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama PIC</th>
                        <th>Jumlah</th>
                      </tr>
                    </thead>

                    <tbody>
                      <?php
                      $no_urut = 1;
                      foreach ($hasil_tabel as $data) {
                        ?>
                        <tr>
                          <td><?php echo $no_urut++ ?></td>
                          <td><?php echo $data->nama_depan?></td>
                          <td><?php echo uang($data->tot_real)?></td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </form>
          </div>
          <!-- footer modal -->
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" style="color: white;background-color: #e91e63">Close</button>
          </div>
        </main>
        <script src="<?php echo base_url('assets/docs/js/jquery-3.2.1.min.js')?>"></script>
        <script src="<?php echo base_url('assets/docs/js/popper.min.js')?>"></script>
        <script src="<?php echo base_url('assets/docs/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/docs/js/main.js')?>"></script>
        <script src="<?php echo base_url('assets/datatables/datatables.min.js')?>"></script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('#example').DataTable(
            {
              "autoWidth": false,
              "pageLength": 3,
              "searching": false,
              "bLengthChange": false,
              "bInfo" : false,
              "dom": '<"toolbar">frtip'
            });
            $("div.toolbar").html('<form method="get" action="detail-pendapatan"><label data-toggle="modal" data-target="#detail-pendapatan" style="color: #16a085;background-color: transparent">Detail</label></form>');
          });
        </script>
        <style type="text/css">
        .toolbar {
          position: absolute;
          bottom: 45px;
          margin-left: 10px;
        }
        th, td { white-space: nowrap; }
        div.dataTables_wrapper {
          width: 90%;
          height:245px;
          margin-top: 10px;
          margin-left: 10px;
        }
        
        tr { height: 30px; }
        .circle {
          border-radius: 100px !important;
          overflow: hidden;
          width: 65px;
          height: 65px;
          left: 10%;
          border: 0px solid rgba(255, 255, 255, 0.7);
        }
      </style>
    </div>
  </div>
</div>  
</body>
</html>

