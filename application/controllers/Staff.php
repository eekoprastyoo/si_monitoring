<?php
class Staff extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->helper(array('url','download'));
		$this->load->model('M_data');
		$this->load->helper('string');	
	}
	function data_staff(){
		$data['t_data_utama'] = $this->M_data->get_data_staff()->result();
		$data['nama_staff'] = $this->M_data->tampil_nama_staff()->result();
		$data['nama_dept'] = $this->M_data->tampil_dept()->result();
		$data['mail_data'] = $this->M_data->get_data_user()->result();
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Staff/v_data_staff',$data);
	}
	function staff(){
		if($this->session->userdata('status') == "staff"){
			$tahun = $this->input->post('tahun');
			$data['hasil'] = $this->M_data->grafik($tahun);
			$data['data_pie'] = $this->M_data->grafik_pie_staff($tahun);
			$data['hasil_estimasi'] = $this->M_data->jumlah_estimasi_staff($tahun);
			$data['hasil_target'] = $this->M_data->jumlah_target($tahun)->result();
			$data['hasil_real'] = $this->M_data->jumlah_real_staff($tahun);
			$data['plan_pribadi'] = $this->M_data->plan_pribadi($tahun)->result();
			$data['search'] = $tahun;
			$data['data_tot'] = $this->M_data->grafik_pie($tahun);
			$data['detail_spk_user']= $this->M_data->detail_spk_user();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Staff/v_hal_utama_staff',$data);
		}else{
			redirect(base_url('Login'));
		}
	}
	function data_tabel_staff(){
		if($this->session->userdata('status') == "staff"){
			$data['t_data_utama'] = $this->M_data->tampil_data_staff()->result();
			$this->load->view('Staff/v_data_tabel_staff',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function daily_report_staff(){
		$id_login = $this->session->userdata('id_user');
		if($this->session->userdata('id_user') == "$id_login" && $this->session->userdata('status') == "staff"){
			$data['t_data_report'] = $this->M_data->tampil_data_report()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Staff/v_daily_report_staff',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function cf_work_staff(){
		$id_login = $this->session->userdata('id_user');
		if($this->session->userdata('id_user') == "$id_login" && $this->session->userdata('status') == "staff"){
			$data['t_data_report'] = $this->M_data->cf_work_staff()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Staff/v_cf_work_staff',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function cek_row($table,$where){		
		return $this->db->get_where($table,$where);
	}
	function tampil_log_staff(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');

		$data["hasil_gambar"] = $this->M_data->tampil_gambar()->result();
		$data["data_log"] = $this->M_data->log_detail($id_user,$nama_project,$instansi)->result();
		$data["nama_project"] = $this->M_data->nama_project_group()->result();
		$data["instansi_group"] = $this->M_data->instansi_group()->result();
	    $data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Staff/v_detail_log_staff',$data);
	}
	function show_file(){
		$id_log = $this->input->post('id_log');
		$data   = $this->M_data->show_file($id_log);
		$list_koma = explode(",",$data);

		foreach($list_koma as $h){
			echo "<table>
			<tr>
			<td>
			$h
			</td>
			<td align='center'>";
			echo "<a href='".base_url("/Staff/download/$h")."'>";
			echo "<i>Download</i></a>";
			echo "</td>
			</tr>
			</table>";
		}
	}
	function download($filename = NULL){    
		$file = $this->input->get('file');
		$data = array('file' => $file);   
		$this->load->helper('download');
		$data = file_get_contents(base_url('/uploads/'.$filename));
		force_download($filename, $data); 
	}
	function show_comment(){
		$id_log = $this->input->post('id_log');
		$data 	= $this->M_data->show_comment($id_log)->result();
		foreach($data as $h){
			echo "$h->komentar";	
		}
	}
	function kembali_log_staff(){
		$nama_pic = $this->input->get('nama_pic');
		$nama_project = $this->input->get('nama_project');
		$instansi = $this->input->get('instansi');

		$where = array('nama_pic' => $nama_pic,'nama_project' => $nama_project,'instansi' => $instansi);
		$data ["data_log"] = $this->M_data->log_detail($where,'t_log')->result();
		$this->load->view('Staff/v_detail_log_staff',$data);
	}
	function update_progres_staff(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');

		$where = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi);			
		$data['edit_prog']   = $this->M_data->tampil_update_log($id_user,$nama_project,$instansi)->result();
		$data['mail_data']   = $this->M_data->get_user_staff_lain()->result();
		$data['data_mail']   = $this->M_data->get_user()->result(); 
		$data['email_exist'] = explode(',',$this->M_data->exist_email_group($where));
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Staff/v_update_prog_staff',$data);
	}
	function upload_file(){
		if (!empty($_FILES)) {
			$tempFile	= $_FILES['file']['tmp_name'];
			$fileName 	= $_FILES['file']['name'];
			$targetPath = getcwd() . '/uploads/';
			$targetFile = $targetPath . $fileName ;
			move_uploaded_file($tempFile, $targetFile);
			
			$this->load->database();
			$this->db->insert('foto',array('nama' => $fileName));
		}
	}
	function set_session_team(){
		$email = $this->input->post('email_group');
		$data = array('nama_pic' =>$email);
		$this->db->insert('t_group_mail',$data);
		echo "Berhasil";
		//$this->session->set_userdata($session_email);
	}
	function tambah_log_staff(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$rincian_log = $this->input->post('rincian');
		$group  = $this->input->post('email_group');
		$status = $this->input->post('status');
		$data_email = implode(",",$group);
		$type = $this->input->post('type');
		$divisi = $this->input->post('divisi');

		$progres = $this->input->post('progres');
		$tanggal_update = $this->input->post('tgl_update');

		$this->load->library('upload');
		$files = $_FILES;
		$images = array();
		$cpt = count($_FILES['userfile']['name']);
		
		for($i=0; $i<$cpt; $i++){
			
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];    

			$this->upload->initialize($this->set_upload_options());
			$this->upload->do_upload();

			$images[] = $_FILES['userfile']['name'];
		}
		$fileName = implode(',',$images);
		$newfile_name= preg_replace('/\s+/','_', $fileName);

		$data_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi,
			'rincian_log' => $rincian_log,'progress_log' => $progres,'file' => $newfile_name,
			'update_log' => $tanggal_update,'email_group' => $data_email,'type' => $type,'divisi' => $divisi);

		$where_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' =>$instansi);

		$data_log_utama = array('progres' =>$progres ,'last_update' =>$tanggal_update,'email_group' =>$data_email,'status_project' =>$status);

		foreach ($group as $key) {
			$data['email'] = $key;
			$cek = $this->M_data->cek_level($data);
			$array = array();
			if($cek != $array){
				foreach ($cek as $key) {
					$id = $key;
					foreach ($id as $hasil) {
					$d_hasil['id_user'] = $hasil;
					$data_update = array('used_by' => $this->session->userdata('email'));
					$this->M_data->update_data($d_hasil,$data_update,'t_data_user');
					}
				}
			}else{
				$where = array('used_by' =>$this->session->userdata('email'));
				$data_update = array('used_by' => '');
				$this->M_data->update_data($where,$data_update,'t_data_user');
			}
		}

		$this->M_data->update_log_utama($where_log,$data_log_utama,'t_data_utama');
		$this->M_data->tambah_log ($data_log,'t_log');

		$email   = $this->M_data->data_user();
		$subject = "Daily Report";

		$mail_data['subject']  = 'Daily Report';
		$mail_data['all_log']  =  $this->M_data->kirim_log($nama_project,$instansi)->result();
		$mail_data['nama_pic'] =  $this->M_data->get_nama_pic($id_user);
		$mail_data['nama_project'] =  $this->input->post('nama_project');
		$mail_data['instansi'] =  $this->input->post('instansi');
		$mail_data['tanggal']  =  $this->input->post('tgl_update');
		$mail_data['description'] = "Update Progres";
		$mail_data['update_by'] = $this->session->userdata('nama');
		$message = $this->load->view('Staff/v_email_page',$mail_data,true);
		$this->sendEmail($email,$subject,$message);
	}
	private function set_upload_options(){   
		$config = array();
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = '*';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		return $config;
	}
	function tampil_detail_log_staff(){
		$id_log = $this->input->get('id_log');
		$hasil  = array('id_log' => $id_log);
		$t['data_log'] = $this->M_data->tampil_detail_log($hasil,'t_log')->result();
		$this->load->view('Staff/v_detail_rincian_staff',$t);
	}
	function sendEmail($email,$subject,$message){
		$email_group = $this->M_data->send_progres();
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'monitoringte2@gmail.com', 
			'smtp_pass' => 'januari18', 
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('monitoringte2@gmail.com');
		$this->email->to($email);
		$this->email->cc($email_group);
		$this->email->subject($subject);
		$this->email->message($message);
		   
		if($this->email->send()){
			$this->session->set_flashdata('message','<strong>Success!</strong> data project berhasil diupdate.');
			redirect('Staff/daily_report_staff');
		}else{
			$this->session->set_flashdata('message_error','<strong>Failed!</strong> Email gagal dikirim.');
			redirect('Staff/daily_report_staff');
		}
	}
	function Email(){
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'monitoringte2@gmail.com', 
			'smtp_pass' => 'januari18', 
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('monitoringte2@gmail.com');
		$this->email->to('eekoprastyoo@gmail.com');
		$this->email->cc('dodih1227@gmail.com,dodih127@gmail.com');
		$this->email->to('prastyo050497@gmail.com');
		$this->email->subject('testing');
		$this->email->message('oke');
		   // $this->email->attach('C:\xampp\htdocs\Magang\uploads\asd.jpg');
		
		if($this->email->send()){
			echo 'Email send.';
		}else{
			show_error($this->email->print_debugger());
		}
	}
	function test_email(){
		$email   = "prastyo050497@gmail.com";
		$subject = "Daily Report";
		
		$mail_data['subject'] = 'Daily Report';
		$mail_data['description'] = "Update Progres";

		$message = $this->load->view('Staff/v_email_page', $mail_data, true);
		$this->sendEmail($email,$subject,$message);
	}
	function print_excel(){
		$group_project  = $this->input->post('group_project');
		$instansi_group = $this->input->post('group_instansi');
		$data['laporan'] = $this->M_data->get_data_excel($group_project,$instansi_group)->result();
		$this->load->view('Staff/v_print_excel',$data);
	}
	function show_profil(){
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Staff/v_profil',$data);
	}
	function upload_profil(){
		$id = $this->session->userdata('id_user');
		$name= "$id";

		$config['upload_path']   = './profile/';
		$config['allowed_types'] = 'jpg';
		$config['max_size']      = '0';
		$config['overwrite'] 	 = TRUE;
		$config['max_filename']	 = '0';
		$config['remove_spaces'] = TRUE;
		$config['file_name']	 = $name;

		$this->load->library('upload', $config);
		$this->upload->do_upload('userfile');

		$data = array('upload_data' => $this->upload->data());
		$image= $data['upload_data']['file_name'];

		$this->load->helper("file");
		unlink(base_url("profile/$image"));

		$where = array('id_user' =>$id);
		$data  = array('picture' =>$image);
		if($this->upload->do_upload('userfile')){
			$this->M_data->update_data($where,$data,'t_data_user');
			$this->session->set_flashdata('success','<strong>Success!</strong>  Profil berhasil diupdate.');
			redirect('Staff/show_profil');
		}else{
			$this->session->set_flashdata('error','<strong>Erorr!</strong>  tidak bisa diupdate.');
			redirect('Staff/show_profil');
		}

	}
	function ganti_pwd(){
		$email = $this->input->post('email');
		$password  = $this->input->post('password_baru');
		$sel_email = $this->M_data->seleksi_email($email);

		if ($sel_email == 0) {
			$this->session->set_flashdata('error','<strong>Erorr!</strong>  Email tidak terdaftar.');
			redirect('Admin/show_profil');
		}else{
			$data = array('password' => $password);
			$where = array('email' =>$email);
			$this->M_data->update_data($where,$data,'t_data_user');
			$this->session->set_flashdata('success','<strong>Success!</strong>  Profil berhasil diupdate.');
			redirect('Staff/show_profil');	
		}
	}
	function tambah_data(){
		if($this->session->userdata('status') == "staff"){
			$id_user = $this->session->userdata('id_user');
			$data['t_data_utama']	= $this->M_data->get_data('t_data_utama')->result();
			$data['nama_staff'] 	= $this->M_data->nama_staff_khusus()->result();
			$data['nama_dept']		= $this->M_data->tampil_dept()->result();
			$data['mail_data']		= $this->M_data->add_data_email()->result();
			$data['profil'] 		= $this->M_data->show_profil()->result();
			$this->load->view('Staff/v_add_data_staff',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function tambah_data_utama(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$type = $this->input->post('type');
		$divisi = $this->input->post('divisi');

		$est_pendapatan = $this->input->post('est_pendapatan');
		$nilai_planning= str_replace(".","", $est_pendapatan);

		$tanggal = $this->input->post('tanggal');
		$pic_instansi = $this->input->post('pic_instansi');
		$no_telp = $this->input->post('no_telp');

		$email = $this->input->post('group_mail');
		$data_email = implode(",",$email);

		$rincian_log = "";
		$progres = "0";

		$data = array(
			'id_user' => $id_user,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'type' => $type,
			'divisi' => $divisi,
			'esti_pendapatan' => $nilai_planning,
			'tanggal' => $tanggal,
			'pic_instansi' => $pic_instansi,
			'no_telp' => $no_telp,
			'email_group' => $data_email,
			'progres' => $progres,
			'last_update' => $tanggal,
			'status_project' => "running"
		);
		$data_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi,'rincian_log' => $rincian_log,'progress_log' => $progres,'update_log' => $tanggal,'email_group' => $data_email);

		$this->M_data->input_data($data,'t_data_utama');
		$this->M_data->tambah_log ($data_log,'t_log');
	}
	function coba(){
		$this->load->view('Staff/v_email_page');
	}
	function cf_work(){
		if($this->session->userdata('status') == "staff"){
			$data['t_data_report'] = $this->M_data->tampil_cf_work()->result();
			$data['target']		   = $this->M_data->target()->result();
			$data['nama_staff']    = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	   = $this->M_data->tampil_dept()->result();
			$data['divisi']		   = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Staff/v_cf_work_staff',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
}
?>