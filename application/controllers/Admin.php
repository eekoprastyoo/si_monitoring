<?php
class Admin extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		$this->load->helper('string');
		$this->load->helper('file');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->session->keep_flashdata('message');
	}
	function coba(){
		$this->load->view('v_coba');
	}
	function admin(){
		if($this->session->userdata('status') == "admin"){
			$tahun = $this->input->post('tahun');
			$data['hasil'] = $this->M_data->grafik($tahun);
			$data['data'] = $this->M_data->grafik_pie($tahun);
			$data['data_total'] = $this->M_data->total_perusahaan($tahun);
			$data['hasil_estimasi'] = $this->M_data->jumlah_estimasi($tahun);
			$data['hasil_target'] = $this->M_data->jumlah_target($tahun)->result();
			$data['hasil_tabel'] = $this->M_data->tabel_grafik($tahun);
			$data['hasil_real'] = $this->M_data->jumlah_real($tahun);
			$data['jml_new'] = $this->M_data->jumlah_new($tahun);
			$data['jml_exist'] = $this->M_data->jumlah_exist($tahun);
			$data['jml_up'] = $this->M_data->jumlah_up($tahun);
			$data['jml_down'] = $this->M_data->jumlah_down($tahun);
			$data['h_pie'] = $this->M_data->grafik_donut($tahun);
			$data['search'] = $tahun;
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Admin/v_hal_utama_admin',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function ambil_data(){
		$id_user = $this->input->post('id_user');
		echo $this->M_data->hasil_email($id_user);
	}
	function show_profil(){
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Admin/v_profil',$data);
	}
	function show_pic(){
		$divisi = $this->input->post('divisi');
		echo $this->M_data->hasil_pic($divisi);
	}

	function show_pic_log(){
		$divisi = $this->input->post('divisi_log');
		echo $this->M_data->hasil_pic($divisi);
	}

	function show_pic_tahun(){
		$tahun = $this->input->post('tahun');
		echo $this->M_data->hasil_pic_tahun($tahun);
	}

	function show_pic_tahun_log(){
		$tahun = $this->input->post('tahun_log');
		echo $this->M_data->hasil_pic_tahun($tahun);
	}

	function show_divisi(){
		$tahun = $this->input->post('tahun');
		echo $this->M_data->hasil_divisi($tahun);
	}

	function show_divisi_log(){
		$tahun = $this->input->post('tahun_log');
		echo $this->M_data->hasil_divisi($tahun);
	}

	function show_divisi_pic(){
		$id_user = $this->input->post('pic');
		echo $this->M_data->hasil_divisi_pic($id_user);
	}

	function show_divisi_pic_log(){
		$id_user = $this->input->post('pic_log');
		echo $this->M_data->hasil_divisi_pic($id_user);
	}

	function show_type(){
		$id_user= $this->input->post('pic');
		echo $this->M_data->hasil_type($id_user);
	}

	function show_type_log(){
		$id_user= $this->input->post('pic_log');
		echo $this->M_data->hasil_type($id_user);
	}
	function show_type_divisi(){
		$divisi	= $this->input->post('divisi');
		echo $this->M_data->hasil_type_divisi($divisi);
	}
	function show_type_divisi_log(){
		$divisi	= $this->input->post('divisi_log');
		echo $this->M_data->hasil_type_divisi($divisi);
	}
	function show_type_tahun(){
		$tahun	= $this->input->post('tahun');
		echo $this->M_data->hasil_type_tahun($tahun);
	}
	function show_type_tahun_log(){
		$tahun	= $this->input->post('tahun_log');
		echo $this->M_data->hasil_type_tahun($tahun);
	}
	function export_excel(){
		$tahun 	  = $this->input->post('tahun');
		$divisi   = $this->input->post('divisi');
		$id_user  = $this->input->post('pic');
		$type     = $this->input->post('type');

		//Tahun Only
		if($tahun != "" && $divisi == "" && $id_user == "" && $type == ""){
			$data['laporan'] = $this->M_data->export_all($tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
		//tahun,divisi
		if($divisi != "" && $tahun != "" && $id_user == "" && $type == ""){
			$data['laporan'] = $this->M_data->export_4($divisi,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
		//tahun,id_user
		if($divisi == "" && $tahun != "" && $id_user != "" && $type == ""){
			$data['laporan'] = $this->M_data->export_7($tahun,$id_user)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
		//tahun,type
		if($divisi == "" && $id_user == "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_6($type,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}

		//tahun,divisi,id_user,
		if($divisi != "" && $id_user != "" && $type == "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_5($id_user,$divisi,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}

		//tahun,divisi,type
		if($divisi != "" && $id_user == "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_2($divisi,$type,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}

		//tahun,id_user,type
		if($divisi == "" && $type != "" && $id_user != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_3($id_user,$type,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
		
		//tahun,divisi,id_user,type
		if($divisi != "" && $id_user != "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_1($divisi,$id_user,$type,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
	}
	function export_excel_log(){
		$tahun 	  = $this->input->post('tahun_log');
		$divisi   = $this->input->post('divisi_log');
		$id_user  = $this->input->post('pic_log');
		$type     = $this->input->post('type_log');

		//Tahun Only
		if($tahun != "" && $divisi == "" && $id_user == "" && $type == ""){
			$data['laporan'] = $this->M_data->export_2_log($tahun)->result();
			$this->load->view('Admin/v_export_excel_log',$data);
		}
		//tahun,divisi
		if($divisi != "" && $tahun != "" && $id_user == "" && $type == ""){
			$data['laporan'] = $this->M_data->export_3_log($tahun,$divisi)->result();
			$this->load->view('Admin/v_export_excel_log',$data);
		}
		//tahun,id_user
		if($divisi == "" && $tahun != "" && $id_user != "" && $type == ""){
			$data['laporan'] = $this->M_data->export_4_log($tahun,$id_user)->result();
			$this->load->view('Admin/v_export_excel_log',$data);
		}
		//tahun,type
		if($divisi == "" && $id_user == "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_5_log($tahun,$type)->result();
			$this->load->view('Admin/v_export_excel_log',$data);
		}

		//tahun,divisi,id_user,
		if($divisi != "" && $id_user != "" && $type == "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_6_log($tahun,$divisi,$id_user)->result();
			$this->load->view('Admin/v_export_excel_log',$data);
		}

		//tahun,divisi,type
		if($divisi != "" && $id_user == "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_7_log($tahun,$divisi,$type)->result();
			$this->load->view('Admin/v_export_excel_log',$data);
		}

		//tahun,id_user,type
		if($divisi == "" && $type != "" && $id_user != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_8_log($tahun,$id_user,$type)->result();
			$this->load->view('Admin/v_export_excel_log',$data);
		}
		
		//tahun,divisi,id_user,type
		if($divisi != "" && $id_user != "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_1_log($divisi,$id_user,$type,$tahun)->result();
			$this->load->view('Admin/v_export_excel_log',$data);
		}
	}
	function hapus_data_staff(){
		$no = $this->input->post('no');
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');

		$where = array('no' => $no);
		$where_data = array('id_user' => $id_user ,'nama_project' => $nama_project, 'instansi' => $instansi);
		$this->M_data->hapus_data($where,'t_data_utama');
		$this->M_data->hapus_data($where_data,'t_log');
	}
	function hapus_prog_utama(){
		$no = $this->input->get('no');
		$id_user = $this->input->get('id_user');
		$nama_project = $this->input->get('nama_project');
		$instansi = $this->input->get('instansi');
		$progres = $this->input->get('progres');

		$where = array('no' => $no,'id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi,'progres' => $progres,);
		$this->M_data->hapus_data($where,'t_data_utama');
		redirect('Admin/daily_report_admin');
	}
	function hapus_log(){
		$id_log = $this->input->get('id_log');
		$id_user = $this->input->get('id_user');
		$nama_project = $this->input->get('nama_project');
		$instansi = $this->input->get('instansi');

		$where = array('id_log' => $id_log);
		$this->M_data->hapus_data($where,'t_log');

		$hasil = array('id_user' => $id_user,'instansi' => $instansi,'nama_project' =>$nama_project);
		$t ['prog_detail'] = $this->M_data->prog_detail($hasil,'t_detail')->result();
		$t ['t_data_report'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();
		$t ['t_data_input'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();

		$this->load->view('Admin/v_data_detail_admin',$t);
	}
  //   public function tampil_detail_log(){
  //   	$id_log = $this->input->get('id_log');
  //   	$hasil  = array('id_log' => $id_log);
  //   	$t['data_log'] = $this->M_data->tampil_detail_log($hasil,'t_log')->result();
		// $this->load->view('v_detail_rincian',$t);
  //   }
	function hapus_rincian(){
		$id_detail = $this->input->get('id_detail');
		$id_user = $this->input->get('id_user');
		$nama_project = $this->input->get('nama_project');
		$instansi = $this->input->get('instansi');

		$where = array('id_detail' => $id_detail);
		$this->M_data->hapus_data($where,'t_detail');

		$hasil = array('id_user' => $id_user,'instansi' => $instansi,'nama_project' =>$nama_project);
		$t ['prog_detail'] = $this->M_data->prog_detail($hasil,'t_detail')->result();
		$t ['t_data_report'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();
		$t ['t_data_input'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();

		$this->load->view('Admin/v_data_detail_admin',$t);
	}
	function hapus_pekerjaan($no){
		$where = array('no' => $no);
		$this->M_data->hapus_data($where,'t_data_utama');
		redirect('Admin/monitoring_kerja');
	}
	function hapus_data_department(){
		$id_department = $this->input->post('id_department');
		$where = array('id_department' => $id_department);
		$this->M_data->hapus_data($where,'t_department');
	}
	function hapus_staff(){
		$id_user = $this->input->post('id_user');
		$where = array('id_user' => $id_user);
		$this->M_data->hapus_data($where,'t_data_user');
	}
	function target_perusahaan(){
		$target  = $this->input->post('target');
		$tanggal = $this->input->post('tanggal');
		$nilai_target= str_replace(".","", $target);
		$data = array('nilai' => $nilai_target,'tanggal' => $tanggal);
		$this->M_data->input_data($data,'t_target_perusahaan');
		$data	= $this->M_data->target()->result();
		foreach($data as $hasil){
			echo "<input type='text'class='form-control' value='$hasil->nilai' readonly>";
		}
		redirect('Admin/data_tabel_admin');
	}
	function delete_target(){
		$id_target = $this->input->post('id_target');
		$where = array ('id_target' =>$id_target);
		$this->M_data->hapus_data($where,'t_target_perusahaan');
		redirect('Admin/data_tabel_admin');
	}
	function edit_target(){
		$id_target = $this->input->post($id_target);
		$this->M_data->show_target($id_target);
	}
	function data_tabel_admin(){
		if($this->session->userdata('status') == "admin"){
			$data['t_data_utama'] = $this->M_data->get_data()->result();
			$data['nama_staff']   = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	  = $this->M_data->tampil_dept()->result();
			$data['mail_data']	  = $this->M_data->get_user()->result();
			$data['divisi']		  = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$data['target']	= $this->M_data->target()->result();
			$this->load->view('Admin/v_data_tabel_admin',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function monitoring_kerja(){
		if($this->session->userdata('status') == "admin"){
			$data['t_data_utama'] = $this->M_data->get_data('t_data_utama')->result();
			$this->load->view('Admin/v_tabel_monitoring_admin',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function daily_report_admin(){
		if($this->session->userdata('status') == "admin"){
			$data['t_data_report'] = $this->M_data->tampil_daily_report_admin()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Admin/v_daily_report_admin',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function data_department_admin(){
		if($this->session->userdata('status') == "admin"){
			$data['t_department'] = $this->M_data->tampil_dept()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Admin/v_dept_admin',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function daftar_staff(){
		if($this->session->userdata('status')== "admin"){
			$data['t_staff'] = $this->M_data->tampil_staff()->result();
			$data['nama_staff'] = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept'] = $this->M_data->tampil_dept()->result();
			$data['mail_data'] = $this->M_data->get_data('t_data_user')->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Admin/v_daftar_staf_admin',$data);
		}else{
			redirect(base_url('Login'));
		}
	}
	function laporan_log(){
		if($this->session->userdata('status') == "admin"){
			$data['t_data_utama'] = $this->M_data->get_data()->result();
			$data['nama_staff']   = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	  = $this->M_data->tampil_dept()->result();
			$data['mail_data']	  = $this->M_data->get_user()->result();
			$data['divisi']		  = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$data['target']	= $this->M_data->target()->result();
			$this->load->view('Admin/v_laporan_log',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function laporan_project(){
		if($this->session->userdata('status') == "admin"){
			$data['t_data_utama'] = $this->M_data->get_data()->result();
			$data['nama_staff']   = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	  = $this->M_data->tampil_dept()->result();
			$data['mail_data']	  = $this->M_data->get_user()->result();
			$data['divisi']		  = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$data['target']	= $this->M_data->target()->result();
			$this->load->view('Admin/v_laporan_project',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function tambah_data_utama(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$type = $this->input->post('type');
		$divisi = $this->input->post('divisi');
		$est_pendapatan = $this->input->post('est_pendapatan');
		$nilai_planning= str_replace(".","", $est_pendapatan);

		$tanggal = $this->input->post('tanggal');
		$pic_instansi = $this->input->post('pic_instansi');
		$no_telp = $this->input->post('no_telp');

		$email = $this->input->post('group_mail');
		$data_email = implode(",",$email);

		$rincian_log = "";
		$progres = "0";

		$data = array(
			'id_user' => $id_user,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'type' => $type,
			'divisi' => $divisi,
			'esti_pendapatan' => $nilai_planning,
			'tanggal' => $tanggal,
			'pic_instansi' => $pic_instansi,
			'no_telp' => $no_telp,
			'email_group' => $data_email,
			'progres' => $progres,
			'last_update' => $tanggal,
			'status_project' => "running"
		);
		$data_log = array(
			'id_user' => $id_user,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'rincian_log' => $rincian_log,
			'progress_log' => $progres,
			'type' => $type,
			'divisi' => $divisi,
			'update_log' => $tanggal,
			'email_group' => $data_email
		);

		$this->M_data->input_data($data,'t_data_utama');
		$this->M_data->tambah_log ($data_log,'t_log');
	}
	function tambah_data_report(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$rincian = $this->input->post('rincian');
		$progres = $this->input->post('progres');
		$tanggal_update = $this->input->post('tanggal_update');

		$data = array(
			'id_user' => $id_user,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'rincian' => $rincian,
			'progres' => $progres,
			'tgl_update' => $tanggal_update,
		);
		$log = array('nama_project' => $nama_project,
			'instansi' => $instansi,
			'id_user' => $id_user,
			'rincian_log'  => $rincian,
			'progress_log' => $progres,
			'update_log' => $tanggal_update,);

		$this->M_data->input_data($data,'t_detail');
		$this->M_data->input_data($log,'t_log');

		$hasil = array('id_user' => $id_user,'instansi' => $instansi,'nama_project' =>$nama_project);
		$t ['prog_detail'] = $this->M_data->prog_detail($hasil,'t_detail')->result();
		$t ['t_data_report'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();
		$t ['t_data_input'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();

		$this->load->view('Admin/v_data_detail_admin',$t);
	}
	function update_progres(){
		$id_detail = $this->input->post('id_detail');
		$rincian = $this->input->post('rincian');
		$progres = $this->input->post('progres');
		$tanggal_update = $this->input->post('tgl_update');

		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');

		$data_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi,'rincian_log' => $rincian,'progress_log' => $progres,'update_log' => $tanggal_update);

		$this->M_data->tambah_log ($data_log,'t_log');


		$hasil = array('id_user' => $id_user,'instansi' => $instansi,'nama_project' =>$nama_project);
		$t ['prog_detail'] = $this->M_data->prog_detail($hasil,'t_detail')->result();
		$t ['t_data_report'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();
		$t ['t_data_input'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();

		$this->load->view('Admin/v_data_detail_admin',$t);

		$data = array(
			'id_detail' => $id_detail,
			'rincian' => $rincian,
			'progres' => $progres,
			'tgl_update' => $tanggal_update,
		);

		$where = array('id_detail' => $id_detail);
		$this->M_data->update_data($where,$data,'t_detail');
	}
	function update_prog(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');

		$where 		 = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi);
		$data['edit_prog']	 = $this->M_data->tampil_update_log($id_user,$nama_project,$instansi)->result();
		$data['mail_data']   = $this->M_data->get_user_staff()->result();
		$data['data_mail']   = $this->M_data->get_user()->result(); 
		$data['email_exist'] = explode(',',$this->M_data->exist_email_group($where));
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Admin/v_update_prog_staff',$data);
	}
	function tambah_log_staff(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$rincian_log = $this->input->post('rincian');
		$group = $this->input->post('email_group');
		$data_email = implode(",",$group);
		$status = $this->input->post('status');
		$type 	= $this->input->post('type');
		$divisi = $this->input->post('divisi');

		$progres = $this->input->post('progres');
		$tanggal_update = $this->input->post('tgl_update');

		$this->load->library('upload');
		$files = $_FILES;
		$images = array();
		$cpt = count($_FILES['userfile']['name']);
		
		for($i=0; $i<$cpt; $i++){
			
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];    

			$this->upload->initialize($this->set_upload_options());
			$this->upload->do_upload();

			$images[] = $_FILES['userfile']['name'];
		}

		$fileName = implode(',',$images);
		$newfile_name= preg_replace('/\s+/', '_', $fileName);

		$data_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi,
			'rincian_log' => $rincian_log,'progress_log' => $progres,'file' => $newfile_name,'update_log' => $tanggal_update,'email_group' => $data_email,'type' => $type,'divisi' => $divisi);

		$where_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' =>$instansi);
		
		$data_log_utama = array('progres' =>$progres ,'last_update' =>$tanggal_update,'email_group' =>$data_email,'status_project' =>$status);

		$this->M_data->update_log_utama($where_log,$data_log_utama,'t_data_utama');
		$this->M_data->tambah_log ($data_log,'t_log');

		$email   = $this->session->userdata('email');//$this->M_data->data_user();
		$subject = "Daily Report";

		$mail_data['subject']  = 'Daily Report';
		$mail_data['all_log']  =  $this->M_data->kirim_log($nama_project,$instansi)->result();

		$mail_data['nama_pic'] =  $this->M_data->get_nama_pic($id_user);
		$mail_data['nama_project'] =  $this->input->post('nama_project');
		$mail_data['instansi'] =  $this->input->post('instansi');
		$mail_data['tanggal']  =  $this->input->post('tgl_update');
		$mail_data['description'] = "Update Progres";
		$mail_data['update_by'] = $this->session->userdata('nama');
		$message = $this->load->view('Admin/v_email_update',$mail_data,true);
		$this->sendEmail($email,$subject,$message);
	}
	private function set_upload_options(){   
		$config = array();
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = '*';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		return $config;
	}
	function profil(){
		$this->load->view('Admin/v_profil');
	}
	function upload_profil(){
		$id = $this->session->userdata('id_user');
		$name= "$id";

		$config['upload_path']   = './profile/';
		$config['allowed_types'] = 'jpg';
		$config['max_size']      = '0';
		$config['overwrite'] 	 = TRUE;
		$config['max_filename']	 = '0';
		$config['remove_spaces'] = TRUE;
		$config['file_name']	 = $name;

		$this->load->library('upload', $config);
		$this->upload->do_upload('userfile');

		$data = array('upload_data' => $this->upload->data());
		$image= $data['upload_data']['file_name'];

		$this->load->helper("file");
		unlink(base_url("profile/$image"));

		$where = array('id_user' =>$id);
		$data  = array('picture' =>$image);
		if($this->upload->do_upload('userfile')){
			$this->M_data->update_data($where,$data,'t_data_user');
			$this->session->set_flashdata('success','<strong>Success!</strong>  Profil berhasil diupdate.');
			redirect('Admin/show_profil');
		}else{
			$this->session->set_flashdata('error','<strong>Erorr!</strong>  tidak bisa diupdate.');
			redirect('Admin/show_profil');
		}

	}
	function ganti_pwd(){
		$email = $this->input->post('email');
		$password  = $this->input->post('password_baru');
		$sel_email = $this->M_data->seleksi_email($email);

		if ($sel_email == 0) {
			$this->session->set_flashdata('error','<strong>Erorr!</strong>  Email tidak terdaftar.');
			redirect('Admin/show_profil');
		}else{
			$data = array('password' => $password);
			$where = array('email' =>$email);
			$this->M_data->update_data($where,$data,'t_data_user');
			$this->session->set_flashdata('success','<strong>Success!</strong>  Profil berhasil diupdate.');
			redirect('Admin/show_profil');	
		}
	}
	function tambah_log(){ 
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$rincian_log = $this->input->post('rincian');
		$detail_rincian = $this->input->post('detail_rincian');

		$progres = $this->input->post('progres');
		$tanggal_update = $this->input->post('tgl_update');

		$data = array(
			'progres' => $progres,
			'tanggal_update' => $tanggal_update,
		);

		$data_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi,'rincian_log' => $rincian_log,'detail_rincian' => $detail_rincian,'progress_log' => $progres,'update_log' => $tanggal_update);

		$where = array(	'id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi);

		$this->M_data->update_data($where,$data,'t_data_utama');
		$this->M_data->tambah_log ($data_log,'t_log');

		redirect('Admin/daily_report_staff');
	}
	function update_progres_utama(){
		$rincian_log = $this->post('rincian');
		$detail_rincian = $this->post('detail_rincian');
		$progres = $this->input->post('progres');
		$nama_project = $this->input->post('nama_project');
		$tanggal_update = $this->input->post('tgl_update');
		$instansi = $this->input->post('instansi');
		$id_user = $this->input->post('id_user');

		$data_log = array(
			'rincian_log' => $rincian_log,
			'detail_rincian' => $detail_rincian,
			'progress_log' => $progres,
			'nama_project' => $nama_project,
			'update_log' => $tanggal_update,
			'instansi' => $instansi,
			'id_user' => $id_user,
		);

		$this->M_data->tambah_log($data_log,'t_log');
		redirect('Admin/daily_report_staff');

		// $hasil = array('id_user' => $id_user,'instansi' => $instansi,'nama_project' =>$nama_project);
		// $t ['prog_detail'] = $this->M_data->prog_detail($hasil,'t_detail')->result();
		// $t ['t_data_report'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();
		// $t ['t_data_input'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();

		// $this->load->view('v_data_detail',$t);
	}
	function update_progres_utama_admin(){
		$no = $this->input->post('no');
		$progres = $this->input->post('progres');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$tanggal_update = $this->input->post('tgl_update');
		$id_user = $this->input->post('id_user');

		$data = array(
			'no' => $no,
			'progres' => $progres,
			'tanggal_update' => $tanggal_update,
			'instansi' => $instansi,
			'nama_project' => $nama_project,
			'id_user' => $id_user,
		);

		$data_log = array(
			'prog_log_utama' => $progres,
			'update_log_utama' => $tanggal_update,
			'instansi' => $instansi,
			'nama_project' => $nama_project,
			'id_user' => $id_user,
		);

		$where = array(
			'no' => $no
		);

		$this->M_data->tambah_log($data_log,'t_log');
		$this->M_data->update_data($where,$data,'t_data_utama');

		$hasil = array('id_user' => $id_user,'instansi' => $instansi,'nama_project' =>$nama_project);
		$t ['prog_detail'] = $this->M_data->prog_detail($hasil,'t_detail')->result();
		$t ['t_data_report'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();
		$t ['t_data_input'] = $this->M_data->prog_detail($hasil,'t_data_utama')->result();

		$this->load->view('Admin/v_data_detail_admin',$t);
	}
	function tambah_data_staff(){
		$email = $this->input->post('email');
		$nama_dep = $this->input->post('nama_depan');
		$nama_bel = $this->input->post('nama_belakang');
		$password = $this->acakangkahuruf(7);
		$level = $this->input->post('level');
		
		$data = array(
			'nama_depan' => $nama_dep,
			'nama_belakang' => $nama_bel,
			'password' => $password,
			'level' => $level,
			'email' => $email
		);

		$sel_email = $this->M_data->seleksi_email($email);
		if ($sel_email > 0) {
			echo ("<script LANGUAGE='JavaScript'>
				window.alert('Mohon Maaf Email Sudah Terdaftar');
				window.location.href='daftar_staff';
				</script>");

		}else{
			$this->M_data->input_data($data,'t_data_user');
			$email   = $this->input->post('email');
			$subject ="Login Information";
			$mail_data['email'] = $email;
			$mail_data['nama'] = $nama_dep;
			$mail_data['password'] = $password;
			$message = $this->load->view('Admin/v_email_page_admin',$mail_data,true);
			$this->sendEmail_User($email,$subject,$message);
		}
	}
	function tambah_data_dept(){
		$nama_dept = $this->input->post('nama');
		$data = array('nama_department' => $nama_dept);
		$this->M_data->input_data($data,'t_department');
	}
	function tambah_data(){
		$this->load->view('Admin/v_tambah_data_utama');
	}
	function edit_pekerjaan($no){
		$where = array('no' => $no);
		$data['t_data_utama'] = $this->M_data->edit_data($where,'t_data_utama')->result();
		$this->load->view('Admin/v_edit_pekerjaan',$data);
	}
	function edit_info_staff($no){
		$where = array('no' => $no);
		$data['t_data_utama'] = $this->M_data->edit_data($where,'t_data_utama')->result();
		$this->load->view('Admin/v_edit_info_staff',$data);
	}

	function edit_progres($id_detail){
		$where = array('id_detail' => $id_detail);
		$data['edit_prog'] = $this->M_data->edit_data($where,'t_detail')->result();
		$this->load->view('Admin/v_edit_progres',$data);
	}

	function edit_progres_admin($id_detail){
		$where = array('id_detail' => $id_detail);
		$data['edit_prog'] = $this->M_data->edit_data($where,'t_detail')->result();
		$this->load->view('v_edit_progres_admin',$data);
	}

	function edit_progres_utama_admin(){
		$no = $this->input->get('no');
		$where = array('no' => $no);
		$data['edit_prog'] = $this->M_data->edit_data($where,'t_data_utama')->result();
		$this->load->view('Admin/v_edit_prog_utama_admin',$data);
	}
	function show_comment(){
		$id_log = $this->input->post('id_log');
		$data 	= $this->M_data->show_comment($id_log)->result();
		foreach($data as $h){
			if($h->komentar == ""){
				echo 
				"<div class='alert alert-danger alert-dismissible show' role='alert'>
				Tidak ada Komentar
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
				<span aria-hidden='true'>&times;</span>
				</button>
				</div>";
			}else{
				echo "$h->komentar";	
			}
		}
	}
	function download($filename = NULL){    
		$file = $this->input->get('file');
		$data = array('file' => $file);   
		$this->load->helper('download');
		$data = file_get_contents(base_url('/uploads/'.$filename));
		force_download($filename, $data); 
	}
	function hapus_file(){    
		$file   = $this->input->post('file'); 
		$id_log = $this->input->post('id_log');

		$this->M_data->delete($id_log);
		unlink("/var/www/Magang/uploads/$file"); 
	}
	function detail_project_admin(){
		$id_user = $this->input->post('id_user');
		$instansi = $this->input->post('instan');
		$nama_project = $this->input->post('project');

		$data ['data_log'] = $this->M_data->prog_detail_admin($id_user,$instansi,$nama_project)->result();
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Admin/v_data_detail_admin',$data);
	}
	function tampil_log_admin(){
		$id_user = $this->input->get('id_user');
		$nama_project = $this->input->get('nama_project');
		$instansi = $this->input->get('instansi');

		$where = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi);
		$data ["data_log"] = $this->M_data->log_detail($where,'t_log')->result();
		$this->load->view('Admin/v_detail_log_admin',$data);
	}

	function update_dept(){
		$nama = $this->input->post('nama');
		$id_department = $this->input->post('id_department');

		$data = array('id_department' => $id_department ,'nama_department' => $nama);
		$where = array('id_department' =>$id_department);

		$this->M_data->update_data($where,$data,'t_department');
	}
	function update(){
		$no = $this->input->post('no');
		$kode_project = $this->input->post('kode_project');
		$id_user = $this->input->post('id_user');
		$id_user_last = $this->input->post('id_user_last');
		$nama_project = $this->input->post('nama_project');
		$nama_project_last = $this->input->post('nama_project_last');
		$instansi = $this->input->post('instansi');
		$instansi_last = $this->input->post('instansi_last');
		$type = $this->input->post('type');
		$divisi = $this->input->post('divisi');
		$est_pendapatan = $this->input->post('est_pendapatan');
		$nilai_planning= str_replace(".","", $est_pendapatan);

		$real_pendapatan = $this->input->post('real_pendapatan');
		$nilai_spk= str_replace(".","", $real_pendapatan);

		$pic_instansi = $this->input->post('pic_instansi');
		$no_telp = $this->input->post('no_telp');
		$no_spk = $this->input->post('no_spk');

		$data = array(
			'no' => $no,
			'kode_project' => $kode_project,
			'id_user' => $id_user,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'type' => $type,
			'divisi' => $divisi,
			'esti_pendapatan' => $nilai_planning,
			'real_pendapatan' => $nilai_spk,
			'no_spk' => $no_spk,
			'pic_instansi' => $pic_instansi,
			'no_telp' => $no_telp
		);

		$where = array(
			'no' => $no
		);

		$data_log = array(
			'id_user' => $id_user,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'divisi' => $divisi,
			'type' => $type
		);

		$where_log = array(
			'id_user' => $id_user_last,
			'nama_project' =>$nama_project_last,
			'instansi' =>$instansi_last
		);

		$id['id_log'] = $this->M_data->select_id_log($id_user_last,$nama_project_last,$instansi_last);
		$this->M_data->update_data($id,$data_log,'t_log');
		$this->M_data->update_data($where,$data,'t_data_utama');
	}
	function update_pekerjaan(){
		$no = $this->input->post('no');
		$kode_project = $this->input->post('kode_project');
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$status = $this->input->post('status');
		$no_spk = $this->input->post('no_spk');
		$tgl_spk = $this->input->post('tgl_spk');
		$info = $this->input->post('info');

		$data = array(
			'no' => $no,
			'kode_project' => $kode_project,
			'id_user' => $id_user,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'status' => $status,
			'no_spk' => $no_spk,
			'tgl_spk' => $tgl_spk,
			'info' => $info,
		);

		$where = array(
			'no' => $no
		);

		$this->M_data->update_data($where,$data,'t_data_utama');
		redirect('Admin/monitoring_kerja');
	}
	function update_info_staff(){
		$no = $this->input->post('no');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$info = $this->input->post('info');
		$tanggal = $this->input->post('tanggal');
		$status = $this->input->post('status');
		$no_spk = $this->input->post('no_spk');
		$tgl_spk = $this->input->post('tgl_spk');

		$data = array(
			'no' => $no,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'info' => $info,
			'tanggal' => $tanggal,
			'status' => $status,
			'no_spk' => $no_spk,
			'tgl_spk' => $tgl_spk,
		);

		$where = array(
			'no' => $no
		);

		$this->M_data->update_data($where,$data,'t_data_utama');
		redirect('Admin/data_tabel_staff');
	}
	function update_staff(){
		$id_user = $this->input->post('id_user');
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$level = $this->input->post('level');
		$data = array(
			'id_user' => $id_user,
			'nama_depan' => $nama,
			'email' => $email,
			'level' => $level
		);

		$where = array(
			'id_user' => $id_user
		);

		$this->M_data->update_data($where,$data,'t_data_user');
		redirect('Admin/daftar_staff');
	}
	function type(){
		$data = array('get_category'=> $this->M_data->pilihan_type());
		$this->load->view('Admin/v_tambah_data_utama',$data);
	}
	function data_grafik(){
		$data  = $this->M_data->hasil_grafik();
		$asd->cols[] = array(
			"id" => "",
			"label" => "Topping",
			"pattern" => "",
			"type" => "string"
		);
		$asd->cols[] = array(
			"id" => "",
			"label" => "Total_esti",
			"pattern" => "",
			"type" => "number"
		);

		foreach($data as $cd){
			$asd->rows[]["c"] = array(
				array(
					"v" => "",
					"f" => "Total Estimasi Pendapatan"
				),
				array(
					"v" => (int)$cd->tot_esti,
					"f" => null
				)
			);
		}
		echo json_encode($asd);
	}
	function grafik_d(){
		$hasil_d = $this->M_data->grafik_donut();
		$responce->cols[] = array(
			"id" => "",
			"label" => "Topping",
			"pattern" => "",
			"type" => "string"
		);
		$responce->cols[] = array(
			"id" => "",
			"label" => "Total",
			"pattern" => "",
			"type" => "number"
		);
		foreach($hasil_d as $cd)
		{
			$responce->rows[]["c"] = array(
				array(
					"v" => "$cd->divisi",
					"f" => null
				) ,
				array(
					"v" => (int)$cd->tot_real,
					"f" => null
				)
			);
		}
		echo json_encode($responce);
	}
	function detail_log_admin(){
		$id_log = $this->input->post('id_log');
		$hasil  = array('id_log' => $id_log);
		$t['data_log'] = $this->M_data->tampil_detail_log($hasil,'t_log')->result();
		$this->load->view('Admin/v_detail_log_admin',$t);
	}
	function setemail(){
		$email   ="prastyo050497@gmail.com";
		$subject ="Time Excelindo";
		$message ="Test";
		$this->sendEmail($email,$subject,$message);
	}
	function cf_work_admin(){
		if($this->session->userdata('status') == "admin"){
			$data['t_data_report'] = $this->M_data->tampil_cf_work()->result();
			$data['target']		   = $this->M_data->target()->result();
			$data['nama_staff']    = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	   = $this->M_data->tampil_dept()->result();
			$data['divisi']		   = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Admin/v_cf_work_admin',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function send_notif(){
		$data_report = $this->M_data->tampil_notif()->result();
		foreach ($data_report as $key) {
				$no['no'] = $key->no;
				$email_cc = $this->M_data->email_cc($no);
				$subject = "Project Notification";
				$mail_data['instansi'] =  $key->instansi;
				$mail_data['nama_pic'] =  $key->nama_depan;
				$mail_data['nama_project'] =  $key->nama_project;
				$mail_data['tanggal']  =  $key->last_update;
				$mail_data['description'] = "Segera update pekerjaan!";
				$message = $this->load->view('Admin/v_email_notif',$mail_data,true);


				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://smtp.googlemail.com',
					'smtp_port' => 465,
					'smtp_user' => 'monitoringte2@gmail.com', 
					'smtp_pass' => 'januari18', 
					'mailtype' => 'html',
					'charset' => 'iso-8859-1',
					'wordwrap' => TRUE
				);
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from('monitoringte2@gmail.com');
				$this->email->to('citra.np@excelindo.co.id');
				$this->email->cc($email_cc);
				$this->email->subject($subject);
				$this->email->message($message);

				if($this->email->send()){
					echo 'Email send.';
				}else{
					show_error($this->email->print_debugger());
				}
				//$this->sendEmail_Notif($email,$subject,$message,$email_cc);
		}
	}
	// function sendEmail_Notif($email,$subject,$message,$email_cc){
	// 	$config = Array(
	// 		'protocol' => 'smtp',
	// 		'smtp_host' => 'ssl://smtp.googlemail.com',
	// 		'smtp_port' => 465,
	// 		'smtp_user' => 'dodih1227@gmail.com', 
	// 		'smtp_pass' => 'Madara74', 
	// 		'mailtype' => 'html',
	// 		'charset' => 'iso-8859-1',
	// 		'wordwrap' => TRUE
	// 	);
	// 	$this->load->library('email', $config);
	// 	$this->email->set_newline("\r\n");
	// 	$this->email->from('monitoringte2@gmail.com');
	// 	$this->email->to($email);
	// 	$this->email->cc('dodih1227@gmail.com,eekoprastyoo@gmail.com');
	// 	$this->email->subject($subject);
	// 	$this->email->message($message);

	// 	if($this->email->send()){
	// 		echo 'Email send.';
	// 	}else{
	// 		show_error($this->email->print_debugger());
	// 	}
	// }
	function sendEmail($email,$subject,$message){
		$email_group = $this->M_data->send_progres();
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'monitoringte2@gmail.com', 
			'smtp_pass' => 'januari18', 
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('monitoringte2@gmail.com');
		$this->email->to($email);
		$this->email->cc($email_group);
		$this->email->subject($subject);
		$this->email->message($message);

		if($this->email->send()){
			$this->session->set_flashdata('message','<strong>Success!</strong> data project berhasil diupdate.');
			redirect('Admin/daily_report_admin');
		}else{
			$this->session->set_flashdata('message_error','<strong>Failed!</strong> Email gagal dikirim.');
			redirect('Admin/daily_report_admin');
			// show_error($this->email->print_debugger());
		}
	}

	function sendEmail_User($email,$subject,$message){
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'monitoringte2@gmail.com', 
			'smtp_pass' => 'januari18', 
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('monitoringte2@gmail.com');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($message);

		if($this->email->send()){
			echo 'Email send.';
		}else{
			show_error($this->email->print_debugger());
		}
	}
	function acakangkahuruf($panjang){
		$karakter= '1234567890';
		$string = '';
		for ($i = 0; $i < $panjang; $i++) {
			$pos = rand(0, strlen($karakter)-1);
			$string .= $karakter{$pos};
		}
		return $string;
	}
	function edit(){
		$no = $this->input->post('no');
		$data = $this->M_data->edit_data_utama($no)->result();
		$pic  = $this->M_data->get_data_user()->result();
		$nama_dept = $this->M_data->tampil_dept()->result();

		foreach($data as $u){
			echo "
			<form action='update' method='post' class='update_project'>
			<table width='100%'>
			<tr>
			<td>
			<input type='hidden' name='no' value='$u->no'>
			<input type='hidden' name='id_user_last' value='$u->id_user'>
			<label for='usr'>Nama PIC</label>
			<select name='id_user' class='form-control'>
			<option value='$u->id_user' selected>$u->nama_depan</option>";
			foreach ($pic as $hasil) {
				echo"
				<option value='$hasil->id_user'>$hasil->nama_depan</option>";
			}
			echo"
			</select>
			</td>
			<td>
			<label for='usr'>Kode Project</label>
			<input type='text' name='kode_project' value='$u->kode_project' class='form-control'>
			</td>
			</tr>

			<tr>
			<td>
			<label for='usr'>Nama Project</label>
			<input type='hidden' name='nama_project_last' value='$u->nama_project'>
			<input type='text' name='nama_project' class='form-control' value='$u->nama_project' required='true'>
			</td>
			<td>
			<label for='usr'>Nilai Planning Pekerjaan</label><br>
			<input type='text' class='testInput form-control' name='est_pendapatan' value='$u->esti_pendapatan' required='true'>
			</td>
			</tr>
			<tr>
			<td>
			<label for='usr'>Instansi</label>
			<input type='hidden' name='instansi_last' value='$u->instansi'>
			<input type='text' name='instansi' class='form-control' value='$u->instansi' required='true'>
			</td>
			<td>
			<label for='usr'>Nomor (SPK)</label><br>
			<input type='text' name='no_spk' class='form-control'>
			</td>
			</tr>

			<tr>
			<td>
			<label for='usr'>Type</label>
			<select class='form-control' name='type'>
			<option value='$u->type' selected>$u->type</option>'
			<option value='New'>New</option>
			<option value='Existing'>Maintenance</option>
			<option value='Upgrade'>Upgrade</option>
			<option value='Downgrade'>Downgrade</option>
			</select>
			</td>
			<td>
			<label for='usr'>Nilai (SPK)</label><br>
			<input type='text' class='testInput form-control' name='real_pendapatan' value='$u->real_pendapatan'>
			</td>
			</tr>

			<tr>
			<td>
			<label for='usr'>Divisi</label>
			<select class='form-control' name='divisi'>
			<option value='$u->divisi' selected>$u->divisi</option>'";
			foreach($nama_dept as $row){ 
				echo "<option value='$row->nama_department'>$row->nama_department</option>"; 
			}
			echo"
			</select>
			</td>
			<td>
			<label for='usr'>Pic Instansi</label>
			<input type='text' name='pic_instansi' class='form-control' value='$u->pic_instansi'>
			</td>
			</tr>
			<tr>
			<td>
			<label for='usr'>No Telp</label>
			<input type='text' name='no_telp' class='form-control' value='$u->no_telp'>
			</td>
			<td align='right'>
			<br>
			<button  type='submit' class='btn btn-info btn-sm'>Update
			</button>
			<td>
			</tr>
			</table>
			</form>";
		}
		// echo " <script src='".base_url('assets/docs/js/jquery-3.2.1.min.js')."'></script>";
		//echo " <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
		
		echo " <script src='http://www.decorplanit.com/plugin/autoNumeric-1.9.41.js'></script>";
		echo "
		<script>
		$(document).ready(function() {
			$('.testInput').autoNumeric('init', {
				aSep: '.', 
				aDec: ',',
				aForm: true,
				vMax: '999999999999999',
				vMin: '-9'
				});
				});

				$('.update_project').submit(function(event){
					event.preventDefault(); //prevent default action 
					$('.update').attr('disabled', true);
					var post_url = $(this).attr('action'); //get form action url
					var request_method = $(this).attr('method'); //get form GET/POST method
					var form_data = $(this).serialize(); //Encode form elements for submission
					console.log(form_data);
					if($('form')[0].checkValidity()) {
						$.ajax({
							url : post_url,
							type: request_method,
							data : form_data
							}).done(function(response){
								swal('Success!', 'Data berhasil diupdate', 'success').then(function(){
									window.location = 'data_tabel_admin';
									});
									$('.update').attr('disabled', false);
									});
								}
								});
		</script>";
		}
	function edit_dept(){
		$id_department = $this->input->post('id_department');
		$where = array('id_department' => $id_department);
		$data  = $this->M_data->edit_data($where,'t_department')->result();

		foreach($data as $u){
			echo "
			<form action='update_dept' method='post' class='update'>
			<table width='100%'>
			<tr>
			<td>
			<input type='hidden' name='id_department' value='$u->id_department' class='form-control'>
			<input type='text' name='nama' value='$u->nama_department' class='form-control'>
			</td>
			<td>
			<button type='submit' class='btn btn-info btn-sm'>Update</button>
			</td>
			</tr>
			</table>
			</form>";
		}
		echo"<script src='".base_url('assets/docs/js/jquery-3.2.1.min.js')."'></script>";
		echo"<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
		echo
		"<script>
		$('.update').submit(function(event){
			event.preventDefault(); //prevent default action 
			var post_url = $(this).attr('action'); //get form action url
			var request_method = $(this).attr('method'); //get form GET/POST method
			var form_data = $(this).serialize(); //Encode form elements for submission
			console.log(form_data);
			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
				}).done(function(response){ 
					swal({
						title: 'Success!',
						text: 'Data berhasil diupdate',
						icon: 'success',
						button: 'Ok',
						}).then(function(){
							window.location = 'data_department_admin';
							});
							}).fail(function(){
								alert('error');
								}).always(function(){
									$('#submit').val('submit');
									});
									});
									</script>";
								}
	function edit_staff(){
		$id_user 		= $this->input->post('id_user');
		$where 			= array('id_user' => $id_user);
		$t_data_staff   = $this->M_data->edit_data($where,'t_data_user')->result();

		foreach($t_data_staff as $u){ 
			echo "
			<form action='update_staff' method='post' class='update'>
			<input type='hidden' name='id_user' value='$u->id_user' 
			<table>
			<tr>
			<td>
			<label for='usr'>Nama</label>
			<input type='text' name='nama' value='$u->nama_depan' class='form-control'>
			<td>
			</tr>

			<tr>
			<td>
			<label for='usr'>Email</label>
			<input type='text' name='email' value='$u->email' class='form-control'>
			</td>
			</tr>

			<tr>
			<td>
			<label for='usr'>Level</label>
			<select class='form-control' name='level'>
			<option value='$u->level' selected>$u->level</option>';
			<option value='admin'>admin</option>
			<option value='staff'>staff</option>
			<option value='manager'>manager</option>
			</select>
			</td>
			</tr>

			<tr>
			<td align='center'>
			<button type='submit' class='btn btn-info btn-sm'>Update</button>
			</td>
			</tr>
			</table>
			</form>";
		}

		echo "
		<script>
		$('.update').submit(function(event){
			$('#LoadingImage').show();  
			event.preventDefault(); //prevent default action
			$('.update').attr('disabled', true); 
			var post_url = $(this).attr('action'); //get form action url
			var request_method = $(this).attr('method'); //get form GET/POST method
			var form_data = $(this).serialize(); //Encode form elements for submission
			console.log(form_data);
			if($('form')[0].checkValidity()) {
				$.ajax({
					url : post_url,
					type: request_method,
					data : form_data
					}).done(function(response){
						$('#LoadingImage').hide(); 
						swal('Success!', 'Data berhasil diinputkan', 'success').then(function(){
							window.location = 'daftar_staff';
							});
							$('.update').attr('disabled', true); 
							});
						}
						});
						</script>"; 
					}


	function show_file(){
		$id_log = $this->input->post('id_log');
		$data   = $this->M_data->show_file($id_log);
		$list_koma = explode(",",$data);
		foreach($list_koma as $h){
			echo "<table style='width:100%;'>
			<tr>
			<td style='width:50%;'>
			<b>$h</b>	
			</td>
			<td style='width:30%;'>";
			echo "
			<table>
			<tr>
			<td>";
			echo "<form>";
			echo "<a href='".base_url("/Admin/download/$h")."'>";
			echo "<i class='btn btn-info btn-sm'>Download</i></a>
			</form>
			</td>
			<td>
			<form action='hapus_file' method='post' class='delete_file'>
			<input type='hidden' name='file' value='$h'>
			<input type='hidden' name='id_log' value='$id_log'>
			<input type='submit' class='btn btn-danger btn-sm' value='Delete'>
			</form>
			</td>
			</tr>
			</table>
			</td>
			</tr>
			</table>";
			echo "
			<script src='".base_url("assets/docs/js/jquery-3.2.1.min.js")."'></script>
			<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>

			<script>
			$('.delete_file').submit(function(event){
				event.preventDefault(); //prevent default action 
				var post_url = $(this).attr('action'); //get form action url
				var request_method = $(this).attr('method'); //get form GET/POST method
				var form_data = $(this).serialize(); //Encode form elements for submission
				console.log(form_data);
				swal({
					title: 'Are you sure?',
					text: 'Once deleted, you will not be able to recover this row file!',
					icon: 'warning',
					buttons: true,
					dangerMode: true
					})
					.then((response) => {
						if (response) {
							$.ajax({
								url : post_url,
								type: request_method,
								data : form_data
								}).done(function(response){
									swal('Poof! your project has been deleted!',{
										icon: 'success'
										})
										});
										}else {
											swal('Your your project is safe!').then(function(){
												});
											}
											});
											});
											</script>";
										}
									}
									
}