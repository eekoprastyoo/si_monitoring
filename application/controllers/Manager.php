<?php
class Manager extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		$this->load->helper('string');	
	}
	function coba(){
		$this->load->view('Manager/v_email_page_manager');
	}
	function manager(){
		if($this->session->userdata('status') == "manager"){
			$tahun = $this->input->post('tahun');
			$data['hasil'] = $this->M_data->grafik($tahun);
			$data['hasil_tabel'] = $this->M_data->tabel_grafik($tahun);
			$data['data'] = $this->M_data->grafik_pie($tahun);
			$data['data_total'] = $this->M_data->total_perusahaan($tahun);
			$data['hasil_estimasi'] = $this->M_data->jumlah_estimasi($tahun);
			$data['hasil_target'] = $this->M_data->jumlah_target($tahun);
			$data['hasil_real'] = $this->M_data->jumlah_real($tahun);
			$data['jml_new'] = $this->M_data->jumlah_new($tahun);
			$data['jml_exist'] = $this->M_data->jumlah_exist($tahun);
			$data['jml_up'] = $this->M_data->jumlah_up($tahun);
			$data['jml_down'] = $this->M_data->jumlah_down($tahun);
			$data['h_pie'] = $this->M_data->grafik_donut($tahun);
			$data['search'] = $tahun;
			$data['profil'] = $this->M_data->show_profil()->result();
			$data['detail_spk'] = $this->M_data->detail_spk()->result();
 			$this->load->view('Manager/v_hal_utama_manager',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function tambah_data(){
		if($this->session->userdata('status') == "manager"){
			$id_user = $this->session->userdata('id_user');
			$data['t_data_utama']	= $this->M_data->get_data('t_data_utama')->result();
			$data['nama_staff'] 	= $this->M_data->nama_staff_khusus()->result();
			$data['nama_dept']		= $this->M_data->tampil_dept()->result();
			$data['mail_data']		= $this->M_data->add_data_email()->result();
			$data['profil'] 		= $this->M_data->show_profil()->result();
			$this->load->view('Manager/v_add_data_manager',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function tambah_data_utama(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$type = $this->input->post('type');
		$divisi = $this->input->post('divisi');

		$est_pendapatan = $this->input->post('est_pendapatan');
		$nilai_planning= str_replace(".","", $est_pendapatan);

		$tanggal = $this->input->post('tanggal');
		$pic_instansi = $this->input->post('pic_instansi');
		$no_telp = $this->input->post('no_telp');

		$email = $this->input->post('group_mail');
		$data_email = implode(",",$email);

		$rincian_log = "";
		$progres = "0";

		$data = array(
			'id_user' => $id_user,
			'nama_project' => $nama_project,
			'instansi' => $instansi,
			'type' => $type,
			'divisi' => $divisi,
			'esti_pendapatan' => $nilai_planning,
			'tanggal' => $tanggal,
			'pic_instansi' => $pic_instansi,
			'no_telp' => $no_telp,
			'email_group' => $data_email,
			'progres' => $progres,
			'last_update' => $tanggal,
			'status_project' => "running"
		);
		$data_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi,'rincian_log' => $rincian_log,'progress_log' => $progres,'update_log' => $tanggal,'email_group' => $data_email);

		$this->M_data->input_data($data,'t_data_utama');
		$this->M_data->tambah_log ($data_log,'t_log');
	}
	function detail_log_manager(){
		$id_log = $this->input->get('id_log');
		$hasil  = array('id_log' => $id_log);
		$t['data_log'] = $this->M_data->tampil_detail_log($hasil,'t_log')->result();
		$this->load->view('Manager/v_detail_log_manager',$t);
	}
	function daily_report_manager(){
		if($this->session->userdata('status') == "manager"){
			$data['t_data_report'] = $this->M_data->tampil_daily_report()->result();
			$data['target']		   = $this->M_data->target()->result();
			$data['nama_staff']    = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	   = $this->M_data->tampil_dept()->result();
			$data['divisi']		   = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Manager/v_daily_report_manager',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function cf_work(){
		if($this->session->userdata('status') == "manager"){
			$data['t_data_report'] = $this->M_data->tampil_cf_work()->result();
			$data['target']		   = $this->M_data->target()->result();
			$data['nama_staff']    = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	   = $this->M_data->tampil_dept()->result();
			$data['divisi']		   = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Manager/v_cf_work',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function report_manager(){
		$id_login = $this->session->userdata('id_user');
		if($this->session->userdata('id_user') == "$id_login" && $this->session->userdata('status') == "manager"){
			$data['t_data_report'] = $this->M_data->tampil_data_report_manager()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Manager/v_report_manager',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function tambah_log_manager(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');
		$rincian_log = $this->input->post('rincian');
		$group  = $this->input->post('email_group');
		$status = $this->input->post('status');
		$data_email = implode(",",$group);
		$type = $this->input->post('type');
		$divisi = $this->input->post('divisi');

		$progres = $this->input->post('progres');
		$tanggal_update = $this->input->post('tgl_update');

		$this->load->library('upload');
		$files = $_FILES;
		$images = array();
		$cpt = count($_FILES['userfile']['name']);
		
		for($i=0; $i<$cpt; $i++){
			
			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];    

			$this->upload->initialize($this->set_upload_options());
			$this->upload->do_upload();

			$images[] = $_FILES['userfile']['name'];
		}
		$fileName = implode(',',$images);
		$newfile_name= preg_replace('/\s+/','_', $fileName);

		$data_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi,
			'rincian_log' => $rincian_log,'progress_log' => $progres,'file' => $newfile_name,
			'update_log' => $tanggal_update,'email_group' => $data_email,'type' => $type,'divisi' => $divisi);

		$where_log = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' =>$instansi);

		$data_log_utama = array('progres' =>$progres ,'last_update' =>$tanggal_update,'email_group' =>$data_email,'status_project' =>$status);

		foreach ($group as $key) {
			$data['email'] = $key;
			$cek = $this->M_data->cek_level($data);
			$array = array();
			if($cek != $array){
				foreach ($cek as $key) {
					$id = $key;
					foreach ($id as $hasil) {
					$d_hasil['id_user'] = $hasil;
					$data_update = array('used_by' => $this->session->userdata('email'));
					$this->M_data->update_data($d_hasil,$data_update,'t_data_user');
					}
				}
			}else{
				$where = array('used_by' =>$this->session->userdata('email'));
				$data_update = array('used_by' => '');
				$this->M_data->update_data($where,$data_update,'t_data_user');
			}
		}

		$this->M_data->update_log_utama($where_log,$data_log_utama,'t_data_utama');
		$this->M_data->tambah_log ($data_log,'t_log');

		$email   = $this->M_data->data_user();
		$subject = "Daily Report";

		$mail_data['subject']  = 'Daily Report';
		$mail_data['all_log']  =  $this->M_data->kirim_log($nama_project,$instansi)->result();
		$mail_data['nama_pic'] =  $this->M_data->get_nama_pic($id_user);
		$mail_data['nama_project'] =  $this->input->post('nama_project');
		$mail_data['instansi'] =  $this->input->post('instansi');
		$mail_data['tanggal']  =  $this->input->post('tgl_update');
		$mail_data['description'] = "Update Progres";
		$mail_data['update_by'] = $this->session->userdata('nama');
		$message = $this->load->view('Staff/v_email_page',$mail_data,true);
		//$this->sendProgres($email,$subject,$message);
		$this->session->set_flashdata('success','<strong>Success!</strong>  Project berhasil diupdate.');
		redirect('Manager/report_manager');
	}
	private function set_upload_options(){   
		$config = array();
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = '*';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		return $config;
	}
	function update_progres_manager(){
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('nama_project');
		$instansi = $this->input->post('instansi');

		$where = array('id_user' => $id_user,'nama_project' => $nama_project,'instansi' => $instansi);			
		$data['edit_prog']   = $this->M_data->tampil_update_log($id_user,$nama_project,$instansi)->result();
		$data['mail_data']   = $this->M_data->get_user_staff_lain()->result();
		$data['data_mail']   = $this->M_data->get_user()->result(); 
		$data['email_exist'] = explode(',',$this->M_data->exist_email_group($where));
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Manager/v_update_prog_manager',$data);
	}
	function delete_target_staff(){
		$id_target = $this->input->post('id_target');
		$where = array ('id_target' =>$id_target);
		$this->M_data->hapus_data($where,'t_target_perusahaan');
		redirect('Manager/t_staff');
	}
	function delete_target_perusahaan(){
		$id_total = $this->input->post('id_total');
		$where = array ('id_total' =>$id_total);
		$this->M_data->hapus_data($where,'t_total_target');
		redirect('Manager/t_perusahaan');
	}
	function export_excel(){
		$tahun 	  = $this->input->post('tahun');
		$divisi   = $this->input->post('divisi');
		$id_user  = $this->input->post('pic');
		$type     = $this->input->post('type');

		//Tahun Only
		if($tahun != "" && $divisi == "" && $id_user == "" && $type == ""){
			$data['laporan'] = $this->M_data->export_all($tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
		//tahun,divisi
		if($divisi != "" && $tahun != "" && $id_user == "" && $type == ""){
			$data['laporan'] = $this->M_data->export_4($divisi,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
		//tahun,id_user
		if($divisi == "" && $tahun != "" && $id_user != "" && $type == ""){
			$data['laporan'] = $this->M_data->export_7($tahun,$id_user)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
		//tahun,type
		if($divisi == "" && $id_user == "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_6($type,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}

		//tahun,divisi,id_user,
		if($divisi != "" && $id_user != "" && $type == "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_5($id_user,$divisi,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}

		//tahun,divisi,type
		if($divisi != "" && $id_user == "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_2($divisi,$type,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}

		//tahun,id_user,type
		if($divisi == "" && $type != "" && $id_user != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_3($id_user,$type,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
		
		//tahun,divisi,id_user,type
		if($divisi != "" && $id_user != "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_1($divisi,$id_user,$type,$tahun)->result();
			$this->load->view('Manager/v_export_excel',$data);
		}
	}
	function export_excel_log(){
		$tahun 	  = $this->input->post('tahun_log');
		$divisi   = $this->input->post('divisi_log');
		$id_user  = $this->input->post('pic_log');
		$type     = $this->input->post('type_log');

		//Tahun Only
		if($tahun != "" && $divisi == "" && $id_user == "" && $type == ""){
			$data['laporan'] = $this->M_data->export_2_log($tahun)->result();
			$this->load->view('Manager/v_export_excel_log',$data);
		}
		//tahun,divisi
		if($divisi != "" && $tahun != "" && $id_user == "" && $type == ""){
			$data['laporan'] = $this->M_data->export_3_log($tahun,$divisi)->result();
			$this->load->view('Manager/v_export_excel_log',$data);
		}
		//tahun,id_user
		if($divisi == "" && $tahun != "" && $id_user != "" && $type == ""){
			$data['laporan'] = $this->M_data->export_4_log($tahun,$id_user)->result();
			$this->load->view('Manager/v_export_excel_log',$data);
		}
		//tahun,type
		if($divisi == "" && $id_user == "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_5_log($tahun,$type)->result();
			$this->load->view('Manager/v_export_excel_log',$data);
		}

		//tahun,divisi,id_user,
		if($divisi != "" && $id_user != "" && $type == "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_6_log($tahun,$divisi,$id_user)->result();
			$this->load->view('Manager/v_export_excel_log',$data);
		}

		//tahun,divisi,type
		if($divisi != "" && $id_user == "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_7_log($tahun,$divisi,$type)->result();
			$this->load->view('Manager/v_export_excel_log',$data);
		}

		//tahun,id_user,type
		if($divisi == "" && $type != "" && $id_user != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_8_log($tahun,$id_user,$type)->result();
			$this->load->view('Manager/v_export_excel_log',$data);
		}
		
		//tahun,divisi,id_user,type
		if($divisi != "" && $id_user != "" && $type != "" && $tahun != ""){
			$data['laporan'] = $this->M_data->export_1_log($divisi,$id_user,$type,$tahun)->result();
			$this->load->view('Manager/v_export_excel_log',$data);
		}
	}
	function upload_profil(){
		$id = $this->session->userdata('id_user');
		$name= "$id";

		$config['upload_path']   = './profile/';
		$config['allowed_types'] = 'jpg|jpeg';
		$config['max_size']      = '0';
		$config['overwrite'] 	 = TRUE;
		$config['max_filename']	 = '0';
		$config['remove_spaces'] = TRUE;
		$config['file_name']	 = $name;

		$this->load->library('upload', $config);
		$this->upload->do_upload('userfile');

		$data = array('upload_data' => $this->upload->data());
		$image= $data['upload_data']['file_name'];

		$this->load->helper("file");
		unlink(base_url("profile/$image"));

		$where = array('id_user' =>$id);
		$data  = array('picture' =>$image);
		if($this->upload->do_upload('userfile')){
			$this->M_data->update_data($where,$data,'t_data_user');
			$this->session->set_flashdata('success','<strong>Success!</strong>  Profil berhasil diupdate.');
			redirect('Manager/show_profil');
		}else{
			$this->session->set_flashdata('error','<strong>Erorr!</strong>  tidak bisa diupdate.');
			redirect('Manager/show_profil');
		}

	}
	function show_file(){
		$id_log = $this->input->post('id_log');
		$data   = $this->M_data->show_file($id_log);
		$list_koma = explode(",",$data);

		foreach($list_koma as $h){
			echo "<table>
			<tr>
			<td>
			$h
			</td>
			<td align='center'>";
			echo "<a href='".base_url("/Manager/download/$h")."'>";
			echo "<i>Download</i></a>";
			echo "</td>
			</tr>
			</table>";
		}
	}
	function download($filename = NULL){    
		$file = $this->input->get('file');
		$data = array('file' => $file);   
		$this->load->helper('download');
		$data = file_get_contents(base_url('/uploads/'.$filename));
		force_download($filename, $data); 
	}
	function ganti_pwd(){
		$email = $this->input->post('email');
		$password  = $this->input->post('password_baru');
		$sel_email = $this->M_data->seleksi_email($email);

		if ($sel_email == 0) {
			$this->session->set_flashdata('error','<strong>Erorr!</strong>  Email tidak terdaftar.');
			redirect('Admin/show_profil');
		}else{
			$data = array('password' => $password);
			$where = array('email' =>$email);
			$this->M_data->update_data($where,$data,'t_data_user');
			$this->session->set_flashdata('success','<strong>Success!</strong>  Profil berhasil diupdate.');
			redirect('Manager/show_profil');	
		}
	}
	function detail_project_manager(){
		$instansi = $this->input->post('instan');
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('project');

		$where = array('id_user' => $id_user,'instansi' => $instansi,'nama_project' =>$nama_project);
		$data ['data_log'] = $this->M_data->prog_detail_admin($id_user,$instansi,$nama_project)->result();
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Manager/v_data_detail_manager',$data);
	}
	function target_staff(){
		$id_user = $this->input->post('id_user');
		$target  = $this->input->post('target');
		$tanggal = $this->input->post('tanggal');
		$nilai_target= str_replace(".","", $target);
		$data = array('id_user' => $id_user,'nilai' => $nilai_target,'tanggal' => $tanggal);
		$this->M_data->input_data($data,'t_target_perusahaan');
		$data	= $this->M_data->target()->result();
		foreach($data as $hasil){
			echo "<input type='text'class='form-control' value='$hasil->nilai' readonly>";
		}
		redirect('Manager/t_staff');
	}
	function target_perusahaan(){
		$target  = $this->input->post('target');
		$tanggal = $this->input->post('tanggal');
		$nilai_target= str_replace(".","", $target);
		$data = array('nilai' => $nilai_target,'tanggal' => $tanggal);
		$this->M_data->input_data($data,'t_total_target');
		$data	= $this->M_data->target()->result();
		foreach($data as $hasil){
			echo "<input type='text'class='form-control' value='$hasil->nilai' readonly>";
		}
		redirect('Manager/t_perusahaan');
	}
	function tambah_komentar(){
		$id_log = $this->input->post('id_log');
		$komentar = $this->input->post('komentar');

		$data = array('komentar' => $komentar);

		$where = array('id_log' => $id_log);
		$this->M_data->input_data_komen($where,'t_log',$data);

		$data_coment = $this->M_data->show_comment($id_log);

		$email   = $this->session->userdata('email');//$this->M_data->data_user();
		$email_group = $this->M_data->email_komentar($where,'t_log');

		$subject = "Manager Comment";
		$mail_data['all_log']   =  $this->M_data->send_komentar($id_log)->result();
		$mail_data['update_by'] = $this->session->userdata('nama');
		$mail_data['tanggal']   = date('d/m/Y H:m:s');
		$message = $this->load->view('Manager/v_email_page_manager',$mail_data,true);
		$this->sendEmail($email,$email_group,$subject,$message);
	}
	function sendEmail($email,$email_group,$subject,$message){
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'monitoringte2@gmail.com', 
			'smtp_pass' => 'januari18', 
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('monitoringte2@gmail.com');
		$this->email->to($email);
		$this->email->cc($email_group);
		$this->email->subject($subject);
		$this->email->message($message);
	}
	function sendProgres($email,$subject,$message){
		$email_group = $this->M_data->send_progres();
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'monitoringte2@gmail.com', 
			'smtp_pass' => 'januari18', 
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('monitoringte2@gmail.com');
		$this->email->to($email);
		$this->email->cc($email_group);
		$this->email->subject($subject);
		$this->email->message($message);
		   
		if($this->email->send()){
			echo 'Email send.';
		}else{
			show_error($this->email->print_debugger());
		}
	}
	function show_pic(){
		$divisi = $this->input->post('divisi');
		echo $this->M_data->hasil_pic($divisi);
	}
	function show_type(){
		$id_user= $this->input->post('pic');
		echo $this->M_data->hasil_type($id_user);
	}
	function show_type_log(){
		$id_user= $this->input->post('pic_log');
		echo $this->M_data->hasil_type($id_user);
	}
	function show_type_divisi_log(){
		$divisi	= $this->input->post('divisi_log');
		echo $this->M_data->hasil_type_divisi($divisi);
	}
	function show_divisi(){
		$tahun = $this->input->post('tahun');
		echo $this->M_data->hasil_divisi($tahun);
	}
	function show_divisi_pic_log(){
		$id_user = $this->input->post('pic_log');
		echo $this->M_data->hasil_divisi_pic($id_user);
	}
	function show_divisi_log(){
		$tahun = $this->input->post('tahun_log');
		echo $this->M_data->hasil_divisi($tahun);
	}
	function show_type_tahun(){
		$tahun	= $this->input->post('tahun');
		echo $this->M_data->hasil_type_tahun($tahun);
	}
	function show_type_tahun_log(){
		$tahun	= $this->input->post('tahun_log');
		echo $this->M_data->hasil_type_tahun($tahun);
	}
	function show_pic_tahun(){
		$tahun = $this->input->post('tahun');
		echo $this->M_data->hasil_pic_tahun($tahun);
	}
	function show_pic_log(){
		$divisi = $this->input->post('divisi_log');
		echo $this->M_data->hasil_pic($divisi);
	}
	function show_pic_tahun_log(){
		$tahun = $this->input->post('tahun_log');
		echo $this->M_data->hasil_pic_tahun($tahun);
	}
	function show_type_divisi(){
		$divisi	= $this->input->post('divisi');
		echo $this->M_data->hasil_type_divisi($divisi);
	}
	function show_profil(){
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Manager/v_profil',$data);
	}
	function t_perusahaan(){
		if($this->session->userdata('status') == "manager"){
			$data['nama_staff']    = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	   = $this->M_data->tampil_dept()->result();
			$data['divisi']		   = $this->M_data->divisi()->result();
			$data['profil']		   = $this->M_data->show_profil()->result();
			$data['target_total']  = $this->M_data->target_total()->result();
			$this->load->view('Manager/v_target_perusahaan',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function t_staff(){
		if($this->session->userdata('status') == "manager"){
			$data['target']		   = $this->M_data->target()->result();
			$data['nama_staff']    = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	   = $this->M_data->tampil_dept()->result();
			$data['divisi']		   = $this->M_data->divisi()->result();
			$data['profil']		   = $this->M_data->show_profil()->result();
			$this->load->view('Manager/v_target_staff',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function laporan_log (){
		if($this->session->userdata('status') == "manager"){
			$data['t_data_report'] = $this->M_data->tampil_daily_report()->result();
			$data['target']		   = $this->M_data->target()->result();
			$data['nama_staff']    = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	   = $this->M_data->tampil_dept()->result();
			$data['divisi']		   = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Manager/v_export_log',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function laporan_project (){
		if($this->session->userdata('status') == "manager"){
			$data['t_data_report'] = $this->M_data->tampil_daily_report()->result();
			$data['target']		   = $this->M_data->target()->result();
			$data['nama_staff']    = $this->M_data->tampil_nama_staff()->result();
			$data['nama_dept']	   = $this->M_data->tampil_dept()->result();
			$data['divisi']		   = $this->M_data->divisi()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Manager/v_export_project',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function edit_target(){
		$id_target	= $this->input->post('id_target');
		$target 	= $this->input->post('target');
		$tanggal 	= $this->input->post('tanggal');
		$nilai_target= str_replace(".","", $target);
		$where 		 = array('id_target' =>$id_target);
		$data_update = array('nilai' =>$nilai_target ,'tanggal' =>$tanggal );

		$this->M_data->update_data($where,$data_update,'t_target_perusahaan');
	}
	function update_target_perusahaan(){
		$id_total	= $this->input->post('id_total');
		$target 	= $this->input->post('target');
		$tanggal 	= $this->input->post('tanggal');
		$nilai_target= str_replace(".","", $target);
		$where 		 = array('id_total' =>$id_total);
		$data_update = array('nilai' =>$nilai_target ,'tanggal' =>$tanggal );

		$this->M_data->update_data($where,$data_update,'t_total_target');
	}
	function show_detail(){
		$id_log = $this->input->post('id_log');
		$data   = $this->M_data->show_detail($id_log)->result();

		foreach($data as $h){
			echo  "<b>Rincian:</b>";
			echo  "<br>";
			if($h->rincian_log == ""){
				echo  "<div class='alert alert-success'>Tidak ada Rincian</div>";
			}else{
				echo  "<div class='alert alert-success'>$h->rincian_log</div>";
			}
			echo  "<br>";
			echo  "<b>Komentar :</b>";
			echo  "<br>";
			if($h->komentar == ""){
				echo  "<div class='alert alert-info'>Tidak ada Komentar</div>";
			}else{
				echo  "<div class='alert alert-info'>$h->komentar</div>";
			}
			echo  "<br>";
			echo  
			"<form action='' method='POST' class='personal-info' class='form-group'>
			<div class='form-group'>
			<input type='hidden' name='id_log' value='$id_log' id='id_log'>
			<div class='col-sm-15'>
			<script src=".base_url('assets/docs/tinymce/js/tinymce.min.js')."></script>
			<script>tinymce.init({ selector:'textarea' });</script>
			<textarea name='komentar' class='form-control' id='komentar'>$h->komentar</textarea>
			</div>
			</div>
			<button id='submit-p' class='form-control btn btn-info btn-sm' type='submit'><i class='fab fa-telegram-plane fa-2x'></i></button>
			</form>";
		}

		echo "<script src=".base_url('assets/docs/js/jquery-3.2.1.min.js')."></script>";
		echo "<script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>";
		echo "<script>
		$(document).ready(function(){
			$('.personal-info').submit(function(e){
				e.preventDefault();
				var id_log = $('#id_log').val();
				var komentar= $('#komentar').val();
				$.ajax({
					type:'POST',
					url: '".base_url()."/Manager/tambah_komentar',
					data: {id_log:id_log,komentar:komentar},
					success:function(data)
					{
						swal({
							title: 'Success',
							text:  'Komentar Berhasil Dikirim',
							icon: 'success',
							button: 'Ok',
							});
							},
							error:function()
							{
								alert('fail');
							}
							});
							});
						});";
		}

}
