<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('M_login');
		$this->load->model('M_data');
		$this->load->helper('form');
		$this->session->keep_flashdata('message');
	}
	public function index(){
		$this->load->view('v_login');
	}
	function aksi_login(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
	
		$level1 = "admin";
		$level2 = "staff";
		$level3 = "manager";
		$level4 = "direktur";
		$level5 = 'staff_lain';

		if($where = array(
			'email' => $email,
			'password' => $password,
			'level' => $level1
			)){

		$cek = $this->M_login->cek_login("t_data_user",$where)->num_rows();

		if($cek > 0){
				$id_user_login = $this->M_login->id_user_login($email);
				$nama_login = $this->M_login->nama_login($email);
				$data_session = array(
					'id_user' => $id_user_login,
					'password' => $password,
					'status' => "admin",
					'nama' => $nama_login,
					'email' => $email
					);
				$this->session->set_userdata($data_session);
				redirect(base_url("Admin/admin"));
		}
		else if($where = array(
			'email' => $email,
			'password' => $password,
			'level' => $level2
			)){

		$cek = $this->M_login->cek_login("t_data_user",$where)->num_rows();

		if($cek > 0){
				$id_user_login = $this->M_login->id_user_login($email);
				$nama_login = $this->M_login->nama_login($email);
				$data_session = array(
					'id_user' => $id_user_login,
					'password' => $password,
					'nama' => $nama_login,
					'status' => "staff", 
					'email' => $email
					);
				$this->session->set_userdata($data_session);
				redirect(base_url("Staff/staff"));
		}
		else if($where = array(
			'email' => $email,
			'password' => $password,
			'level' => $level3
			)){

		$cek = $this->M_login->cek_login("t_data_user",$where)->num_rows();

		if($cek > 0){
				$id_user_login = $this->M_login->id_user_login($email);
				$nama_login = $this->M_login->nama_login($email);
				$data_session = array(
					'id_user' => $id_user_login,
					'password' => $password,
					'nama' => $nama_login,
					'status' => "manager",
					'email' => $email
					);
	 
				$this->session->set_userdata($data_session);
				redirect(base_url("Manager/manager"));
		}
		else if($where = array(
			'email' => $email,
			'password' => $password,
			'level' => $level4
			)){

		$cek = $this->M_login->cek_login("t_data_user",$where)->num_rows();

		if($cek > 0){
				$id_user_login = $this->M_login->id_user_login($email);
				$nama_login = $this->M_login->nama_login($email);
				$data_session = array(
					'id_user' => $id_user_login,
					'password' => $password,
					'nama' => $nama_login,
					'status' => "direktur",
					'email' => $email
					);
	 
				$this->session->set_userdata($data_session);
				redirect(base_url("Direktur/direktur"));
		}
		else if($where = array(
			'email' => $email,
			'password' => $password,
			'level' => $level5
			)){

		$cek = $this->M_login->cek_login("t_data_user",$where)->num_rows();

		if($cek > 0){
				$id_user_login = $this->M_login->id_user_login($email);
				$nama_login = $this->M_login->nama_login($email);
				$data_session = array(
					'id_user' => $id_user_login,
					'password' => $password,
					'nama' => $nama_login,
					'status' => "staff_lain",
					'email' => $email
					);
	 
				$this->session->set_userdata($data_session);
				redirect(base_url("Staff_Lain/staff_lain"));
		}
		else{
			$this->session->set_flashdata('message','<center><strong>Failed!</strong>  Email atau Password Salah.</center>');
			redirect('Login');
		}
		}
		}
		}
		}
		}
		
	}
	function cek_level(){
		$data['data_'] = $this->M_data->lihat_data()->result();
		$this->load->view('v_tampil',$data);
	}
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('Login'));
	}
	function forgot_password(){
		$this->load->view('v_lupa_password');
	}
	function send_password(){
		$email = $this->input->post('email');
		$where = array('email' => $email);

		$cek   = $this->M_login->cek_login("t_data_user",$where)->num_rows();

		if($cek > 0){
			$mail_data['password'] = $this->M_data->send_password($where,'t_data_user');
			$subject ="Your Password";
			$message = $this->load->view('v_send_password',$mail_data,true);
			$this->sendEmail($email,$subject,$message);
			$this->session->set_flashdata('message','<strong>Success!</strong> password berhasil dikirim.');
			redirect('Login');
		}else{
			$this->session->set_flashdata('message_error','<strong>Error!</strong> Mohon maaf email tidak terdaftar.');
			redirect('Login');
		}
	}
	function sendEmail($email,$subject,$message){
		    $config = Array(
		      'protocol' => 'smtp',
		      'smtp_host' => 'ssl://smtp.googlemail.com',
		      'smtp_port' => 465,
		      'smtp_user' => 'monitoringte2@gmail.com', 
		      'smtp_pass' => 'januari18', 
		      'mailtype' => 'html',
		      'charset' => 'iso-8859-1',
		      'wordwrap' => TRUE
		    );
		       $this->load->library('email', $config);
		       $this->email->set_newline("\r\n");
		       $this->email->from('monitoringte2@gmail.com');
		       $this->email->to($email);
		       $this->email->subject($subject);
		       $this->email->message($message);		  
		       if($this->email->send()){
		          echo 'Email send.';
		          redirect('Login');
		         }else{
		         show_error($this->email->print_debugger());
		}
	}
}
