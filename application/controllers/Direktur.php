<?php
class Direktur extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('M_data');
		$this->load->helper('string');	
	}
	function direktur(){
		if($this->session->userdata('status') == "direktur"){
			$tahun = $this->input->post('tahun');
			$data['hasil'] = $this->M_data->grafik($tahun);
			$data['hasil_tabel'] = $this->M_data->tabel_grafik($tahun);
			$data['data'] = $this->M_data->grafik_pie($tahun);
			$data['data_total'] = $this->M_data->total_perusahaan($tahun);
			$data['hasil_estimasi'] = $this->M_data->jumlah_estimasi($tahun);
			$data['hasil_target'] = $this->M_data->jumlah_target($tahun);
			$data['hasil_real'] = $this->M_data->jumlah_real($tahun);
			$data['jml_new'] = $this->M_data->jumlah_new($tahun);
			$data['jml_exist'] = $this->M_data->jumlah_exist($tahun);
			$data['jml_up'] = $this->M_data->jumlah_up($tahun);
			$data['jml_down'] = $this->M_data->jumlah_down($tahun);
			$data['h_pie'] = $this->M_data->grafik_donut($tahun);
			$data['search'] = $tahun;
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Direktur/v_hal_utama_direktur',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function upload_profil(){
		$id = $this->session->userdata('id_user');
		$name= "$id";

		$config['upload_path']   = './profile/';
		$config['allowed_types'] = 'jpg';
		$config['max_size']      = '0';
		$config['overwrite'] 	 = TRUE;
		$config['max_filename']	 = '0';
		$config['remove_spaces'] = TRUE;
		$config['file_name']	 = $name;

		$this->load->library('upload', $config);
		$this->upload->do_upload('userfile');

		$data = array('upload_data' => $this->upload->data());
		$image= $data['upload_data']['file_name'];

		$this->load->helper("file");
		unlink(base_url("profile/$image"));

		$where = array('id_user' =>$id);
		$data  = array('picture' =>$image);
		if($this->upload->do_upload('userfile')){
			$this->M_data->update_data($where,$data,'t_data_user');
			$this->session->set_flashdata('success','<strong>Success!</strong>  Profil berhasil diupdate.');
			redirect('Direktur/show_profil');
		}else{
			$this->session->set_flashdata('error','<strong>Erorr!</strong>  tidak bisa diupdate.');
			redirect('Direktur/show_profil');
		}

	}
	function show_profil(){
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Direktur/v_profil',$data);
	}
	function detail_log_direktur(){
		$id_log = $this->input->get('id_log');
		$hasil  = array('id_log' => $id_log);
		$t['data_log'] = $this->M_data->tampil_detail_log($hasil,'t_log')->result();
		$t['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Direktur/v_detail_log_direktur',$t);
	}
	function show_coment(){
		$id_log = $this->input->post('id_log');
		$data 	= $this->M_data->show_comment($id_log)->result();
		foreach($data as $h){
			echo "$h->komentar";	
		}
	}
	function daily_report_direktur(){
		if($this->session->userdata('status') == "direktur"){
			$data['t_data_report'] = $this->M_data->tampil_daily_direktur()->result();
			$data['profil'] = $this->M_data->show_profil()->result();
			$this->load->view('Direktur/v_daily_report_direktur',$data);
		}
		else{
			redirect(base_url('Login'));
		}
	}
	function ganti_pwd(){
		$email = $this->input->post('email');
		$password  = $this->input->post('password_baru');
		$sel_email = $this->M_data->seleksi_email($email);

		if ($sel_email == 0) {
			$this->session->set_flashdata('error','<strong>Erorr!</strong>  Email tidak terdaftar.');
			redirect('Admin/show_profil');
		}else{
			$data = array('password' => $password);
			$where = array('email' =>$email);
			$this->M_data->update_data($where,$data,'t_data_user');
			$this->session->set_flashdata('success','<strong>Success!</strong>  Profil berhasil diupdate.');
			redirect('Direktur/show_profil');	
		}
	}
	function detail_project_direktur(){
		$instansi = $this->input->post('instan');
		$id_user = $this->input->post('id_user');
		$nama_project = $this->input->post('project');

		$where = array('id_user' => $id_user,'instansi' => $instansi,'nama_project' =>$nama_project);
		$data ['data_log'] = $this->M_data->prog_detail_admin($id_user,$instansi,$nama_project)->result();
		$data['profil'] = $this->M_data->show_profil()->result();
		$this->load->view('Direktur/v_data_detail_direktur',$data);
	}
}
